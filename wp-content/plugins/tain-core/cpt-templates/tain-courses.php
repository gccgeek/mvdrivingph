<?php
// Course Content
$title_opt = TainFamework::tain_static_theme_mod('course-title-opt');
while ( have_posts() ) : the_post();
?>
	
	<div class="course">
		<div class="course-info-wrap">
		
			<?php if( $title_opt ) : ?>
			<div class="course-title">
				<h2><?php the_title(); ?></h2>
			</div>
			<?php endif; // desg exists ?>
		
			<?php if( has_post_thumbnail( get_the_ID() ) ): ?>
			<div class="course-img">
				<?php the_post_thumbnail( 'large', array( 'class' => 'img-fluid' ) ); ?>
			</div>
			<?php endif; // if thumb exists ?>
			
			<?php
			$price = get_post_meta( get_the_ID(), 'tain_course_price', true );
			if( $price ): 
				$price_allowed_html = array(
					'i' => array(
						'class' => array()
					),
					'span' => array(
						'class' => array()
					)
				);
			?>
				<div class="course-price-wrap"><span class="course-price"><?php echo wp_kses( $price, $price_allowed_html ); ?></span></div>
			<?php
			endif;
			?>
		
			<div class="course-content">
				<?php the_content(); ?>
			</div>
			
			<?php TainCPTElements::tain_cpt_nav(); ?>
		</div> <!-- .course-info-wrap -->
	</div><!-- .course -->
<?php
endwhile; // End of the loop.