<?php
	
class TainFamework {
	
	public static $tain_mod = '';
	
	function __construct(){
		$tain_mod = get_option( 'tain_theme_options_new');
		if( !empty( $tain_mod ) ){
			self::$tain_mod = $tain_mod;
		}elseif( function_exists( 'tain_default_theme_values' ) ){
			$input_val = tain_default_theme_values();
			self::$tain_mod = json_decode( $input_val, true );
		}
	}
	
	public static function tain_static_theme_mod($field){
		$tain_mod = self::$tain_mod;
		return isset( $tain_mod[$field] ) && $tain_mod[$field] != '' ? $tain_mod[$field] : '';
	}

}
$tain_framework = new TainFamework();