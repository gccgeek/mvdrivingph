<?php
/**
 * Template coming soon default
 */
 
 //get maintenance header
 require_once( TAIN_CORE_DIR . 'maintenance/header.php' );

 $address = TainFamework::tain_static_theme_mod( 'maintenance-address' );
 $email = TainFamework::tain_static_theme_mod( 'maintenance-email' );
 $phone = TainFamework::tain_static_theme_mod( 'maintenance-phone' );
 
?>
<div class="container text-center maintenance-wrap">
	<div class="row">
		<div class="col-md-12">
			<h1 class="maintenance-title"><?php esc_html_e( 'Coming Soon', 'tain' ); ?></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<h4><?php esc_html_e( 'Phone', 'tain' ); ?></h4>
			<div class="maintenance-phone">
				<?php echo esc_html(  $phone ); ?>
			</div>
		</div>
		<div class="col-md-4">
			<h4><?php esc_html_e( 'Address', 'tain' ); ?></h4>
			<div class="maintenance-address">
				<?php echo wp_kses_post( $address ); ?>
			</div>
		</div>
		<div class="col-md-4">
			<h4><?php esc_html_e( 'Email', 'tain' ); ?></h4>
			<div class="maintenance-email">
				<?php echo esc_html(  $email ); ?>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12 maintenance-footer">
			<p><?php esc_html_e( 'We are currently working on an awesome new site, which will be ready soon. Stay Tuned!', 'tain' ); ?></p>
		</div>
	</div>
	
</div>
<?php
 //get maintenance header
 require_once( TAIN_CORE_DIR . 'maintenance/footer.php' );
?>