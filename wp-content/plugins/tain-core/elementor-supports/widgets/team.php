<?php
/**
 * Tain Elementor Addon Team Widget
 *
 * @since 1.0.0
 */
class Elementor_Team_Widget extends \Elementor\Widget_Base {
	
	private $excerpt_len;
	
	/**
	 * Get widget name.
	 *
	 * Retrieve Team widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return "tainteam";
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Team widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( "Team", "tain-core" );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Team widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return "ti-user";
	}


	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Animated Text widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ "tain-elements" ];
	}
	
	/**
	 * Retrieve the list of scripts the counter widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.3.0
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends() {
		if ( is_elementor_editor() ){
			wp_enqueue_style( 'owl-carousel' );
			return [ 'owl-carousel', 'custom-front'  ];
		}
		
		return [ 'owl-carousel', 'custom-front'  ];
	}
	
	public function get_style_depends() {
		return [ 'owl-carousel' ];
	}

	/**
	 * Register Animated Text widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function register_controls() {

		//General Section
		$this->start_controls_section(
			"general_section",
			[
				"label"	=> esc_html__( "General", "tain-core" ),
				"tab"	=> \Elementor\Controls_Manager::TAB_CONTENT,
				"description"	=> esc_html__( "Default team options.", "tain-core" ),
			]
		);
		$this->add_control(
			"extra_class",
			[
				"type"			=> \Elementor\Controls_Manager::TEXT,
				"label" 		=> esc_html__( "Extra Class", "tain-core" ),
				"description"	=> esc_html__( "Put extra class for some additional styles.", "tain-core" ),
			]
		);
		$this->add_control(
			"post_per_page",
			[
				"type"			=> \Elementor\Controls_Manager::TEXT,
				"label"			=> esc_html__( "Post Per Page", "tain-core" ),
				"description"	=> esc_html__( "Here you can define post limits per page. Example 10", "tain-core" ),
				"default" 		=> "10",
				"placeholder"	=> "10"
			]
		);
		$this->add_control(
			"excerpt_length",
			[
				"type"			=> \Elementor\Controls_Manager::TEXT,
				"label"			=> esc_html__( "Excerpt Length", "tain-core" ),
				"description"	=> esc_html__( "Here you can define post excerpt length. Example 10", "tain-core" ),
				"default" 		=> "15"
			]
		);
		$this->add_control(
			"more_text",
			[
				"type"			=> \Elementor\Controls_Manager::TEXT,
				"label"			=> esc_html__( "Read More Text", "tain-core" ),
				"description"	=> esc_html__( "Here you can enter read more text instead of default text.", "tain-core" ),
				"default" 		=> esc_html__( "Read More", "tain-core" )
			]
		);
		$this->end_controls_section();

		//Filter Section
		$this->start_controls_section(
			"filter_section",
			[
				"label"			=> esc_html__( "Filter", "tain-core" ),
				"tab"			=> \Elementor\Controls_Manager::TAB_CONTENT,
				"description"	=> esc_html__( "Team filter options here available.", "tain-core" ),
			]
		);
		$this->add_control(
			"team_cats",
			[
				"type"			=> \Elementor\Controls_Manager::TEXT,
				"label"			=> esc_html__( "Enter Categories Id", "tain-core" ),
				"description"	=> esc_html__( "Here you can enter team categoeis id. Example 4, 5, 7", "tain-core" ),
				"default" 		=> ''
			]
		);
		$this->add_control(
			"team_ids",
			[
				"type"			=> \Elementor\Controls_Manager::TEXT,
				"label"			=> esc_html__( "Enter Team Id's", "tain-core" ),
				"description"	=> esc_html__( "Here you can enter team post id's for get specific team member. Example 4, 5, 7", "tain-core" ),
				"default" 		=> ''
			]
		);
		$this->end_controls_section();
		
		//Layouts Section
		$this->start_controls_section(
			"layouts_section",
			[
				"label"			=> esc_html__( "Layouts", "tain-core" ),
				"tab"			=> \Elementor\Controls_Manager::TAB_CONTENT,
				"description"	=> esc_html__( "Team layout options here available.", "tain-core" ),
			]
		);
		$this->add_control(
			"heading_tag",
			[
				"label"			=> esc_html__( "Heading Tag", "tain-core" ),
				"type"			=> \Elementor\Controls_Manager::SELECT,
				"default"		=> "h3",
				"options"		=> [
					"h1"		=> esc_html__( "Heading 1", "tain-core" ),
					"h2"		=> esc_html__( "Heading 2", "tain-core" ),
					"h3"		=> esc_html__( "Heading 3", "tain-core" ),
					"h4"		=> esc_html__( "Heading 4", "tain-core" ),
					"h5"		=> esc_html__( "Heading 5", "tain-core" ),
					"h6"		=> esc_html__( "Heading 6", "tain-core" ),
				]
			]
		);
		$this->add_control(
			"font_color",
			[
				"type"			=> \Elementor\Controls_Manager::COLOR,
				"label"			=> esc_html__( "Font Color", "tain-core" ),
				"description"	=> esc_html__( "Here you can put the font color.", "tain-core" ),
				"default" 		=> "",
				'selectors' => [
					'{{WRAPPER}} .team-wrapper' => 'color: {{VALUE}};',
					'{{WRAPPER}} .team-wrapper.team-dark .team-inner' => 'color: {{VALUE}};'
				],
			]
		);
		$this->add_control(
			"team_layout",
			[
				"label"			=> esc_html__( "Team Layout", "tain-core" ),
				"type"			=> \Elementor\Controls_Manager::SELECT,
				"default"		=> "default",
				"options"		=> [
					"default"		=> esc_html__( "Default", "tain-core" ),
					"classic"		=> esc_html__( "Classic", "tain-core" ),
					"modern"		=> esc_html__( "Modern", "tain-core" ),
					"list"	=> esc_html__( "List", "tain-core" ),
				]
			]
		);
		$this->add_control(
			"variation",
			[
				"type"			=> \Elementor\Controls_Manager::SELECT,
				"label"			=> esc_html__( "Team Variation", "tain-core" ),
				"description"	=> esc_html__( "This is option for team variatoin either dark or light.", "tain-core" ),
				"default"		=> "light",
				"options"		=> [
					"light"			=> esc_html__( "Light", "tain-core" ),
					"dark"			=> esc_html__( "Dark", "tain-core" )
				]
			]
		);
		$this->add_control(
			"team_cols",
			[
				"type"			=> \Elementor\Controls_Manager::SELECT,
				"label"			=> esc_html__( "Team Columns", "tain-core" ),
				"description"	=> esc_html__( "This is option for team columns.", "tain-core" ),
				"default"		=> "6",
				"options"		=> [
					"3"			=> esc_html__( "4 Columns", "tain-core" ),
					"4"			=> esc_html__( "3 Columns", "tain-core" ),
					"6"			=> esc_html__( "2 Columns", "tain-core" ),
					"12"		=> esc_html__( "1 Column", "tain-core" )
				]
			]
		);
		$this->add_control(
			"team_items",
			[
				"label"				=> "Post Items",
				"description"		=> esc_html__( "This is settings for team custom layout. here you can set your own layout. Drag and drop needed team items to Enabled part.", "tain-core" ),
				"type"				=> "dragdrop",
				"ddvalues" 			=> [ 
					"Enabled" 		=> [ 
						"thumb"			=> esc_html__( "Feature Image", "tain-core" ),
						"name"			=> esc_html__( "Name", "tain-core" ),
						"designation"	=> esc_html__( "Designation", "tain-core" ),
						"excerpt"		=> esc_html__( "Excerpt", "tain-core" ),
						"social"		=> esc_html__( "Social Links", "tain-core" )
					],
					"disabled"		=> [
						"more"			=> esc_html__( "Read More", "tain-core" ),
						"info"			=> esc_html__( "Info", "tain-core" )
					]
				]
			]
		);
		$this->add_control(
			"text_align",
			[
				"type"			=> \Elementor\Controls_Manager::SELECT,
				"label"			=> esc_html__( "Text Align", "tain-core" ),
				"description"	=> esc_html__( "This is option for team text align.", "tain-core" ),
				"default"		=> "default",
				"options"		=> [
					"default"	=> esc_html__( "Default", "tain-core" ),
					"left"		=> esc_html__( "Left", "tain-core" ),
					"center"	=> esc_html__( "Center", "tain-core" ),
					"right"		=> esc_html__( "Right", "tain-core" )
				]
			]
		);
		$this->end_controls_section();
		
		//Overlay Section
		$this->start_controls_section(
			"overlay_section",
			[
				"label"			=> esc_html__( "Overlay", "tain-core" ),
				"tab"			=> \Elementor\Controls_Manager::TAB_CONTENT,
				"description"	=> esc_html__( "Team image overlay options here available.", "tain-core" ),
			]
		);
		$this->add_control(
			"team_overlay_opt",
			[
				"label" 		=> esc_html__( "Overlay Team Option", "tain-core" ),
				"description"	=> esc_html__( "This is option for enable overlay team option.", "tain-core" ),
				"type" 			=> "toggleswitch",
				"default" 		=> "0"
			]
		);
		$this->add_control(
			"team_overlay_font_color",
			[
				"type"			=> \Elementor\Controls_Manager::COLOR,
				"label"			=> esc_html__( "Overlay Font Color", "tain-core" ),
				"description"	=> esc_html__( "Here you can put team overlay font color.", "tain-core" ),
				"default" 		=> "",
				'selectors' => [
					'{{WRAPPER}} .team-wrapper .team-overlay' => 'color: {{VALUE}};',
				],
				"condition" 	=> [
					"team_overlay_opt" 	=> "1"
				]
			]
		);
		$this->add_control(
			"team_overlay_link_color",
			[
				"type"			=> \Elementor\Controls_Manager::COLOR,
				"label"			=> esc_html__( "Overlay Link Color", "tain-core" ),
				"description"	=> esc_html__( "Here you can put team overlay link normal color. Example #ffffff", "tain-core" ),
				"default" 		=> "",
				'selectors' => [
					'{{WRAPPER}} .team-wrapper .team-overlay a.client-name' => 'color: {{VALUE}};',
				],
				"condition" 	=> [
					"team_overlay_opt" 	=> "1"
				]
			]
		);
		$this->add_control(
			"team_overlay_link_hcolor",
			[
				"type"			=> \Elementor\Controls_Manager::COLOR,
				"label"			=> esc_html__( "Overlay Link Hover Color", "tain-core" ),
				"description"	=> esc_html__( "Here you can put team overlay link hover color. Example #ffffff", "tain-core" ),
				"default" 		=> "",
				'selectors' => [
					'{{WRAPPER}} .team-wrapper .team-overlay a.client-name:hover' => 'color: {{VALUE}};',
				],
				"condition" 	=> [
					"team_overlay_opt" 	=> "1"
				]
			]
		);
		$this->add_control(
			"overlay_team_items",
			[
				"label"				=> "Overlay Team Items",
				"description"		=> esc_html__( "This is settings for team items(name, excerpt etc..) overlay on thumbnail. Drag and drop needed team items to Enabled part.", "tain-core" ),
				"type"				=> "dragdrop",
				"ddvalues" 			=> [ 
					"Enabled" 		=> [ 
						"name"			=> esc_html__( "Name", "tain-core" ),
					],
					"disabled"		=> [
						"designation"	=> esc_html__( "Designation", "tain-core" ),
						"excerpt"		=> esc_html__( "Excerpt", "tain-core" ),
						"social"		=> esc_html__( "Social Links", "tain-core" )
					]
				],
				"condition" 	=> [
					"team_overlay_opt" 	=> "1"
				]
			]
		);
		$this->add_control(
			"team_overlay_position",
			[
				"type"			=> \Elementor\Controls_Manager::SELECT,
				"label"			=> esc_html__( "Overlay Items Position", "tain-core" ),
				"description"	=> esc_html__( "This is option for overlay items position.", "tain-core" ),
				"default"		=> "bottom-left",
				"options"		=> [
					"center"	=> esc_html__( "Center", "tain-core" ),
					"top-left"		=> esc_html__( "Top Left", "tain-core" ),
					"top-right"	=> esc_html__( "Top Right", "tain-core" ),
					"bottom-left"		=> esc_html__( "Bottom Left", "tain-core" ),
					"bottom-right"		=> esc_html__( "Bottom Right", "tain-core" )
				],
				"condition" 	=> [
					"team_overlay_opt" 	=> "1"
				]
			]
		);
		$this->add_control(
			"overlay_text_align",
			[
				"type"			=> \Elementor\Controls_Manager::SELECT,
				"label"			=> esc_html__( "Overlay Text Align", "tain-core" ),
				"description"	=> esc_html__( "This is option for overlay team text align.", "tain-core" ),
				"default"		=> "default",
				"options"		=> [
					"default"	=> esc_html__( "Default", "tain-core" ),
					"left"		=> esc_html__( "Left", "tain-core" ),
					"center"	=> esc_html__( "Center", "tain-core" ),
					"right"		=> esc_html__( "Right", "tain-core" )
				],
				"condition" 	=> [
					"team_overlay_opt" 	=> "1"
				]
			]
		);
		$this->add_control(
			"team_overlay_type",
			[
				"type"			=> \Elementor\Controls_Manager::SELECT,
				"label"			=> esc_html__( "Overlay Type", "tain-core" ),
				"description"	=> esc_html__( "This is option for team overlay type.", "tain-core" ),
				"default"		=> "none",
				"options"		=> [
					"none"		=> esc_html__( "None", "tain-core" ),
					"dark"		=> esc_html__( "Overlay Dark", "tain-core" ),
					"light"		=> esc_html__( "Overlay White", "tain-core" ),
					"custom"	=> esc_html__( "Custom Color", "tain-core" )
				],
				"condition" 	=> [
					"team_overlay_opt" 	=> "1"
				]
			]
		);
		$this->add_control(
			"team_overlay_custom_color",
			[
				"type"			=> \Elementor\Controls_Manager::COLOR,
				"label"			=> esc_html__( "Overlay Custom Color", "tain-core" ),
				"description"	=> esc_html__( "Here you can put team overlay custom color.", "tain-core" ),
				"default" 		=> "",
				'selectors' => [
					'{{WRAPPER}} .team-wrapper .team-thumb .overlay-custom' => 'background: {{VALUE}};',
				],
				"condition" 	=> [
					"team_overlay_type" 	=> "custom"
				]
			]
		);
		$this->end_controls_section();
		
		//Image Section
		$this->start_controls_section(
			"image_section",
			[
				"label"			=> esc_html__( "Image", "tain-core" ),
				"tab"			=> \Elementor\Controls_Manager::TAB_CONTENT,
				"description"	=> esc_html__( "Image options here available.", "tain-core" ),
			]
		);
		$this->add_control(
			"image_size",
			[
				"type"			=> \Elementor\Controls_Manager::SELECT,
				"label"			=> esc_html__( "Image Size", "tain-core" ),
				'description'	=> esc_html__( 'Choose thumbnail size for display different size image.', 'tain-core' ),
				"default"		=> "large",
				"options"		=> [
					"large"			=> esc_html__( "Large", "tain-core" ),
					"medium"		=> esc_html__( "Medium", "tain-core" ),
					"thumbnail"		=> esc_html__( "Thumbnail", "tain-core" ),
					"custom"		=> esc_html__( "Custom", "tain-core" )
				]
			]
		);
		$this->add_control(
			"custom_image_size",
			[
				"type"			=> \Elementor\Controls_Manager::TEXT,
				"label"			=> esc_html__( "Custom Image Size", "tain-core" ),
				"description"	=> esc_html__( "Enter custom image size. You must specify the semi colon(;) at last then only it'll crop. eg: 200x200;", "tain-core" ),
				"default" 		=> "",
				"condition" 	=> [
					"image_size" 		=> "custom"
				]
			]
		);
		$this->add_control(
			"hard_croping",
			[
				"label" 		=> esc_html__( "Image Hard Crop", "tain-core" ),
				"type" 			=> "toggleswitch",
				"default" 		=> "0",
				"condition" 	=> [
					"image_size" 		=> "custom"
				]
			]
		);
		$this->add_control(
			"image_shape",
			[
				"type"			=> \Elementor\Controls_Manager::SELECT,
				"label"			=> esc_html__( "Image Shape", "tain-core" ),
				'description'	=> esc_html__( 'Choose team image shape. Like rounded, circle, etc..', 'tain-core' ),
				"default"		=> "",
				"options"		=> [
					""			=> esc_html__( "Default", "tain-core" ),
					"rounded"			=> esc_html__( "Rounded", "tain-core" ),
					"rounded-circle"	=> esc_html__( "Circle", "tain-core" ),
					"img-thumbnail"		=> esc_html__( "Thumbnail", "tain-core" ),
					"rounded img-thumbnail"	=> esc_html__( "Rounded Thumbnail", "tain-core" ),
					"rounded-circle img-thumbnail"	=> esc_html__( "Circle Thumbnail", "tain-core" )
				]
			]
		);
		$this->end_controls_section();
		
		//Social Icons Section
		$this->start_controls_section(
			"social_section",
			[
				"label"			=> esc_html__( "Social Icons", "tain-core" ),
				"tab"			=> \Elementor\Controls_Manager::TAB_CONTENT,
				"description"	=> esc_html__( "Team social icons options here available.", "tain-core" ),
			]
		);
		$this->add_control(
			"social_icons_type",
			[
				"label"			=> esc_html__( "Social Iocns Type", "tain-core" ),
				"description"	=> esc_html__( "Here you can choose the social icons view type.", "tain-core" ),
				"type"			=> \Elementor\Controls_Manager::SELECT,
				"default"		=> "transparent",
				"options"		=> [
					"squared"		=> esc_html__( "Squared", "tain-core" ),
					"rounded"		=> esc_html__( "Rounded", "tain-core" ),
					"circled"		=> esc_html__( "Circled", "tain-core" ),
					"transparent"	=> esc_html__( "Transparent", "tain-core" )
				]
			]
		);
		$this->add_control(
			"social_icons_fore",
			[
				"type"			=> \Elementor\Controls_Manager::SELECT,
				"label"			=> esc_html__( "Social Icons Fore", "tain-core" ),
				"description"	=> esc_html__( "This is option for day social icons fore color.", "tain-core" ),
				"default"		=> "black",
				"options"		=> [
					"black"	=> esc_html__( "Black", "tain-core" ),
					"white"		=> esc_html__( "White", "tain-core" ),
					"own"	=> esc_html__( "Own Color", "tain-core" )
				]
			]
		);
		$this->add_control(
			"social_icons_hfore",
			[
				"type"			=> \Elementor\Controls_Manager::SELECT,
				"label"			=> esc_html__( "Social Icons Fore Hover", "tain-core" ),
				"description"	=> esc_html__( "This is option for day social icons fore hover color.", "tain-core" ),
				"default"		=> "h-own",
				"options"		=> [
					"h-black"	=> esc_html__( "Black", "tain-core" ),
					"h-white"	=> esc_html__( "White", "tain-core" ),
					"h-own"		=> esc_html__( "Own Color", "tain-core" )
				]
			]
		);
		$this->add_control(
			"social_icons_bg",
			[
				"type"			=> \Elementor\Controls_Manager::SELECT,
				"label"			=> esc_html__( "Social Icons Background", "tain-core" ),
				"description"	=> esc_html__( "This is option for day social icons background color.", "tain-core" ),
				"default"		=> "bg-transparent",
				"options"		=> [
					"bg-transparent"		=> esc_html__( "Transparent", "tain-core" ),
					"bg-white"		=> esc_html__( "White", "tain-core" ),
					"bg-black"		=> esc_html__( "Black", "tain-core" ),
					"bg-light"		=> esc_html__( "Light", "tain-core" ),
					"bg-dark"		=> esc_html__( "Dark", "tain-core" ),
					"bg-own"		=> esc_html__( "Own Color", "tain-core" )
				]
			]
		);
		$this->add_control(
			"social_icons_hbg",
			[
				"type"			=> \Elementor\Controls_Manager::SELECT,
				"label"			=> esc_html__( "Social Icons Background Hover", "tain-core" ),
				"description"	=> esc_html__( "This is option for day social icons background hover color.", "tain-core" ),
				"default"		=> "hbg-transparent",
				"options"		=> [
					"hbg-transparent"	=> esc_html__( "Transparent", "tain-core" ),
					"hbg-white"			=> esc_html__( "White", "tain-core" ),
					"hbg-black"			=> esc_html__( "Black", "tain-core" ),
					"hbg-light"			=> esc_html__( "Light", "tain-core" ),
					"hbg-dark"			=> esc_html__( "Dark", "tain-core" ),
					"hbg-own"			=> esc_html__( "Own Color", "tain-core" )
				]
			]
		);
		$this->end_controls_section();
		
		//Slide Section
		$this->start_controls_section(
			"slide_section",
			[
				"label"			=> esc_html__( "Slide", "tain-core" ),
				"tab"			=> \Elementor\Controls_Manager::TAB_CONTENT,
				"description"	=> esc_html__( "Team slide options here available.", "tain-core" ),
			]
		);
		$this->add_control(
			"slide_opt",
			[
				"label" 		=> esc_html__( "Slide Option", "tain-core" ),
				"description"	=> esc_html__( "This is option for team slider option.", "tain-core" ),
				"type" 			=> "toggleswitch",
				"default" 		=> "0"
			]
		);
		$this->add_control(
			"slide_item",
			[
				"type"			=> \Elementor\Controls_Manager::TEXT,
				"label"			=> esc_html__( "Slide Items", "tain-core" ),
				"description"	=> esc_html__( "This is option for team slide items shown on large devices.", "tain-core" ),
				"default" 		=> "2",
			]
		);
		$this->add_control(
			"slide_item_tab",
			[
				"type"			=> \Elementor\Controls_Manager::TEXT,
				"label"			=> esc_html__( "Items on Tab", "tain-core" ),
				"description"	=> esc_html__( "This is option for team slide items shown on tab.", "tain-core" ),
				"default" 		=> "2",
			]
		);
		$this->add_control(
			"slide_item_mobile",
			[
				"type"			=> \Elementor\Controls_Manager::TEXT,
				"label"			=> esc_html__( "Items on Mobile", "tain-core" ),
				"description"	=> esc_html__( "This is option for team slide items shown on mobile.", "tain-core" ),
				"default" 		=> "1",
			]
		);
		$this->add_control(
			"slide_item_autoplay",
			[
				"label" 		=> esc_html__( "Auto Play", "tain-core" ),
				"description"	=> esc_html__( "This is option for team slider auto play.", "tain-core" ),
				"type" 			=> "toggleswitch",
				"default" 		=> "0"
			]
		);
		$this->add_control(
			"slide_item_loop",
			[
				"label" 		=> esc_html__( "Loop", "tain-core" ),
				"description"	=> esc_html__( "This is option for team slider loop.", "tain-core" ),
				"type" 			=> "toggleswitch",
				"default" 		=> "0"
			]
		);
		$this->add_control(
			"slide_center",
			[
				"label" 		=> esc_html__( "Items Center", "tain-core" ),
				"description"	=> esc_html__( "This is option for team slider center, for this option must active loop and minimum items 2.", "tain-core" ),
				"type" 			=> "toggleswitch",
				"default" 		=> "0"
			]
		);
		$this->add_control(
			"slide_nav",
			[
				"label" 		=> esc_html__( "Navigation", "tain-core" ),
				"description"	=> esc_html__( "This is option for team slider navigation.", "tain-core" ),
				"type" 			=> "toggleswitch",
				"default" 		=> "0"
			]
		);
		$this->add_control(
			"slide_dots",
			[
				"label" 		=> esc_html__( "Pagination", "tain-core" ),
				"description"	=> esc_html__( "This is option for team slider pagination.", "tain-core" ),
				"type" 			=> "toggleswitch",
				"default" 		=> "0"
			]
		);
		$this->add_control(
			"slide_margin",
			[
				"type"			=> \Elementor\Controls_Manager::TEXT,
				"label"			=> esc_html__( "Items Margin", "tain-core" ),
				"description"	=> esc_html__( "This is option for team slider margin space.", "tain-core" ),
				"default" 		=> "",
			]
		);
		$this->add_control(
			"slide_duration",
			[
				"type"			=> \Elementor\Controls_Manager::TEXT,
				"label"			=> esc_html__( "Items Duration", "tain-core" ),
				"description"	=> esc_html__( "This is option for team slider duration.", "tain-core" ),
				"default" 		=> "5000",
			]
		);
		$this->add_control(
			"slide_smart_speed",
			[
				"type"			=> \Elementor\Controls_Manager::TEXT,
				"label"			=> esc_html__( "Items Smart Speed", "tain-core" ),
				"description"	=> esc_html__( "This is option for team slider smart speed.", "tain-core" ),
				"default" 		=> "250",
			]
		);
		$this->add_control(
			"slide_slideby",
			[
				"type"			=> \Elementor\Controls_Manager::TEXT,
				"label"			=> esc_html__( "Items Slideby", "tain-core" ),
				"description"	=> esc_html__( "This is option for team slider scroll by.", "tain-core" ),
				"default" 		=> "1",
			]
		);
		$this->end_controls_section();
		
		//Spacing Section
		$this->start_controls_section(
			"spacing_section",
			[
				"label"			=> esc_html__( "Spacing", "tain-core" ),
				"tab"			=> \Elementor\Controls_Manager::TAB_CONTENT,
				"description"	=> esc_html__( "Each item bottom space options here available.", "tain-core" ),
			]
		);
		$this->add_control(
			"sc_spacing",
			[
				"type"			=> 'itemspacing',
				"label"			=> esc_html__( "Items Spacing", "tain-core" ),
				"description"	=> esc_html__( "Here you can mention each team items bottom space if you want to set default space of any item just use hyphen(-). Example 10px 20px - 10px", "tain-core" ),
				"default" 		=> ""
			]
		);
		$this->end_controls_section();

	}

	/**
	 * Render Animated Text widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

		$settings = $this->get_settings_for_display();
		
		extract( $settings );
		$output = '';
		
		//Defined Variable
		$class_names = isset( $extra_class ) && $extra_class != '' ? ' ' . $extra_class : '';
		$post_per_page = isset( $post_per_page ) && $post_per_page != '' ? $post_per_page : '';
		$excerpt_length = isset( $excerpt_length ) && $excerpt_length != '' ? $excerpt_length : 0;
		$this->excerpt_len = $excerpt_length;
		$class_names .= isset( $team_layout ) ? ' team-' . $team_layout : ' team-1';
		$class_names .= isset( $text_align ) && $text_align != 'default' ? ' text-' . $text_align : '';
		$team_layout = isset( $team_layout ) ? $team_layout : '';
		$class_names .= isset( $variation ) ? ' team-' . $variation : '';
		$cols = isset( $team_cols ) ? $team_cols : 12;
		$more_text = isset( $more_text ) && $more_text != '' ? $more_text : '';
		$slide_opt = isset( $slide_opt ) && $slide_opt == '1' ? true : false;
		$team_overlay_opt = isset( $team_overlay_opt ) && $team_overlay_opt == '1' ? true : false;
		$team_overlay_type = isset( $team_overlay_type ) ? $team_overlay_type : 'none';
		$heading_tag = isset( $heading_tag ) ? $heading_tag : 'h3';
		$list_layout = isset( $team_layout ) && $team_layout == 'list' ? 1 : 0;
		$image_shape = isset( $image_shape ) && $image_shape != '' ? $image_shape : '';
		$team_cats = isset( $team_cats ) && $team_cats != '' ? $team_cats : '';
		$team_ids = isset( $team_ids ) && $team_ids != '' ? $team_ids : '';
		$order_by = '';
		
		$inc_cat_array = '';
		if( $team_cats ){
			//Cats In
			$cats_in = array();
			$filter = preg_replace( '/\s+/', '', $team_cats );
			$filter = explode( ',', rtrim( $filter, ',' ) );
			foreach( $filter as $cat ){
				$cat_term = get_term_by( 'id', $cat, 'team-categories' );	
				//post in array push
				if( isset( $cat_term->term_id ) ) array_push( $cats_in, absint( $cat_term->term_id ) );
			}

			$inc_cat_array = $cats_in ? array( 'taxonomy' => 'team-categories', 'field' => 'id', 'terms' => $cats_in ) : '';
		}
		
		if( $team_ids ){
			$team_ids = preg_replace( '/\s+/', '', $team_ids );
			$team_ids = explode( ',', rtrim( $team_ids, ',' ) );
			$order_by = 'post__in';
		}
		
		$sclass_name = isset( $social_style ) && !empty( $social_style ) ? ' social-' . $social_style : '';
		$sclass_name .= isset( $social_color ) && !empty( $social_color ) ? ' social-' . $social_color : '';
		$sclass_name .= isset( $social_hcolor ) && !empty( $social_hcolor ) ? ' social-' . $social_hcolor : '';
		$sclass_name .= isset( $social_bg ) && !empty( $social_bg ) ? ' social-' . $social_bg : '';
		$sclass_name .= isset( $social_hbg ) && !empty( $social_hbg ) ? ' social-' . $social_hbg : '';
		
		$overlay_class = '';
		$overlay_class .= isset( $team_overlay_position ) ? ' overlay-'.$team_overlay_position : ' overlay-center';
		
		$thumb_size = isset( $image_size ) ? $image_size : '';
		$cus_thumb_size = '';
		$hard_crop = false;
		if( $thumb_size == 'custom' ){
			$cus_thumb_size = isset( $custom_image_size ) && $custom_image_size != '' ? $custom_image_size : '';
			$hard_crop = isset( $hard_croping ) && $hard_croping == '1' ? true : false;
		}
		
		$default_items = array( 
			'thumb'	=> esc_html__( 'Image', 'tain-core' ),
			'name'	=> esc_html__( 'Name', 'tain-core' ),
			'designation'	=> esc_html__( 'Designation', 'tain-core' ),
			'excerpt'	=> esc_html__( 'Excerpt', 'tain-core' ),
			'social'	=> esc_html__( 'Social Links', 'tain-core' )
		);
		$default_overlay_items = array( 
			'name'	=> esc_html__( 'Name', 'tain-core' )
		);
		$elemetns = isset( $team_items ) && !empty( $team_items ) ? json_decode( $team_items, true ) : array( 'Enabled' => $default_items );
		$overlay_elemetns = isset( $overlay_team_items ) && !empty( $overlay_team_items ) ? json_decode( $overlay_team_items, true ) : array( 'Enabled' => $default_overlay_items );
		
		// This is custom css options for main shortcode warpper
		$shortcode_css = '';
		$shortcode_rand_id = $rand_class = 'shortcode-rand-'. tain_shortcode_rand_id();
		
		//Spacing
		if( isset( $sc_spacing ) && !empty( $sc_spacing ) ){
			$sc_spacing = preg_replace( '!\s+!', ' ', $sc_spacing );
			$space_arr = explode( " ", $sc_spacing );
			$i = 1;
			
			if( !$list_layout ){
				$space_class_name = '.' . esc_attr( $rand_class ) . '.team-wrapper .team-inner >';
			}else{
				$space_class_name = '.' . esc_attr( $rand_class ) . '.team-wrapper .team-list-item .media-body >';
			}
			
			foreach( $space_arr as $space ){
				$shortcode_css .= $space != '-' ? $space_class_name .' *:nth-child('. esc_attr( $i ) .') { margin-bottom: '. esc_attr( $space ) .'; }' : '';
				$i++;
			}
		}

		
		$gal_atts = '';
		if( $slide_opt ){
			$gal_atts = array(
				'data-loop="'. ( isset( $slide_item_loop ) && $slide_item_loop == '1' ? 1 : 0 ) .'"',
				'data-margin="'. ( isset( $slide_margin ) && $slide_margin != '' ? absint( $slide_margin ) : 0 ) .'"',
				'data-center="'. ( isset( $slide_center ) && $slide_center == '1' ? 1 : 0 ) .'"',
				'data-nav="'. ( isset( $slide_nav ) && $slide_nav == '1' ? 1 : 0 ) .'"',
				'data-dots="'. ( isset( $slide_dots ) && $slide_dots == '1' ? 1 : 0 ) .'"',
				'data-autoplay="'. ( isset( $slide_item_autoplay ) && $slide_item_autoplay == '1' ? 1 : 0 ) .'"',
				'data-items="'. ( isset( $slide_item ) && $slide_item != '' ? absint( $slide_item ) : 1 ) .'"',
				'data-items-tab="'. ( isset( $slide_item_tab ) && $slide_item_tab != '' ? absint( $slide_item_tab ) : 1 ) .'"',
				'data-items-mob="'. ( isset( $slide_item_mobile ) && $slide_item_mobile != '' ? absint( $slide_item_mobile ) : 1 ) .'"',
				'data-duration="'. ( isset( $slide_duration ) && $slide_duration != '' ? absint( $slide_duration ) : 5000 ) .'"',
				'data-smartspeed="'. ( isset( $slide_smart_speed ) && $slide_smart_speed != '' ? absint( $slide_smart_speed ) : 250 ) .'"',
				'data-scrollby="'. ( isset( $slide_slideby ) && $slide_slideby != '' ? absint( $slide_slideby ) : 1 ) .'"',
				'data-autoheight="false"',
			);
			$data_atts = implode( " ", $gal_atts );
		}
		
		$args = array(
			'post_type' => 'tain-team',
			'posts_per_page' => absint( $post_per_page ),
			'ignore_sticky_posts' => 1,
			'post__in' => $team_ids,
			'orderby' => $order_by,
			'tax_query' => array(
				$inc_cat_array
			)
		);
		
		$query = new WP_Query( $args );
		if ( $query->have_posts() ) {
		
			$team_array = array(
				'thumb_size' => $thumb_size,
				'cus_thumb_size' => $cus_thumb_size,
				'hard_crop' => $hard_crop,
				'excerpt_length' => $excerpt_length,
				'cols' => $cols,
				'more_text' => $more_text,
				'social_class' => $sclass_name,
				'post_heading' => $heading_tag,
				'team_layout' => $team_layout,
				'image_shape' => $image_shape
			);	
		
			if( $shortcode_css ) $class_names .= ' ' . $shortcode_rand_id . ' tain-inline-css';
			
			echo '<div class="team-wrapper'. esc_attr( $class_names ) .'" data-css="'. htmlspecialchars( json_encode( $shortcode_css ), ENT_QUOTES, 'UTF-8' ) .'">';
				$row_stat = 0;
				
				if( !$list_layout ){
				
					//Team Slide
					if( $slide_opt ) echo '<div class="owl-carousel" '. ( $data_atts ) .'>';	

					// Start the Loop
					while ( $query->have_posts() ) : $query->the_post();
						
						// Parameters Defined
						$post_id = get_the_ID();
						$team_array['post_id'] = $post_id;
					
						//Overlay Output Formation
						$overlay_out = '';
						if( $team_overlay_opt ) {
							if( $team_overlay_type != 'none' ){
								$overlay_out .= '<span class="overlay-bg overlay-'. esc_attr( $team_overlay_type ) .'"></span>';
							}
							$overlay_out .= '<div class="team-overlay'. esc_attr( $overlay_class ) .'">';
							
								if( isset( $overlay_elemetns['Enabled'] ) ) :
									foreach( $overlay_elemetns['Enabled'] as $element => $value ){
										$overlay_out .= $this->tain_team_shortcode_elements( $element, $team_array );
									}
								endif;
								
							$overlay_out .= '</div><!-- .team-overlay -->';
						}
					
						if( $row_stat == 0 && !$slide_opt ) echo '<div class="row">';
						
						if( $slide_opt ){
							echo '<div class="item">';	//Service Slide Item
						}else{
							$col_class = "col-lg-". absint( $cols );
							$col_class .= " " . ( $cols == 3 ? "col-md-6" : "col-md-". absint( $cols ) );
							echo '<div class="'. esc_attr( $col_class ) .'">';
						}
						
							$inner_class = $overlay_out ? ' team-overlay-actived' : '';
							echo '<div class="team-inner'. esc_attr( $inner_class ) .'">';

							if( isset( $elemetns['Enabled'] ) ) :
								foreach( $elemetns['Enabled'] as $element => $value ){
									if( $element == 'thumb' ){
										$team_array['overlay'] = $overlay_out;
									}
									echo $this->tain_team_shortcode_elements( $element, $team_array );
								}
							endif;
							
							echo '</div><!-- .team-inner -->';
							
						if( $slide_opt ){
							echo '</div><!-- .item -->';
						}else{
							echo '</div><!-- .cols -->';
							$row_stat++;
							if( $row_stat == ( 12/ $cols ) && !$slide_opt ) :
								echo '</div><!-- .row -->';
								$row_stat = 0;
							endif;
						}
						
					endwhile;
					
					if( $row_stat != 0 && !$slide_opt ){
						echo '</div><!-- .row Unexpected row close -->'; // Unexpected row close
					}
					
					//Team Slide End
					if( $slide_opt ) echo '</div><!-- .owl-carousel -->';
				
				}else{
					
					if( isset( $elemetns['Enabled']['thumb'] ) ) unset( $elemetns['Enabled']['thumb'] );
					
					//Team Slide
					if( $slide_opt ) echo '<div class="owl-carousel" '. ( $data_atts ) .'>';	

					// Start the Loop
					while ( $query->have_posts() ) : $query->the_post();
						
						// Parameters Defined
						$post_id = get_the_ID();
						$team_array['post_id'] = $post_id;
					
						//Overlay Output Formation
						$overlay_out = '';
						if( $team_overlay_opt ) {
							if( $team_overlay_type != 'none' ){
								$overlay_out .= '<span class="overlay-bg overlay-'. esc_attr( $team_overlay_type ) .'"></span>';
							}
							$overlay_out .= '<div class="team-overlay'. esc_attr( $overlay_class ) .'">';
							
								if( isset( $overlay_elemetns['Enabled'] ) ) :
									foreach( $overlay_elemetns['Enabled'] as $element => $value ){
										$overlay_out .= $this->tain_team_shortcode_elements( $element, $team_array );
									}
								endif;
								
							$overlay_out .= '</div><!-- .team-overlay -->';
						}
					
						if( $row_stat == 0 && !$slide_opt ) echo '<div class="row">';
						
						if( $slide_opt ){
							echo '<div class="item">';	//Service Slide Item
						}else{
							$col_class = "col-lg-". absint( $cols );
							$col_class .= " " . ( $cols == 3 ? "col-md-6" : "col-md-". absint( $cols ) );
							echo '<div class="'. esc_attr( $col_class ) .'">';
						}
						
							$inner_class = $overlay_out ? ' team-overlay-actived' : '';
							echo '<div class="media team-list-item'. esc_attr( $inner_class ) .'">';
							
								echo $this->tain_team_shortcode_elements( 'thumb', $team_array );
								
								echo '<div class="media-body">';
									if( isset( $elemetns['Enabled'] ) ) :
										foreach( $elemetns['Enabled'] as $element => $value ){
											if( $element == 'thumb' ){
												$team_array['overlay'] = $overlay_out;
											}
											echo $this->tain_team_shortcode_elements( $element, $team_array );
										}
									endif;
								echo '</div><!-- .media-body -->';
							
							echo '</div><!-- .team-list-item -->';
							
						if( $slide_opt ){
							echo '</div><!-- .item -->';
						}else{
							echo '</div><!-- .cols -->';
							$row_stat++;
							if( $row_stat == ( 12/ $cols ) && !$slide_opt ) :
								echo '</div><!-- .row -->';
								$row_stat = 0;
							endif;
						}
						
					endwhile;
					
					if( $row_stat != 0 && !$slide_opt ){
						echo '</div><!-- .row Unexpected row close -->'; // Unexpected row close
					}
					
					//Team Slide End
					if( $slide_opt ) echo '</div><!-- .owl-carousel -->';
					
				}
				
			echo '</div><!-- .team-wrapper -->';
			
		}// query exists
		
		// use reset postdata to restore orginal query
		wp_reset_postdata();

	}
	
	function tain_team_shortcode_elements( $element, $opts = array() ){
		$output = '';
		switch( $element ){
		
			case "name":
				$head = isset( $opts['post_heading'] ) ? $opts['post_heading'] : 'h3';
				$output .= '<div class="team-name">';
					$output .= '<' . esc_attr( $head ) . '><span class="client-name">'. esc_html( get_the_title() ) .'</span></' . esc_attr( $head ) . '>';
				$output .= '</div><!-- .team-name -->';		
			break;
			
			case "designation":
				$designation = get_post_meta( $opts['post_id'], 'tain_team_designation', true );
				if( $designation ) :
					
					$output .= '<div class="team-designation">';
						$output .= esc_html( $designation );
					$output .= '</div><!-- .team-designation -->';		
				endif;
			break;
			
			case "info":
				$output .= '<div class="team-info-wrap">';
					$output .= $this->tain_team_shortcode_elements( 'name', $opts );
					$output .= $this->tain_team_shortcode_elements( 'designation', $opts );
				$output .= '</div><!-- .team-info-wrap -->';	
			break;
			
			case "thumb":
				if ( has_post_thumbnail() ) {
				
					// Custom Thumb Code
					$thumb_size = $thumb_cond = $opts['thumb_size'];
					$cus_thumb_size = $opts['cus_thumb_size'];
					$hard_crop = $opts['hard_crop'];
					$custom_opt = $img_prop = '';
					if( $thumb_cond == 'custom' ){
						if( strpos( $cus_thumb_size, ";" ) ){
							$custom_opt = $cus_thumb_size != '' ? explode( "x", str_replace( ";", "", $cus_thumb_size ) ) : array();
							$img_prop = tain_get_custom_size_image( $custom_opt, $hard_crop );
							$thumb_size = array( $img_prop[1], $img_prop[2] );
						}else{
							$thumb_size = 'large';
							$thumb_cond = '';
						}
						
					}// Custom Thumb Code End
					
					$output .= '<div class="team-thumb">';
						$output .= isset( $opts['overlay'] ) ? $opts['overlay'] : '';
						$img_class = 'img-fluid';
						$img_class .= isset( $opts['image_shape'] ) ? ' '. $opts['image_shape'] : '';
						
						if( $thumb_cond == 'custom' ){
							$output .= '<img height="'. esc_attr( $img_prop[2] ) .'" width="'. esc_attr( $img_prop[1] ) .'" class="img-fluid" alt="'. esc_attr( get_the_title() ) .'" src="' . esc_url( $img_prop[0] ) . '"/>';
						}else{
							$output .= get_the_post_thumbnail( $opts['post_id'], $thumb_size, array( 'class' => 'img-fluid' ) );
						}
						
						if( isset( $opts['team_layout'] ) && $opts['team_layout'] == 'modern' ){
							$output .= '<span class="animate-bubble-box"></span>';
						}
					$output .= '</div><!-- .team-thumb -->';
				}
			break;
			
			case "excerpt":
				$excerpt = isset( $opts['excerpt_length'] ) && $opts['excerpt_length'] != '' ? $opts['excerpt_length'] : 20;
				$output .= '<div class="team-excerpt">';
					add_filter( 'excerpt_more', 'tain_excerpt_more' );
					add_filter( 'excerpt_length', array( &$this, 'tain_excerpt_length' ) );
					ob_start();
					the_excerpt();
					$excerpt_cont = ob_get_clean();
					$output .= $excerpt_cont;
				$output .= '</div><!-- .team-excerpt -->';	
			break;
			
			case "more":
				$read_more_text = isset( $opts['more_text'] ) ? $opts['more_text'] : esc_html__( 'Read more', 'tain-core' );
				$output = '<div class="post-more"><a class="read-more" href="'. esc_url( get_permalink( get_the_ID() ) ) . '">'. esc_html( $read_more_text ) .'</a></div>';
			break;
			
			case "social":
				$taget = get_post_meta( get_the_ID(), 'tain_team_link_target', true );
				$social_media = array( 
					'social-fb' => 'ti-facebook', 
					'social-twitter' => 'ti-twitter', 
					'social-instagram' => 'ti-instagram',
					'social-linkedin' => 'ti-linkedin', 
					'social-pinterest' => 'ti-pinterest',  
					'social-youtube' => 'ti-youtube', 
					'social-vimeo' => 'ti-vimeo',
					'social-flickr' => 'ti-flickr-alt', 
					'social-dribbble' => 'ti-dribbble'
				);
				$social_opt = array(
					'social-fb' => 'tain_team_facebook', 
					'social-twitter' => 'tain_team_twitter',
					'social-instagram' => 'tain_team_instagram',
					'social-linkedin' => 'tain_team_linkedin',
					'social-pinterest' => 'tain_team_pinterest',
					'social-youtube' => 'tain_team_youtube',
					'social-vimeo' => 'tain_team_vimeo',
					'social-flickr' => 'tain_team_flickr',
					'social-dribbble' => 'tain_team_dribbble',
				);
				// Actived social icons from theme option output generate via loop
				$li_output = '';
				foreach( $social_media as $key => $class ){
					$social_url = get_post_meta( get_the_ID(), $social_opt[$key], true );
					if( $social_url ):
						$li_output .= '<li>';
							$li_output .= '<a class="'. esc_attr( $key ) .'" href="'. esc_url( $social_url ) .'" target="'. esc_attr( $taget ) .'">';
								$li_output .= '<i class="'. esc_attr( $class ) .'"></i>';
							$li_output .= '</a>';
						$li_output .= '</li>';
					endif;
				}
				if( $li_output != '' ){
					$output .= '<div class="team-social-wrap clearfix">';
						$output .= '<ul class="nav social-icons team-social'. esc_attr( $opts['social_class'] ) .'">';
							$output .= $li_output;
						$output .= '</ul>';
					$output .= '</div> <!-- .team-social-wrap -->';
				}
			break;
			
		}
		return $output;
	}
	
	function tain_excerpt_length( $length ) {
		return $this->excerpt_len;
	}

}