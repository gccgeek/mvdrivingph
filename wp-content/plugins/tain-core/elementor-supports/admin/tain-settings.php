<?php 
add_action( 'admin_menu', 'tain_admin_menu' );
function tain_admin_menu() {
	add_submenu_page(
		'tain',
		esc_html__( 'Elementor Settings', 'tain' ),
		esc_html__( 'Elementor Settings', 'tain' ),
		'administrator',
		'tain-addons',
		'classic_elementor_addon_admin_page'
	);
}

function change_admin_menu_name(){
	global $submenu;
	if(isset($submenu['tain-addons'])){
		$submenu['tain-addons'][0][0] = esc_html__( 'Widgets Settings', 'tain-core' );
	}
}
add_action( 'admin_menu', 'change_admin_menu_name');    

function classic_elementor_addon_admin_page(){
	?>
	<div class="tain-admin-wrap">
		<h1><?php esc_html_e( 'Tain Elementor Addons', 'tain-core' ); ?></h1>
		<p class="pa-title-sub"><?php esc_html_e( 'Thank you for using Tain Elementor Addons for Elementor. This plugin has been developed by ', 'tain-core' ); ?><strong><?php echo esc_html( 'zozothemes' ); ?></strong></p>
		
		<?php
			$shortcode_stat = array(
				'animated-text' 	=> esc_html__( 'Animated Text', 'tain-core' ),
				'circle-progress'	=> esc_html__( 'Circle Progress', 'tain-core' ),
				'contact-form' 		=> esc_html__( 'Contact Form', 'tain-core' ),
				'contact-info' 		=> esc_html__( 'Contact Info', 'tain-core' ),
				'content-carousel' 	=> esc_html__( 'Content Carousel', 'tain-core' ),
				'counter' 			=> esc_html__( 'Counter', 'tain-core' ),
				'day-counter' 		=> esc_html__( 'Day Counter', 'tain-core' ),
				'feature-box' 		=> esc_html__( 'Feature Box', 'tain-core' ),
				'flip-box' 			=> esc_html__( 'Flip Box', 'tain-core' ),
				'google-map' 		=> esc_html__( 'Google Map', 'tain-core' ),
				'icon' 				=> esc_html__( 'Icon', 'tain-core' ),
				'icon-list' 		=> esc_html__( 'Icon List', 'tain-core' ),
				'image-grid' 		=> esc_html__( 'Image Grid', 'tain-core' ),
				'modal-popup' 		=> esc_html__( 'Modal Popup', 'tain-core' ),
				'pricing-table' 	=> esc_html__( 'Pricing Table', 'tain-core' ),
				'section-title' 	=> esc_html__( 'Section Title', 'tain-core' ),
				'social-links' 		=> esc_html__( 'Social Links', 'tain-core' ),
				'timeline' 			=> esc_html__( 'Timeline', 'tain-core' ),
				'timeline-slide' 	=> esc_html__( 'Timeline Slide', 'tain-core' ),
				'chart' 			=> esc_html__( 'Chart', 'tain-core' ),
				'recent-popular' 	=> esc_html__( 'Recent/Popular Post', 'tain-core' ),
				'blog' 				=> esc_html__( 'Blog', 'tain-core' ),
				'portfolio' 		=> esc_html__( 'Portfolio', 'tain-core' ),
				'team' 				=> esc_html__( 'Team', 'tain-core' ),
				'event' 			=> esc_html__( 'Event', 'tain-core' ),
				'course' 			=> esc_html__( 'Course', 'tain-core' ),
				'testimonial' 		=> esc_html__( 'Testimonial', 'tain-core' ),
				'toggle-content' 	=> esc_html__( 'Toggle Content', 'tain-core' ),
				'mailchimp' 		=> esc_html__( 'Mailchimp', 'tain-core' ),
				'popup-anything' 	=> esc_html__( 'Popup Anything', 'tain-core' ),
				'popover' 			=> esc_html__( 'Popover', 'tain-core' ),
				'round-tab' 		=> esc_html__( 'Round Tab', 'tain-core' )
			);
			
			if ( isset( $_POST['save_tain_shortcodes_options'] ) && wp_verify_nonce( $_POST['save_tain_shortcodes_options'], 'tain_plugin_shortcodes_options' ) ) {
				update_option( 'tain_shortcodes', $_POST['tain_shortcodes'] );
			}
			$tain_shortcodes = get_option('tain_shortcodes');
			
		?>
		
		<div class="tain-admin-content-wrap">
			<form method="post" action="#" enctype="multipart/form-data" id="tain-plugin-form-wrapper">
				<?php wp_nonce_field( 'tain_plugin_shortcodes_options', 'save_tain_shortcodes_options' ); ?>
				<input class="tain-plugin-submit button button-primary" type="submit" value="<?php echo esc_attr__( 'Save', 'tain-core' ); ?>" />
				<div class="tain-shortcodes-container">
			<?php
				$row = 1;
				foreach( $shortcode_stat as $key => $value ){
				
					$shortcode_name = str_replace( "-", "_", $key );
					if( !empty( $tain_shortcodes ) ){
						if( isset( $tain_shortcodes[$shortcode_name] ) ){
							$saved_val = 'on';
						}else{
							$saved_val = 'off';
						}
					}else{
						$saved_val = 'on';
					}
					$checked_stat = $saved_val == 'on' ? 'checked="checked"' : '';
				
					if( $row % 4 == 1 ) echo '<div class="tain-row">';
					
						echo '
						<div class="tain-col-3">
							<div class="element-group">
								<h4>'. esc_html( $value ) .'</h4>
								<label class="switch">
									<input class="switch-checkbox" type="checkbox" name="tain_shortcodes['. esc_attr( $shortcode_name ) .']" '. $checked_stat .'>
									<span class="slider round"></span>
								</label>
							</div><!-- .element-group -->
						</div><!-- .tain-col-2 -->';
									
					if( $row % 4 == 0 ) echo '</div><!-- .tain-row -->';
					$row++;
				}
				
				if( $row % 4 != 1 ) echo '</div><!-- .tain-row unexpceted close -->';
			?>
				</div> <!-- .tain-shortcodes-container -->
			</form>
		</div><!-- .tain-admin-content-wrap -->
		
		<div class="tain-customizer-options-wrap">
			<h2><?php esc_html_e( 'Enable/Disable Customizer Auto Refresh Option', 'tain-core' ); ?></h2>
			<?php 
				$customizer_auto_load = get_option( 'tain_customizer_auto_load' );;
				$checked_stat = $customizer_auto_load == '1' ? 'checked="checked"' : '';
			?>
			<div class="tain-customizer-option">
				<label class="switch">
					<input class="switch-checkbox" type="checkbox" <?php echo ''. $checked_stat ?>>
					<span class="slider round"></span>
				</label>
			</div>
			<p><?php esc_html_e( 'If you want to live editor experience, Just turn on this option. No need to auto load customizer editor for every option change means turn off this option.' ); ?></p>
		</div><!-- .tain-customizer-options-wrap -->
		
	</div><!-- .tain-admin-wrap -->
	<?php
}

add_action('wp_ajax_tain-customizer-auto-load', 'tain_customizer_auto_load_option');
function tain_customizer_auto_load_option(){
	$nonce = $_POST['nonce'];
  
    if ( ! wp_verify_nonce( $nonce, 'tain-customizer-#$%&*(' ) )
        die ( esc_html__( 'Busted!', 'tain' ) );
	
	$auto_load = isset( $_POST['auto_load'] ) && $_POST['auto_load'] == '1' ? 1 : 0;
	update_option( 'tain_customizer_auto_load', $auto_load );
	echo 'success';
	exit;
}