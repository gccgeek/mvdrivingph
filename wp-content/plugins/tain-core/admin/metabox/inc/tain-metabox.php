<?php
/* Tain Page Options */
$prefix = 'tain_post_';
$fields = array(
	array( 
		'label'	=> esc_html__( 'Post General Settings', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are single post general settings.', 'tain-core' ), 
		'tab'	=> esc_html__( 'General', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Post Layout', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose post layout for current post single view.', 'tain-core' ), 
		'id'	=> $prefix.'layout',
		'tab'	=> esc_html__( 'General', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'wide' => esc_html__( 'Wide', 'tain-core' ),
			'boxed' => esc_html__( 'Boxed', 'tain-core' )			
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Post Content Padding Option', 'tain-core' ),
		'id'	=> $prefix.'content_padding_opt',
		'tab'	=> esc_html__( 'General', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'		
	),
	array( 
		'label'	=> esc_html__( 'Post Content Padding', 'tain-core' ), 
		'desc'	=> esc_html__( 'Set the top/right/bottom/left padding of post content.', 'tain-core' ),
		'id'	=> $prefix.'content_padding',
		'tab'	=> esc_html__( 'General', 'tain-core' ),
		'type'	=> 'space',
		'required'	=> array( $prefix.'content_padding_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Post Template Option', 'tain-core' ),
		'id'	=> $prefix.'template_opt',
		'tab'	=> esc_html__( 'General', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'		
	),
	array( 
		'label'	=> esc_html__( 'Post Template', 'tain-core' ),
		'id'	=> $prefix.'template',
		'tab'	=> esc_html__( 'General', 'tain-core' ),
		'type'	=> 'image_select',
		'options' => array(
			'no-sidebar'	=> get_theme_file_uri( '/assets/images/page-layouts/1.png' ), 
			'right-sidebar'	=> get_theme_file_uri( '/assets/images/page-layouts/2.png' ), 
			'left-sidebar'	=> get_theme_file_uri( '/assets/images/page-layouts/3.png' ), 
			'both-sidebar'	=> get_theme_file_uri( '/assets/images/page-layouts/4.png' ), 
		),
		'default'	=> 'right-sidebar',
		'required'	=> array( $prefix.'template_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Left Sidebar', 'tain-core' ),
		'id'	=> $prefix.'left_sidebar',
		'tab'	=> esc_html__( 'General', 'tain-core' ),
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'template_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Right Sidebar', 'tain-core' ),
		'id'	=> $prefix.'right_sidebar',
		'tab'	=> esc_html__( 'General', 'tain-core' ),
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'template_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Sidebar On Mobile', 'tain-core' ),
		'id'	=> $prefix.'sidebar_mobile',
		'tab'	=> esc_html__( 'General', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'1' => esc_html__( 'Show', 'tain-core' ),
			'0' => esc_html__( 'Hide', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Featured Slider', 'tain-core' ),
		'id'	=> $prefix.'featured_slider',
		'tab'	=> esc_html__( 'General', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'1' => esc_html__( 'Enable', 'tain-core' ),
			'0' => esc_html__( 'Disable', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Full Width Wrap', 'tain-core' ),
		'id'	=> $prefix.'full_wrap',
		'tab'	=> esc_html__( 'General', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'1' => esc_html__( 'Enable', 'tain-core' ),
			'0' => esc_html__( 'Disable', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Post Items Option', 'tain-core' ),
		'id'	=> $prefix.'items_opt',
		'tab'	=> esc_html__( 'General', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'		
	),
	array( 
		'label'	=> esc_html__( 'Post Items', 'tain-core' ),
		'desc'	=> esc_html__( 'Needed single post items drag from disabled and put enabled part.', 'tain-core' ),
		'id'	=> $prefix.'items',
		'tab'	=> esc_html__( 'General', 'tain-core' ),
		'type'	=> 'dragdrop_multi',
		'dd_fields' => array ( 
			'Enabled'  => array(
				'title' 	=> esc_html__( 'Title', 'tain-core' ),
				'top-meta'	=> esc_html__( 'Top Meta', 'tain-core' ),
				'thumb' 	=> esc_html__( 'Thumbnail', 'tain-core' ),
				'content' 	=> esc_html__( 'Content', 'tain-core' ),
				'bottom-meta'		=> esc_html__( 'Bottom Meta', 'tain-core' )
			),
			'disabled' => array()
		),
		'required'	=> array( $prefix.'items_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Post Overlay', 'tain-core' ),
		'id'	=> $prefix.'overlay_opt',
		'tab'	=> esc_html__( 'General', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'1' => esc_html__( 'Enable', 'tain-core' ),
			'0' => esc_html__( 'Disable', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Post Overlay Items', 'tain-core' ),
		'desc'	=> esc_html__( 'Needed overlay post items drag from disabled and put enabled part.', 'tain-core' ),
		'id'	=> $prefix.'overlay_items',
		'tab'	=> esc_html__( 'General', 'tain-core' ),
		'type'	=> 'dragdrop_multi',
		'dd_fields' => array ( 
			'Enabled'  => array(
				'title' 	=> esc_html__( 'Title', 'tain-core' )
			),
			'disabled' => array(
				'top-meta'	=> esc_html__( 'Top Meta', 'tain-core' ),
				'bottom-meta'		=> esc_html__( 'Bottom Meta', 'tain-core' )
			)
		),
		'required'	=> array( $prefix.'overlay_opt', '1' )
	),
	array( 
		'label'	=> esc_html__( 'Post Page Items Option', 'tain-core' ),
		'id'	=> $prefix.'page_items_opt',
		'tab'	=> esc_html__( 'General', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'		
	),
	array( 
		'label'	=> esc_html__( 'Post Page Items', 'tain-core' ),
		'desc'	=> esc_html__( 'Needed post page items drag from disabled and put enabled part.', 'tain-core' ),
		'id'	=> $prefix.'page_items',
		'tab'	=> esc_html__( 'General', 'tain-core' ),
		'type'	=> 'dragdrop_multi',
		'dd_fields' => array ( 
			'Enabled'  => array(
				'post-items' 	=> esc_html__( 'Post Items', 'tain-core' ),
				'author-info'	=> esc_html__( 'Author Info', 'tain-core' ),
				'related-slider'=> esc_html__( 'Related Slider', 'tain-core' ),
				'post-nav' 	=> esc_html__( 'Post Nav', 'tain-core' ),
				'comment' 	=> esc_html__( 'Comment', 'tain-core' )
			),
			'disabled' => array()
		),
		'default'	=> 'post-items,author-info,related-slider,post-nav,comment',
		'required'	=> array( $prefix.'page_items_opt', 'custom' )
	),
	//Header
	array( 
		'label'	=> esc_html__( 'Header General Settings', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header general settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Header Layout', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose post layout for current post header layout.', 'tain-core' ), 
		'id'	=> $prefix.'header_layout',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'wide' => esc_html__( 'Wide', 'tain-core' ),
			'boxed' => esc_html__( 'Boxed', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Type', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose post layout for current post header type.', 'tain-core' ), 
		'id'	=> $prefix.'header_type',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'default' => esc_html__( 'Default', 'tain-core' ),
			'left-sticky' => esc_html__( 'Left Sticky', 'tain-core' ),
			'right-sticky' => esc_html__( 'Right Sticky', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Background Image', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header background image for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'image',
		'id'	=> $prefix.'header_bg_img',
		'required'	=> array( $prefix.'header_type', 'default' )
	),
	array( 
		'label'	=> esc_html__( 'Header Items Options', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header items options for enable header drag and drop items.', 'tain-core' ), 
		'id'	=> $prefix.'header_items_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Items', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header general items for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'dragdrop_multi',
		'id'	=> $prefix.'header_items',
		'dd_fields' => array ( 
			'Normal' => array( 
				'header-topbar' 	=> esc_html__( 'Topbar', 'tain-core' ),
				'header-logo'	=> esc_html__( 'Logo Bar', 'tain-core' )
			),
			'Sticky' => array( 
				'header-nav' 	=> esc_html__( 'Navbar', 'tain-core' )
			),
			'disabled' => array()
		),
		'required'	=> array( $prefix.'header_items_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Absolute Option', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header absolute to change header look transparent.', 'tain-core' ), 
		'id'	=> $prefix.'header_absolute_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'1' => esc_html__( 'Enable', 'tain-core' ),
			'0' => esc_html__( 'Disable', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header sticky options.', 'tain-core' ), 
		'id'	=> $prefix.'header_sticky_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'sticky' => esc_html__( 'Header Sticky Part', 'tain-core' ),
			'sticky-scroll' => esc_html__( 'Sticky Scroll Up', 'tain-core' ),
			'none' => esc_html__( 'None', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header topbar settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Options', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header items options for enable header drag and drop items.', 'tain-core' ), 
		'id'	=> $prefix.'header_topbar_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Height', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header topbar height for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'dimension',
		'id'	=> $prefix.'header_topbar_height',
		'property' => 'height',
		'required'	=> array( $prefix.'header_topbar_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Sticky Height', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header topbar sticky height for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'dimension',
		'id'	=> $prefix.'header_topbar_sticky_height',
		'property' => 'height',
		'required'	=> array( $prefix.'header_topbar_opt', 'custom' )
	),
	array( 
		'label'	=> '',
		'desc'	=> esc_html__( 'These all are header topbar skin settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Skin Settings', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header topbar skin settings options.', 'tain-core' ), 
		'id'	=> $prefix.'header_topbar_skin_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Font Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header topbar font color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'color',
		'id'	=> $prefix.'header_topbar_font',
		'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Background', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header topbar background color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'header_topbar_bg',
		'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Link Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header topbar link color settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'header_topbar_link',
		'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Border', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header topbar border settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'header_topbar_border',
		'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Padding', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header topbar padding settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'space',
		'id'	=> $prefix.'header_topbar_padding',
		'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Sticky Skin Settings', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header top bar sticky skin settings options.', 'tain-core' ), 
		'id'	=> $prefix.'header_topbar_sticky_skin_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Sticky Font Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header top bar sticky font color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'color',
		'id'	=> $prefix.'header_topbar_sticky_font',
		'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Sticky Background', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header top bar sticky background color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'header_topbar_sticky_bg',
		'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Sticky Link Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header top bar sticky link color settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'header_topbar_sticky_link',
		'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Sticky Border', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header top bar sticky border settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'header_topbar_sticky_border',
		'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Sticky Padding', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header top bar sticky padding settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'space',
		'id'	=> $prefix.'header_topbar_sticky_padding',
		'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Items Option', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header topbar items enable options.', 'tain-core' ), 
		'id'	=> $prefix.'header_topbar_items_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Top Bar Items', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header topbar items for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'dragdrop_multi',
		'id'	=> $prefix.'header_topbar_items',
		'dd_fields' => array ( 
			'Left'  => array(
				'header-topbar-date' => esc_html__( 'Date', 'tain-core' ),						
			),
			'Center' => array(),
			'Right' => array(),
			'disabled' => array(
				'header-topbar-text-1'	=> esc_html__( 'Custom Text 1', 'tain-core' ),
				'header-topbar-text-2'	=> esc_html__( 'Custom Text 2', 'tain-core' ),
				'header-topbar-text-3'	=> esc_html__( 'Custom Text 3', 'tain-core' ),
				'header-topbar-menu'    => esc_html__( 'Top Bar Menu', 'tain-core' ),
				'header-topbar-social'	=> esc_html__( 'Social', 'tain-core' ),
				'header-topbar-search'	=> esc_html__( 'Search', 'tain-core' )
			)
		),
		'required'	=> array( $prefix.'header_topbar_items_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header logo bar settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Options', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header items options for enable header drag and drop items.', 'tain-core' ), 
		'id'	=> $prefix.'header_logo_bar_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Height', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header logo bar height for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'dimension',
		'id'	=> $prefix.'header_logo_bar_height',
		'property' => 'height',
		'required'	=> array( $prefix.'header_logo_bar_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Sticky Height', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header logo bar sticky height for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'dimension',
		'id'	=> $prefix.'header_logo_bar_sticky_height',
		'property' => 'height',
		'required'	=> array( $prefix.'header_logo_bar_opt', 'custom' )
	),
	array( 
		'label'	=> '',
		'desc'	=> esc_html__( 'These all are header logo bar skin settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Skin Settings', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header logo bar skin settings options.', 'tain-core' ), 
		'id'	=> $prefix.'header_logo_bar_skin_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Font Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header logo bar font color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'color',
		'id'	=> $prefix.'header_logo_bar_font',
		'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Background', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header logo bar background color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'header_logo_bar_bg',
		'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Link Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header logo bar link color settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'header_logo_bar_link',
		'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Border', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header logo bar border settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'header_logo_bar_border',
		'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Padding', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header logo bar padding settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'space',
		'id'	=> $prefix.'header_logo_bar_padding',
		'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Sticky Skin Settings', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header logo bar sticky skin settings options.', 'tain-core' ), 
		'id'	=> $prefix.'header_logobar_sticky_skin_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Sticky Font Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header logo bar sticky font color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'color',
		'id'	=> $prefix.'header_logobar_sticky_font',
		'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Sticky Background', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header logo bar sticky background color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'header_logobar_sticky_bg',
		'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Sticky Link Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header logo bar sticky link color settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'header_logobar_sticky_link',
		'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Sticky Border', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header logo bar sticky border settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'header_logobar_sticky_border',
		'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Sticky Padding', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header logo bar sticky padding settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'space',
		'id'	=> $prefix.'header_logobar_sticky_padding',
		'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Items Option', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header logo bar items enable options.', 'tain-core' ), 
		'id'	=> $prefix.'header_logo_bar_items_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Logo Bar Items', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header logo bar items for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'dragdrop_multi',
		'id'	=> $prefix.'header_logo_bar_items',
		'dd_fields' => array ( 
			'Left'  => array(
				'header-logobar-logo'		=> esc_html__( 'Logo', 'tain-core' ),
				'header-logobar-sticky-logo' => esc_html__( 'Sticky Logo', 'tain-core' )											
			),
			'Center' => array(),
			'Right' => array(),
			'disabled' => array(
				'header-logobar-social'		=> esc_html__( 'Social', 'tain-core' ),
				'header-logobar-search'		=> esc_html__( 'Search', 'tain-core' ),
				'header-logobar-secondary-toggle'	=> esc_html__( 'Secondary Toggle', 'tain-core' ),	
				'header-phone'   			=> esc_html__( 'Phone Number', 'tain-core' ),
				'header-address'  			=> esc_html__( 'Address Text', 'tain-core' ),
				'header-email'   			=> esc_html__( 'Email', 'tain-core' ),
				'header-logobar-menu'   	=> esc_html__( 'Main Menu', 'tain-core' ),
				'header-logobar-search-toggle'	=> esc_html__( 'Search Toggle', 'tain-core' ),
				'header-logobar-text-1'		=> esc_html__( 'Custom Text 1', 'tain-core' ),
				'header-logobar-text-2'		=> esc_html__( 'Custom Text 2', 'tain-core' ),
				'header-logobar-text-3'		=> esc_html__( 'Custom Text 3', 'tain-core' ),	
				'header-cart'   			=> esc_html__( 'Cart', 'tain-core' ),
				'header-wishlist'   		=> esc_html__( 'Wishlist', 'tain-core' ),
				'multi-info'   				=> esc_html__( 'Address, Phone, Email', 'tain-core' )
			)
		),
		'required'	=> array( $prefix.'header_logo_bar_items_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header navbar settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Options', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header items options for enable header drag and drop items.', 'tain-core' ), 
		'id'	=> $prefix.'header_navbar_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Height', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header navbar height for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'dimension',
		'id'	=> $prefix.'header_navbar_height',
		'property' => 'height',
		'required'	=> array( $prefix.'header_navbar_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Sticky Height', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header navbar sticky height for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'dimension',
		'id'	=> $prefix.'header_navbar_sticky_height',
		'property' => 'height',
		'required'	=> array( $prefix.'header_navbar_opt', 'custom' )
	),
	array( 
		'label'	=> '',
		'desc'	=> esc_html__( 'These all are header navbar skin settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Skin Settings', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header navbar skin settings options.', 'tain-core' ), 
		'id'	=> $prefix.'header_navbar_skin_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Font Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header navbar font color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'color',
		'id'	=> $prefix.'header_navbar_font',
		'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Background', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header navbar background color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'header_navbar_bg',
		'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Link Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header navbar link color settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'header_navbar_link',
		'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Border', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header navbar border settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'header_navbar_border',
		'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Padding', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header navbar padding settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'space',
		'id'	=> $prefix.'header_navbar_padding',
		'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Sticky Skin Settings', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header navbar sticky skin settings options.', 'tain-core' ), 
		'id'	=> $prefix.'header_navbar_sticky_skin_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Sticky Font Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header navbar sticky font color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'color',
		'id'	=> $prefix.'header_navbar_sticky_font',
		'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Sticky Background', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header navbar sticky background color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'header_navbar_sticky_bg',
		'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Sticky Link Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header navbar sticky link color settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'header_navbar_sticky_link',
		'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Sticky Border', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header navbar sticky border settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'header_navbar_sticky_border',
		'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Sticky Padding', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header navbar sticky padding settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'space',
		'id'	=> $prefix.'header_navbar_sticky_padding',
		'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Items Option', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header navbar items enable options.', 'tain-core' ), 
		'id'	=> $prefix.'header_navbar_items_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Navbar Items', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header navbar items for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'dragdrop_multi',
		'id'	=> $prefix.'header_navbar_items',
		'dd_fields' => array ( 
			'Left'  => array(											
				'header-navbar-menu'    => esc_html__( 'Main Menu', 'tain-core' ),
			),
			'Center' => array(
			),
			'Right' => array(
				'header-navbar-search'	=> esc_html__( 'Search', 'tain-core' ),
			),
			'disabled' => array(
				'header-navbar-text-1'	=> esc_html__( 'Custom Text 1', 'tain-core' ),
				'header-navbar-text-2'	=> esc_html__( 'Custom Text 2', 'tain-core' ),
				'header-navbar-text-3'	=> esc_html__( 'Custom Text 3', 'tain-core' ),
				'header-navbar-logo'	=> esc_html__( 'Logo', 'tain-core' ),
				'header-navbar-social'	=> esc_html__( 'Social', 'tain-core' ),
				'header-navbar-secondary-toggle'	=> esc_html__( 'Secondary Toggle', 'tain-core' ),
				'header-navbar-search-toggle'	=> esc_html__( 'Search Toggle', 'tain-core' ),
				'header-navbar-sticky-logo'	=> esc_html__( 'Stikcy Logo', 'tain-core' ),
				'header-cart'   		=> esc_html__( 'Cart', 'tain-core' )
			)
		),
		'required'	=> array( $prefix.'header_navbar_items_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header sticky settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Options', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header sticky part option.', 'tain-core' ), 
		'id'	=> $prefix.'header_stikcy_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Width', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header Sticky part width for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'dimension',
		'id'	=> $prefix.'header_stikcy_width',
		'property' => 'width',
		'required'	=> array( $prefix.'header_stikcy_opt', 'custom' )
	),
	array( 
		'label'	=> '',
		'desc'	=> esc_html__( 'These all are header Sticky skin settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Skin Settings', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header Sticky skin settings options.', 'tain-core' ), 
		'id'	=> $prefix.'header_stikcy_skin_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Font Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header Sticky font color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'color',
		'id'	=> $prefix.'header_stikcy_font',
		'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Background', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header Sticky background color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'header_stikcy_bg',
		'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Link Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header Sticky link color settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'header_stikcy_link',
		'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Border', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header Sticky border settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'space',

		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'header_stikcy_border',
		'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Padding', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header Sticky padding settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'space',
		'id'	=> $prefix.'header_stikcy_padding',
		'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Items Option', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose header Sticky items enable options.', 'tain-core' ), 
		'id'	=> $prefix.'header_stikcy_items_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Header Sticky/Fixed Part Items', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are header Sticky items for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'dragdrop_multi',
		'id'	=> $prefix.'header_stikcy_items',
		'dd_fields' => array ( 
			'Top'  => array(
				'header-fixed-logo' => esc_html__( 'Logo', 'tain-core' )
			),
			'Middle'  => array(
				'header-fixed-menu'	=> esc_html__( 'Menu', 'tain-core' )					
			),
			'Bottom'  => array(
				'header-fixed-social'	=> esc_html__( 'Social', 'tain-core' )					
			),
			'disabled' => array(
				'header-fixed-text-1'	=> esc_html__( 'Custom Text 1', 'tain-core' ),
				'header-fixed-text-2'	=> esc_html__( 'Custom Text 2', 'tain-core' ),				
				'header-fixed-search'	=> esc_html__( 'Search Form', 'tain-core' )
			)
		),
		'required'	=> array( $prefix.'header_stikcy_items_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Bar', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are post title bar settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Post Title Option', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose post title enable or disable.', 'tain-core' ), 
		'id'	=> $prefix.'header_post_title_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'1' => esc_html__( 'Enable', 'tain-core' ),
			'0' => esc_html__( 'Disable', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Post Title Text', 'tain-core' ),
		'desc'	=> esc_html__( 'If this post title is empty, then showing current post default title.', 'tain-core' ), 
		'id'	=> $prefix.'header_post_title_text',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'text',
		'default'	=> '',
		'required'	=> array( $prefix.'header_post_title_opt', '1' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Description', 'tain-core' ),
		'desc'	=> esc_html__( 'Enter post title description.', 'tain-core' ), 
		'id'	=> $prefix.'header_post_title_desc',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'textarea',
		'default'	=> '',
		'required'	=> array( $prefix.'header_post_title_opt', '1' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Background Parallax', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose post title background parallax.', 'tain-core' ), 
		'id'	=> $prefix.'header_post_title_parallax',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'1' => esc_html__( 'Enable', 'tain-core' ),
			'0' => esc_html__( 'Disable', 'tain-core' )
		),
		'default'	=> 'theme-default',
		'required'	=> array( $prefix.'header_post_title_opt', '1' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Background Video Option', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose post title background video option.', 'tain-core' ), 
		'id'	=> $prefix.'header_post_title_video_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'1' => esc_html__( 'Enable', 'tain-core' ),
			'0' => esc_html__( 'Disable', 'tain-core' )
		),
		'default'	=> 'theme-default',
		'required'	=> array( $prefix.'header_post_title_opt', '1' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Background Video', 'tain-core' ),
		'desc'	=> esc_html__( 'Enter youtube video ID. Example: ZSt9tm3RoUU.', 'tain-core' ), 
		'id'	=> $prefix.'header_post_title_video',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'text',
		'default'	=> '',
		'required'	=> array( $prefix.'header_post_title_video_opt', '1' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Bar Items Option', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose post title bar items option.', 'tain-core' ), 
		'id'	=> $prefix.'post_title_items_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default',
		'required'	=> array( $prefix.'header_post_title_opt', '1' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Bar Items', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are post title bar items for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'dragdrop_multi',
		'id'	=> $prefix.'post_title_items',
		'dd_fields' => array ( 
			'Left'  => array(
				'title' => esc_html__( 'Post Title Text', 'tain-core' ),
			),
			'Center'  => array(
				
			),
			'Right'  => array(
				'breadcrumb'	=> esc_html__( 'Breadcrumb', 'tain-core' )
			),
			'disabled' => array()
		),
		'required'	=> array( $prefix.'post_title_items_opt', 'custom' )
	),
	array( 
		'label'	=> '',
		'desc'	=> esc_html__( 'These all are post title skin settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'label',
		'required'	=> array( $prefix.'header_post_title_opt', '1' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Skin Settings', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose post title skin settings options.', 'tain-core' ), 
		'id'	=> $prefix.'post_title_skin_opt',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default',
		'required'	=> array( $prefix.'header_post_title_opt', '1' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Font Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are post title font color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'color',
		'id'	=> $prefix.'post_title_font',
		'required'	=> array( $prefix.'post_title_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Background Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are post title background color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'post_title_bg',
		'required'	=> array( $prefix.'post_title_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Background Image', 'tain-core' ),
		'desc'	=> esc_html__( 'Enter post title background image url.', 'tain-core' ), 
		'id'	=> $prefix.'post_title_bg_img',
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'url',
		'default'	=> '',
		'required'	=> array( $prefix.'post_title_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Link Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are post title link color settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'post_title_link',
		'required'	=> array( $prefix.'post_title_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Border', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are post title border settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'post_title_border',
		'required'	=> array( $prefix.'post_title_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Padding', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are post title padding settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'space',
		'id'	=> $prefix.'post_title_padding',
		'required'	=> array( $prefix.'post_title_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Post Title Overlay', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are post title overlay color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Header', 'tain-core' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'post_title_overlay',
		'required'	=> array( $prefix.'post_title_skin_opt', 'custom' )
	),
	//Footer
	array( 
		'label'	=> 'Footer General',
		'desc'	=> esc_html__( 'These all are header footer settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Footer Layout', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose footer layout for current post.', 'tain-core' ), 
		'id'	=> $prefix.'footer_layout',
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'wide' => esc_html__( 'Wide', 'tain-core' ),
			'boxed' => esc_html__( 'Boxed', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Hidden Footer', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose hidden footer option.', 'tain-core' ), 
		'id'	=> $prefix.'hidden_footer',
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'1' => esc_html__( 'Enable', 'tain-core' ),
			'0' => esc_html__( 'Disable', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> '',
		'desc'	=> esc_html__( 'These all are footer skin settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Footer Skin Settings', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose footer skin settings options.', 'tain-core' ), 
		'id'	=> $prefix.'footer_skin_opt',
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Footer Font Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer font color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'color',
		'id'	=> $prefix.'footer_font',
		'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Background Image', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose footer background image for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'image',
		'id'	=> $prefix.'footer_bg_img',
		'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Background Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer background color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'color',
		'id'	=> $prefix.'footer_bg',
		'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Background Overlay', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer background overlay color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'footer_bg_overlay',
		'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Link Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer link color settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'footer_link',
		'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Border', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer border settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'footer_border',
		'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Padding', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer padding settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'space',
		'id'	=> $prefix.'footer_padding',
		'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Items Option', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose footer items enable options.', 'tain-core' ), 
		'id'	=> $prefix.'footer_items_opt',
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Footer Items', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer items for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'dragdrop_multi',
		'id'	=> $prefix.'footer_items',
		'dd_fields' => array ( 
			'Enabled'  => array(
				'footer-bottom'	=> esc_html__( 'Footer Bottom', 'tain-core' )
			),
			'disabled' => array(
				'footer-top' => esc_html__( 'Footer Top', 'tain-core' ),
				'footer-middle'	=> esc_html__( 'Footer Middle', 'tain-core' )
			)
		),
		'required'	=> array( $prefix.'footer_items_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Top',
		'desc'	=> esc_html__( 'These all are footer top settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Footer Top Skin', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose footer top skin options.', 'tain-core' ), 
		'id'	=> $prefix.'footer_top_skin_opt',
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Footer Top Font Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer top font color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'color',
		'id'	=> $prefix.'footer_top_font',
		'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Top Widget Title color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer top widget title color.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'color',
		'id'	=> $prefix.'footer_top_widget_title_color',
		'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Top Background', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer background color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'footer_top_bg',
		'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Top Link Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer top link color settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'footer_top_link',
		'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Top Border', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer top border settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'footer_top_border',
		'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Top Padding', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer top padding settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'space',
		'id'	=> $prefix.'footer_top_padding',
		'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Top Columns and Sidebars Settings',
		'desc'	=> esc_html__( 'These all are footer top columns and sidebar settings.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Footer Layout Option', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose footer layout option.', 'tain-core' ), 
		'id'	=> $prefix.'footer_top_layout_opt',
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Footer Layout', 'tain-core' ),
		'id'	=> $prefix.'footer_top_layout',
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'image_select',
		'options' => array(
			'3-3-3-3'	=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-1.png',
			'4-4-4'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-2.png',
			'3-6-3'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-3.png',
			'6-6'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-4.png',
			'9-3'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-5.png',
			'3-9'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-6.png',
			'4-2-2-2-2'	=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-8.png',
			'6-2-2-2'	=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-9.png',
			'12'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-7.png'
		),
		'default'	=> '4-4-4',
		'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer First Column',
		'desc'	=> esc_html__( 'Select footer first column widget.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'id'	=> $prefix.'footer_top_sidebar_1',
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Second Column',
		'desc'	=> esc_html__( 'Select footer second column widget.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'id'	=> $prefix.'footer_top_sidebar_2',
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Third Column',
		'desc'	=> esc_html__( 'Select footer third column widget.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'id'	=> $prefix.'footer_top_sidebar_3',
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Fourth Column',
		'desc'	=> esc_html__( 'Select footer fourth column widget.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'id'	=> $prefix.'footer_top_sidebar_4',
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Middle',
		'desc'	=> esc_html__( 'These all are footer middle settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Footer Middle Skin', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose footer middle skin options.', 'tain-core' ), 
		'id'	=> $prefix.'footer_middle_skin_opt',
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Footer Middle Font Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer middle font color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'color',
		'id'	=> $prefix.'footer_middle_font',
		'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Middle Widget Title Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer middle widget title color.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'color',
		'id'	=> $prefix.'footer_middle_widget_title_color',
		'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Middle Background', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer background color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'footer_middle_bg',
		'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Middle Link Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer middle link color settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'footer_middle_link',
		'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Middle Border', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer middle border settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'footer_middle_border',
		'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Middle Padding', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer middle padding settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'space',
		'id'	=> $prefix.'footer_middle_padding',
		'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Middle Columns and Sidebars Settings',
		'desc'	=> esc_html__( 'These all are footer middle columns and sidebar settings.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Footer Layout Option', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose footer layout option.', 'tain-core' ), 
		'id'	=> $prefix.'footer_middle_layout_opt',
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Footer Layout', 'tain-core' ),
		'id'	=> $prefix.'footer_middle_layout',
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'image_select',
		'options' => array(
			'3-3-3-3'	=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-1.png',
			'4-4-4'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-2.png',
			'3-6-3'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-3.png',
			'6-6'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-4.png',
			'9-3'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-5.png',
			'3-9'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-6.png',
			'4-2-2-2-2'	=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-8.png',
			'6-2-2-2'	=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-9.png',
			'12'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-7.png'
		),
		'default'	=> '4-4-4',
		'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer First Column',
		'desc'	=> esc_html__( 'Select footer first column widget.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'id'	=> $prefix.'footer_middle_sidebar_1',
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Second Column',
		'desc'	=> esc_html__( 'Select footer second column widget.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'id'	=> $prefix.'footer_middle_sidebar_2',
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Third Column',
		'desc'	=> esc_html__( 'Select footer third column widget.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'id'	=> $prefix.'footer_middle_sidebar_3',
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Fourth Column',
		'desc'	=> esc_html__( 'Select footer fourth column widget.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'id'	=> $prefix.'footer_middle_sidebar_4',
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
	),
	array( 
		'label'	=> 'Footer Bottom',
		'desc'	=> esc_html__( 'These all are footer bottom settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Fixed', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose footer bottom fixed option.', 'tain-core' ), 
		'id'	=> $prefix.'footer_bottom_fixed',
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'1' => esc_html__( 'Enable', 'tain-core' ),
			'0' => esc_html__( 'Disable', 'tain-core' )			
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> '',
		'desc'	=> esc_html__( 'These all are footer bottom skin settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Skin', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose footer bottom skin options.', 'tain-core' ), 
		'id'	=> $prefix.'footer_bottom_skin_opt',
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Font Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer bottom font color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'color',
		'id'	=> $prefix.'footer_bottom_font',
		'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Background', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer bottom background color for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'alpha_color',
		'id'	=> $prefix.'footer_bottom_bg',
		'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Link Color', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer bottom link color settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'link_color',
		'id'	=> $prefix.'footer_bottom_link',
		'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Border', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer bottom border settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'space',
		'color' => 1,
		'border_style' => 1,
		'id'	=> $prefix.'footer_bottom_border',
		'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Padding', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer bottom padding settings for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'space',
		'id'	=> $prefix.'footer_bottom_padding',
		'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Widget Option', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose footer bottom widget options.', 'tain-core' ), 
		'id'	=> $prefix.'footer_bottom_widget_opt',
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> 'Footer Bottom Widget',
		'desc'	=> esc_html__( 'Select footer bottom widget.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'id'	=> $prefix.'footer_bottom_widget',
		'type'	=> 'sidebar',
		'required'	=> array( $prefix.'footer_bottom_widget_opt', 'custom' )
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Items Option', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose footer bottom items options.', 'tain-core' ), 
		'id'	=> $prefix.'footer_bottom_items_opt',
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'custom' => esc_html__( 'Custom', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Footer Bottom Items', 'tain-core' ),
		'desc'	=> esc_html__( 'These all are footer bottom items for current post.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Footer', 'tain-core' ),
		'type'	=> 'dragdrop_multi',
		'id'	=> $prefix.'footer_bottom_items',
		'dd_fields' => array ( 
			'Left'  => array(
				'copyright' => esc_html__( 'Copyright Text', 'tain-core' )
			),
			'Center'  => array(
				'menu'	=> esc_html__( 'Footer Menu', 'tain-core' )
			),
			'Right'  => array(),
			'disabled' => array(
				'social'	=> esc_html__( 'Footer Social Links', 'tain-core' ),
				'widget'	=> esc_html__( 'Custom Widget', 'tain-core' )
			)
		),
		'required'	=> array( $prefix.'footer_bottom_items_opt', 'custom' )
	),
	//Header Slider
	array( 
		'label'	=> esc_html__( 'Slider', 'tain-core' ),
		'desc'	=> esc_html__( 'This header slider settings.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Slider', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Slider Option', 'tain-core' ),
		'id'	=> $prefix.'header_slider_opt',
		'tab'	=> esc_html__( 'Slider', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'bottom' => esc_html__( 'Below Header', 'tain-core' ),
			'top' => esc_html__( 'Above Header', 'tain-core' ),
			'none' => esc_html__( 'None', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Slider Shortcode', 'tain-core' ),
		'desc'	=> esc_html__( 'This is the place for enter slider shortcode. Example revolution slider shortcodes.', 'tain-core' ), 
		'id'	=> $prefix.'header_slider',
		'tab'	=> esc_html__( 'Slider', 'tain-core' ),
		'type'	=> 'textarea',
		'default'	=> ''
	),
	//Post Format
	array( 
		'label'	=> esc_html__( 'Video', 'tain-core' ),
		'desc'	=> esc_html__( 'This part for if you choosed video format, then you must choose video type and give video id.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Format', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Video Modal', 'tain-core' ),
		'id'	=> $prefix.'video_modal',
		'tab'	=> esc_html__( 'Format', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'onclick' => esc_html__( 'On Click Run Video', 'tain-core' ),
			'overlay' => esc_html__( 'Modal Box Video', 'tain-core' ),
			'direct' => esc_html__( 'Direct Video', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Video Type', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose video type.', 'tain-core' ), 
		'id'	=> $prefix.'video_type',
		'tab'	=> esc_html__( 'Format', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'' => esc_html__( 'None', 'tain-core' ),
			'youtube' => esc_html__( 'Youtube', 'tain-core' ),
			'vimeo' => esc_html__( 'Vimeo', 'tain-core' ),
			'custom' => esc_html__( 'Custom Video', 'tain-core' )
		),
		'default'	=> ''
	),
	array( 
		'label'	=> esc_html__( 'Video ID', 'tain-core' ),
		'desc'	=> esc_html__( 'Enter Video ID Example: ZSt9tm3RoUU. If you choose custom video type then you enter custom video url and video must be mp4 format.', 'tain-core' ), 
		'id'	=> $prefix.'video_id',
		'tab'	=> esc_html__( 'Format', 'tain-core' ),
		'type'	=> 'text',
		'default'	=> ''
	),
	array( 
		'type'	=> 'line',
		'tab'	=> esc_html__( 'Format', 'tain-core' )
	),
	array( 
		'label'	=> esc_html__( 'Audio', 'tain-core' ),
		'desc'	=> esc_html__( 'This part for if you choosed audio format, then you must give audio id.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Format', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Audio Type', 'tain-core' ),
		'desc'	=> esc_html__( 'Choose audio type.', 'tain-core' ), 
		'id'	=> $prefix.'audio_type',
		'tab'	=> esc_html__( 'Format', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'' => esc_html__( 'None', 'tain-core' ),
			'soundcloud' => esc_html__( 'Soundcloud', 'tain-core' ),
			'custom' => esc_html__( 'Custom Audio', 'tain-core' )
		),
		'default'	=> ''
	),
	array( 
		'label'	=> esc_html__( 'Audio ID', 'tain-core' ),
		'desc'	=> esc_html__( 'Enter soundcloud audio ID. Example: 315307209.', 'tain-core' ), 
		'id'	=> $prefix.'audio_id',
		'tab'	=> esc_html__( 'Format', 'tain-core' ),
		'type'	=> 'text',
		'default'	=> ''
	),
	array( 
		'type'	=> 'line',
		'tab'	=> esc_html__( 'Format', 'tain-core' )
	),
	array( 
		'label'	=> esc_html__( 'Gallery', 'tain-core' ),
		'desc'	=> esc_html__( 'This part for if you choosed gallery format, then you must choose gallery images here.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Format', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Gallery Modal', 'tain-core' ),
		'id'	=> $prefix.'gallery_modal',
		'tab'	=> esc_html__( 'Format', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'default' => esc_html__( 'Default Gallery', 'tain-core' ),
			'popup' => esc_html__( 'Popup Gallery', 'tain-core' ),
			'grid' => esc_html__( 'Grid Popup Gallery', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Choose Gallery Images', 'tain-core' ),
		'id'	=> $prefix.'gallery',
		'type'	=> 'gallery',
		'tab'	=> esc_html__( 'Format', 'tain-core' )
	),
	array( 
		'type'	=> 'line',
		'tab'	=> esc_html__( 'Format', 'tain-core' )
	),
	array( 
		'label'	=> esc_html__( 'Quote', 'tain-core' ),
		'desc'	=> esc_html__( 'This part for if you choosed quote format, then you must fill the quote text and author name box.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Format', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Quote Modal', 'tain-core' ),
		'id'	=> $prefix.'quote_modal',
		'tab'	=> esc_html__( 'Format', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'featured' => esc_html__( 'Dark Overlay', 'tain-core' ),
			'theme-overlay' => esc_html__( 'Theme Overlay', 'tain-core' ),
			'theme' => esc_html__( 'Theme Color Background', 'tain-core' ),
			'none' => esc_html__( 'None', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Quote Text', 'tain-core' ),
		'desc'	=> esc_html__( 'Enter quote text.', 'tain-core' ), 
		'id'	=> $prefix.'quote_text',
		'tab'	=> esc_html__( 'Format', 'tain-core' ),
		'type'	=> 'textarea',
		'default'	=> ''
	),
	array( 
		'label'	=> esc_html__( 'Quote Author', 'tain-core' ),
		'desc'	=> esc_html__( 'Enter quote author name.', 'tain-core' ), 
		'id'	=> $prefix.'quote_author',
		'tab'	=> esc_html__( 'Format', 'tain-core' ),
		'type'	=> 'text',
		'default'	=> ''
	),
	array( 
		'type'	=> 'line',
		'tab'	=> esc_html__( 'Format', 'tain-core' )
	),
	array( 
		'label'	=> esc_html__( 'Link', 'tain-core' ),
		'desc'	=> esc_html__( 'This part for if you choosed link format, then you must fill the external link box.', 'tain-core' ), 
		'tab'	=> esc_html__( 'Format', 'tain-core' ),
		'type'	=> 'label'
	),
	array( 
		'label'	=> esc_html__( 'Link Modal', 'tain-core' ),
		'id'	=> $prefix.'link_modal',
		'tab'	=> esc_html__( 'Format', 'tain-core' ),
		'type'	=> 'select',
		'options' => array ( 
			'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
			'featured' => esc_html__( 'Dark Overlay', 'tain-core' ),
			'theme-overlay' => esc_html__( 'Theme Overlay', 'tain-core' ),
			'theme' => esc_html__( 'Theme Color Background', 'tain-core' ),
			'none' => esc_html__( 'None', 'tain-core' )
		),
		'default'	=> 'theme-default'
	),
	array( 
		'label'	=> esc_html__( 'Link Text', 'tain-core' ),
		'desc'	=> esc_html__( 'Enter link text to show.', 'tain-core' ), 
		'id'	=> $prefix.'link_text',
		'tab'	=> esc_html__( 'Format', 'tain-core' ),
		'type'	=> 'text',
		'default'	=> ''
	),
	array( 
		'label'	=> esc_html__( 'External Link', 'tain-core' ),
		'desc'	=> esc_html__( 'Enter external link.', 'tain-core' ), 
		'id'	=> $prefix.'extrenal_link',
		'tab'	=> esc_html__( 'Format', 'tain-core' ),
		'type'	=> 'url',
		'default'	=> ''
	),
	array( 
		'type'	=> 'line',
		'tab'	=> esc_html__( 'Format', 'tain-core' )
	),
	
);
/**
 * Instantiate the class with all variables to create a meta box
 * var $id string meta box id
 * var $title string title
 * var $fields array fields
 * var $page string|array post type to add meta box to
 * var $js bool including javascript or not
 */
 
$post_box = new Custom_Add_Meta_Box( 'tain_post_metabox', esc_html__( 'Tain Post Options', 'tain-core' ), $fields, 'post', true );

/* Tain Page Options */
function tain_metabox_fields( $prefix ){
	
	$tain_menus = get_terms( 'nav_menu', array( 'hide_empty' => true ) );
	$tain_nav_menus = array( "none" => esc_html__( "None", "tain" ) );
	foreach( $tain_menus as $menu ){
		$tain_nav_menus[$menu->slug] = $menu->name;
	}
			
	$fields = array(
		array( 
			'label'	=> esc_html__( 'Page General Settings', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are page general settings.', 'tain-core' ), 
			'tab'	=> esc_html__( 'General', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Page Layout', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose page layout for current post single view.', 'tain-core' ), 
			'id'	=> $prefix.'layout',
			'tab'	=> esc_html__( 'General', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'wide' => esc_html__( 'Wide', 'tain-core' ),
				'boxed' => esc_html__( 'Boxed', 'tain-core' )			
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Page Content Padding Option', 'tain-core' ),
			'id'	=> $prefix.'content_padding_opt',
			'tab'	=> esc_html__( 'General', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'		
		),
		array( 
			'label'	=> esc_html__( 'Page Content Padding', 'tain-core' ), 
			'desc'	=> esc_html__( 'Set the top/right/bottom/left padding of page content.', 'tain-core' ),
			'id'	=> $prefix.'content_padding',
			'tab'	=> esc_html__( 'General', 'tain-core' ),
			'type'	=> 'space',
			'required'	=> array( $prefix.'content_padding_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Page Background Color', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose color setting for current page background.', 'tain-core' ),
			'tab'	=> esc_html__( 'General', 'tain-core' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'main_bg_color'
		),
		array( 
				'label'	=> esc_html__( 'Page Background Image', 'tain-core' ),
				'desc'	=> esc_html__( 'Choose custom logo image for current page.', 'tain-core' ), 
				'tab'	=> esc_html__( 'General', 'tain-core' ),
				'type'	=> 'image',
				'id'	=> $prefix.'main_bg_image'
			),
		array( 
			'label'	=> esc_html__( 'Page Margin', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are margin settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'General', 'tain-core' ),
			'type'	=> 'space',
			'id'	=> $prefix.'main_margin'
		),
		array( 
			'label'	=> esc_html__( 'Page Template Option', 'tain-core' ),
			'id'	=> $prefix.'template_opt',
			'tab'	=> esc_html__( 'General', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'		
		),
		array( 
			'label'	=> esc_html__( 'Page Template', 'tain-core' ),
			'id'	=> $prefix.'template',
			'tab'	=> esc_html__( 'General', 'tain-core' ),
			'type'	=> 'image_select',
			'options' => array(
				'no-sidebar'	=> get_theme_file_uri( '/assets/images/page-layouts/1.png' ), 
				'right-sidebar'	=> get_theme_file_uri( '/assets/images/page-layouts/2.png' ), 
				'left-sidebar'	=> get_theme_file_uri( '/assets/images/page-layouts/3.png' ), 
				'both-sidebar'	=> get_theme_file_uri( '/assets/images/page-layouts/4.png' ), 
			),
			'default'	=> 'right-sidebar',
			'required'	=> array( $prefix.'template_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Left Sidebar', 'tain-core' ),
			'id'	=> $prefix.'left_sidebar',
			'tab'	=> esc_html__( 'General', 'tain-core' ),
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'template_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Right Sidebar', 'tain-core' ),
			'id'	=> $prefix.'right_sidebar',
			'tab'	=> esc_html__( 'General', 'tain-core' ),
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'template_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Sidebar On Mobile', 'tain-core' ),
			'id'	=> $prefix.'sidebar_mobile',
			'tab'	=> esc_html__( 'General', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'1' => esc_html__( 'Show', 'tain-core' ),
				'0' => esc_html__( 'Hide', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header General Settings', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header general settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Header Extra Class', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter extra class name for additional class name for header.', 'tain-core' ), 
			'id'	=> $prefix.'header_extra_class',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'One Page Menu Offset', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter one page menu offset for desktop menu.', 'tain-core' ), 
			'id'	=> $prefix.'one_page_menu_offset',
			'tab'	=> esc_html__( 'One Page', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> '60'
		),
		array( 
			'label'	=> esc_html__( 'One Page Mobile Menu Offset', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter one page mobile menu offset for desktop menu.', 'tain-core' ), 
			'id'	=> $prefix.'one_page_mobmenu_offset',
			'tab'	=> esc_html__( 'One Page', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> '60'
		),
		array( 
			'label'	=> esc_html__( 'Header Layout', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose page layout for current page header layout.', 'tain-core' ), 
			'id'	=> $prefix.'header_layout',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'wide' => esc_html__( 'Wide', 'tain-core' ),
				'boxed' => esc_html__( 'Boxed', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Type', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose page layout for current page header type.', 'tain-core' ), 
			'id'	=> $prefix.'header_type',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'default' => esc_html__( 'Default', 'tain-core' ),
				'left-sticky' => esc_html__( 'Left Sticky', 'tain-core' ),
				'right-sticky' => esc_html__( 'Right Sticky', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Background Image', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header background image for current post.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'image',
			'id'	=> $prefix.'header_bg_img',
			'required'	=> array( $prefix.'header_type', 'default' )
		),
		array( 
			'label'	=> esc_html__( 'Header Items Options', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header items options for enable header drag and drop items.', 'tain-core' ), 
			'id'	=> $prefix.'header_items_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Items', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header general items for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'header_items',
			'dd_fields' => array ( 
				'Normal' => array( 
					'header-topbar' 	=> esc_html__( 'Topbar', 'tain-core' ),
					'header-logo'	=> esc_html__( 'Logo Bar', 'tain-core' )
				),
				'Sticky' => array( 
					'header-nav' 	=> esc_html__( 'Navbar', 'tain-core' )
				),
				'disabled' => array()
			),
			'required'	=> array( $prefix.'header_items_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Absolute Option', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header absolute to change header look transparent.', 'tain-core' ), 
			'id'	=> $prefix.'header_absolute_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'1' => esc_html__( 'Enable', 'tain-core' ),
				'0' => esc_html__( 'Disable', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header sticky options.', 'tain-core' ), 
			'id'	=> $prefix.'header_sticky_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'sticky' => esc_html__( 'Header Sticky Part', 'tain-core' ),
				'sticky-scroll' => esc_html__( 'Sticky Scroll Up', 'tain-core' ),
				'none' => esc_html__( 'None', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Secondary Space Option', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose secondary space option for enable secondary menu space for current page.', 'tain-core' ), 
			'id'	=> $prefix.'header_secondary_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'enable' => esc_html__( 'Enable', 'tain-core' ),
				'disable' => esc_html__( 'Disable', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Secondary Space Animate Type', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose secondary space option animate type for current page.', 'tain-core' ), 
			'id'	=> $prefix.'header_secondary_animate',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array(
				'left-push'		=> esc_html__( 'Left Push', 'tain-core' ),
				'left-overlay'	=> esc_html__( 'Left Overlay', 'tain-core' ),
				'right-push'	=> esc_html__( 'Right Push', 'tain-core' ),
				'right-overlay'	=> esc_html__( 'Right Overlay', 'tain-core' ),
				'full-overlay'	=> esc_html__( 'Full Page Overlay', 'tain-core' ),
			),
			'default'	=> 'left-push',
			'required'	=> array( $prefix.'header_secondary_opt', 'enable' )
		),
		array( 
			'label'	=> esc_html__( 'Secondary Space Width', 'tain-core' ),
			'desc'	=> esc_html__( 'Set secondary space width for current page. Example 300', 'tain-core' ), 
			'id'	=> $prefix.'header_secondary_width',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> '',
			'required'	=> array( $prefix.'header_secondary_opt', 'enable' )
		),
		array( 
			'label'	=> esc_html__( 'Custom Logo', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose custom logo image for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'image',
			'id'	=> $prefix.'custom_logo',
		),
		array( 
			'label'	=> esc_html__( 'Custom Sticky Logo', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose custom sticky logo image for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'image',
			'id'	=> $prefix.'custom_sticky_logo',
		),
		array( 
			'label'	=> esc_html__( 'Select Navigation Menu', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose navigation menu for current page.', 'tain-core' ), 
			'id'	=> $prefix.'nav_menu',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => $tain_nav_menus
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header topbar settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Options', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header items options for enable header drag and drop items.', 'tain-core' ), 
			'id'	=> $prefix.'header_topbar_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Height', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header topbar height for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'dimension',
			'id'	=> $prefix.'header_topbar_height',
			'property' => 'height',
			'required'	=> array( $prefix.'header_topbar_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Sticky Height', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header topbar sticky height for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'dimension',
			'id'	=> $prefix.'header_topbar_sticky_height',
			'property' => 'height',
			'required'	=> array( $prefix.'header_topbar_opt', 'custom' )
		),
		array( 
			'label'	=> '',
			'desc'	=> esc_html__( 'These all are header topbar skin settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Skin Settings', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header topbar skin settings options.', 'tain-core' ), 
			'id'	=> $prefix.'header_topbar_skin_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Font Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header topbar font color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'color',
			'id'	=> $prefix.'header_topbar_font',
			'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Background', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header topbar background color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'header_topbar_bg',
			'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Link Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header topbar link color settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'header_topbar_link',
			'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Border', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header topbar border settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'header_topbar_border',
			'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Padding', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header topbar padding settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'space',
			'id'	=> $prefix.'header_topbar_padding',
			'required'	=> array( $prefix.'header_topbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Sticky Skin Settings', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header top bar sticky skin settings options.', 'tain-core' ), 
			'id'	=> $prefix.'header_topbar_sticky_skin_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Sticky Font Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header top bar sticky font color for current post.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'color',
			'id'	=> $prefix.'header_topbar_sticky_font',
			'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Sticky Background', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header top bar sticky background color for current post.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'header_topbar_sticky_bg',
			'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Sticky Link Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header top bar sticky link color settings for current post.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'header_topbar_sticky_link',
			'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Sticky Border', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header top bar sticky border settings for current post.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'header_topbar_sticky_border',
			'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Sticky Padding', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header top bar sticky padding settings for current post.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'space',
			'id'	=> $prefix.'header_topbar_sticky_padding',
			'required'	=> array( $prefix.'header_topbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Items Option', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header topbar items enable options.', 'tain-core' ), 
			'id'	=> $prefix.'header_topbar_items_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Top Bar Items', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header topbar items for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'header_topbar_items',
			'dd_fields' => array ( 
				'Left'  => array(
					'header-topbar-date' => esc_html__( 'Date', 'tain-core' ),						
				),
				'Center' => array(),
				'Right' => array(),
				'disabled' => array(
					'header-topbar-text-1'	=> esc_html__( 'Custom Text 1', 'tain-core' ),
					'header-topbar-text-2'	=> esc_html__( 'Custom Text 2', 'tain-core' ),
					'header-topbar-text-3'	=> esc_html__( 'Custom Text 3', 'tain-core' ),
					'header-topbar-menu'    => esc_html__( 'Top Bar Menu', 'tain-core' ),
					'header-topbar-social'	=> esc_html__( 'Social', 'tain-core' ),
					'header-topbar-search'	=> esc_html__( 'Search', 'tain-core' ),
					'header-topbar-search-toggle'	=> esc_html__( 'Search Toggle', 'tain-core' ),
					'header-cart'   		=> esc_html__( 'Cart', 'tain-core' )
				)
			),
			'required'	=> array( $prefix.'header_topbar_items_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header logo bar settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Options', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header items options for enable header drag and drop items.', 'tain-core' ), 
			'id'	=> $prefix.'header_logo_bar_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Height', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header logo bar height for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'dimension',
			'id'	=> $prefix.'header_logo_bar_height',
			'property' => 'height',
			'required'	=> array( $prefix.'header_logo_bar_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Sticky Height', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header logo bar sticky height for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'dimension',
			'id'	=> $prefix.'header_logo_bar_sticky_height',
			'property' => 'height',
			'required'	=> array( $prefix.'header_logo_bar_opt', 'custom' )
		),
		array( 
			'label'	=> '',
			'desc'	=> esc_html__( 'These all are header logo bar skin settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Skin Settings', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header logo bar skin settings options.', 'tain-core' ), 
			'id'	=> $prefix.'header_logo_bar_skin_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Font Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header logo bar font color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'color',
			'id'	=> $prefix.'header_logo_bar_font',
			'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Background', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header logo bar background color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'header_logo_bar_bg',
			'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Link Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header logo bar link color settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'header_logo_bar_link',
			'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Border', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header logo bar border settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'header_logo_bar_border',
			'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Padding', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header logo bar padding settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'space',
			'id'	=> $prefix.'header_logo_bar_padding',
			'required'	=> array( $prefix.'header_logo_bar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Sticky Skin Settings', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header logo bar sticky skin settings options.', 'tain-core' ), 
			'id'	=> $prefix.'header_logobar_sticky_skin_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Sticky Font Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header logo bar sticky font color for current post.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'color',
			'id'	=> $prefix.'header_logobar_sticky_font',
			'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Sticky Background', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header logo bar sticky background color for current post.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'header_logobar_sticky_bg',
			'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Sticky Link Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header logo bar sticky link color settings for current post.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'header_logobar_sticky_link',
			'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Sticky Border', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header logo bar sticky border settings for current post.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'header_logobar_sticky_border',
			'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Sticky Padding', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header logo bar sticky padding settings for current post.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'space',
			'id'	=> $prefix.'header_logobar_sticky_padding',
			'required'	=> array( $prefix.'header_logobar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Items Option', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header logo bar items enable options.', 'tain-core' ), 
			'id'	=> $prefix.'header_logo_bar_items_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Logo Bar Items', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header logo bar items for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'header_logo_bar_items',
			'dd_fields' => array ( 
				'Left'  => array(
					'header-logobar-logo'		=> esc_html__( 'Logo', 'tain-core' ),
					'header-logobar-sticky-logo' => esc_html__( 'Sticky Logo', 'tain-core' )											
				),
				'Center' => array(),
				'Right' => array(),
				'disabled' => array(
					'header-logobar-social'		=> esc_html__( 'Social', 'tain-core' ),
					'header-logobar-search'		=> esc_html__( 'Search', 'tain-core' ),
					'header-logobar-secondary-toggle'	=> esc_html__( 'Secondary Toggle', 'tain-core' ),	
					'header-phone'   			=> esc_html__( 'Phone Number', 'tain-core' ),
					'header-address'  			=> esc_html__( 'Address Text', 'tain-core' ),
					'header-email'   			=> esc_html__( 'Email', 'tain-core' ),
					'header-logobar-menu'   	=> esc_html__( 'Main Menu', 'tain-core' ),
					'header-logobar-search-toggle'	=> esc_html__( 'Search Toggle', 'tain-core' ),
					'header-logobar-text-1'		=> esc_html__( 'Custom Text 1', 'tain-core' ),
					'header-logobar-text-2'		=> esc_html__( 'Custom Text 2', 'tain-core' ),
					'header-logobar-text-3'		=> esc_html__( 'Custom Text 3', 'tain-core' ),	
					'header-cart'   			=> esc_html__( 'Cart', 'tain-core' ),
					'header-wishlist'   		=> esc_html__( 'Wishlist', 'tain-core' ),
					'multi-info'   				=> esc_html__( 'Address, Phone, Email', 'tain-core' )
				)
			),
			'required'	=> array( $prefix.'header_logo_bar_items_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header navbar settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Options', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header items options for enable header drag and drop items.', 'tain-core' ), 
			'id'	=> $prefix.'header_navbar_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Height', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header navbar height for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'dimension',
			'id'	=> $prefix.'header_navbar_height',
			'property' => 'height',
			'required'	=> array( $prefix.'header_navbar_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Sticky Height', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header navbar sticky height for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'dimension',
			'id'	=> $prefix.'header_navbar_sticky_height',
			'property' => 'height',
			'required'	=> array( $prefix.'header_navbar_opt', 'custom' )
		),
		array( 
			'label'	=> '',
			'desc'	=> esc_html__( 'These all are header navbar skin settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Skin Settings', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header navbar skin settings options.', 'tain-core' ), 
			'id'	=> $prefix.'header_navbar_skin_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Font Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header navbar font color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'color',
			'id'	=> $prefix.'header_navbar_font',
			'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Background', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header navbar background color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'header_navbar_bg',
			'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Link Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header navbar link color settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'header_navbar_link',
			'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Border', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header navbar border settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'header_navbar_border',
			'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Padding', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header navbar padding settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'space',
			'id'	=> $prefix.'header_navbar_padding',
			'required'	=> array( $prefix.'header_navbar_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Sticky Skin Settings', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header navbar sticky skin settings options.', 'tain-core' ), 
			'id'	=> $prefix.'header_navbar_sticky_skin_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Sticky Font Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header navbar sticky font color for current post.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'color',
			'id'	=> $prefix.'header_navbar_sticky_font',
			'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Sticky Background', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header navbar sticky background color for current post.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'header_navbar_sticky_bg',
			'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Sticky Link Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header navbar sticky link color settings for current post.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'header_navbar_sticky_link',
			'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Sticky Border', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header navbar sticky border settings for current post.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'header_navbar_sticky_border',
			'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Sticky Padding', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header navbar sticky padding settings for current post.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'space',
			'id'	=> $prefix.'header_navbar_sticky_padding',
			'required'	=> array( $prefix.'header_navbar_sticky_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Items Option', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header navbar items enable options.', 'tain-core' ), 
			'id'	=> $prefix.'header_navbar_items_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Navbar Items', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header navbar items for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'header_navbar_items',
			'dd_fields' => array ( 
				'Left'  => array(
					'header-navbar-logo'		=> esc_html__( 'Logo', 'tain' ),
					'header-navbar-sticky-logo'	=> esc_html__( 'Stikcy Logo', 'tain' ),
					'header-navbar-menu'    	=> esc_html__( 'Main Menu', 'tain' )										
				),
				'Center' => array(),
				'Right' => array(),
				'disabled' => array(					
					'header-navbar-text-1'		=> esc_html__( 'Custom Text 1', 'tain' ),
					'header-navbar-text-2'		=> esc_html__( 'Custom Text 2', 'tain' ),
					'header-navbar-text-3'		=> esc_html__( 'Custom Text 3', 'tain' ),					
					'header-navbar-social'		=> esc_html__( 'Social', 'tain' ),
					'header-navbar-secondary-toggle'	=> esc_html__( 'Secondary Toggle', 'tain' ),					
					'header-navbar-search'		=> esc_html__( 'Search', 'tain' ),
					'header-phone'   			=> esc_html__( 'Phone Number', 'tain' ),
					'header-address'  			=> esc_html__( 'Address Text', 'tain' ),
					'header-email'   			=> esc_html__( 'Email', 'tain' ),
					'header-navbar-search-toggle'	=> esc_html__( 'Search Toggle', 'tain' ),
					'header-cart'   			=> esc_html__( 'Cart', 'tain' ),
					'header-wishlist'   		=> esc_html__( 'Wishlist', 'tain' )
				)
			),
			'required'	=> array( $prefix.'header_navbar_items_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header sticky settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Options', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header sticky part option.', 'tain-core' ), 
			'id'	=> $prefix.'header_stikcy_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Width', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header sticky part width for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'dimension',
			'id'	=> $prefix.'header_stikcy_width',
			'property' => 'width',
			'required'	=> array( $prefix.'header_stikcy_opt', 'custom' )
		),
		array( 
			'label'	=> '',
			'desc'	=> esc_html__( 'These all are header sticky skin settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Skin Settings', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header sticky skin settings options.', 'tain-core' ), 
			'id'	=> $prefix.'header_stikcy_skin_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Font Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header sticky font color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'color',
			'id'	=> $prefix.'header_stikcy_font',
			'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Background', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header sticky background color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'header_stikcy_bg',
			'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Link Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header Sticky link color settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'header_stikcy_link',
			'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Border', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header sticky border settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'header_stikcy_border',
			'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Padding', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header sticky padding settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'space',
			'id'	=> $prefix.'header_stikcy_padding',
			'required'	=> array( $prefix.'header_stikcy_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Items Option', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose header sticky items enable options.', 'tain-core' ), 
			'id'	=> $prefix.'header_stikcy_items_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Header Sticky/Fixed Part Items', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are header sticky items for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'header_stikcy_items',
			'dd_fields' => array ( 
				'Top'  => array(
					'header-fixed-logo' => esc_html__( 'Logo', 'tain-core' )
				),
				'Middle'  => array(
					'header-fixed-menu'	=> esc_html__( 'Menu', 'tain-core' )					
				),
				'Bottom'  => array(
					'header-fixed-social'	=> esc_html__( 'Social', 'tain-core' )					
				),
				'disabled' => array(
					'header-fixed-text-1'	=> esc_html__( 'Custom Text 1', 'tain-core' ),
					'header-fixed-text-2'	=> esc_html__( 'Custom Text 2', 'tain-core' ),					
					'header-fixed-search'	=> esc_html__( 'Search Form', 'tain-core' )
				)
			),
			'required'	=> array( $prefix.'header_stikcy_items_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Bar', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are page title bar settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Page Title Option', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose page title enable or disable.', 'tain-core' ), 
			'id'	=> $prefix.'header_page_title_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'1' => esc_html__( 'Enable', 'tain-core' ),
				'0' => esc_html__( 'Disable', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Page Title Text', 'tain-core' ),
			'desc'	=> esc_html__( 'If this page title is empty, then showing current page default title.', 'tain-core' ), 
			'id'	=> $prefix.'header_page_title_text',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> '',
			'required'	=> array( $prefix.'header_page_title_opt', '1' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Description', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter page title description.', 'tain-core' ), 
			'id'	=> $prefix.'header_page_title_desc',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'textarea',
			'default'	=> '',
			'required'	=> array( $prefix.'header_page_title_opt', '1' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Background Parallax', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose page title background parallax.', 'tain-core' ), 
			'id'	=> $prefix.'header_page_title_parallax',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'1' => esc_html__( 'Enable', 'tain-core' ),
				'0' => esc_html__( 'Disable', 'tain-core' )
			),
			'default'	=> 'theme-default',
			'required'	=> array( $prefix.'header_page_title_opt', '1' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Background Video Option', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose page title background video option.', 'tain-core' ), 
			'id'	=> $prefix.'header_page_title_video_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'1' => esc_html__( 'Enable', 'tain-core' ),
				'0' => esc_html__( 'Disable', 'tain-core' )
			),
			'default'	=> 'theme-default',
			'required'	=> array( $prefix.'header_page_title_opt', '1' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Background Video', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter youtube video ID. Example: ZSt9tm3RoUU.', 'tain-core' ), 
			'id'	=> $prefix.'header_page_title_video',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> '',
			'required'	=> array( $prefix.'header_page_title_video_opt', '1' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Bar Items Option', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose page title bar items option.', 'tain-core' ), 
			'id'	=> $prefix.'page_title_items_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default',
			'required'	=> array( $prefix.'header_page_title_opt', '1' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Bar Items', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are page title bar items for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'page_title_items',
			'dd_fields' => array ( 
				'Left'  => array(
					'title' => esc_html__( 'Page Title Text', 'tain-core' ),
				),
				'Center'  => array(
					
				),
				'Right'  => array(
					'breadcrumb'	=> esc_html__( 'Breadcrumb', 'tain-core' )
				),
				'disabled' => array(
					'description' => esc_html__( 'Page Title Description', 'tain-core' )
				)
			),
			'required'	=> array( $prefix.'page_title_items_opt', 'custom' )
		),
		array( 
			'label'	=> '',
			'desc'	=> esc_html__( 'These all are page title skin settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'label',
			'required'	=> array( $prefix.'header_page_title_opt', '1' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Skin Settings', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose page title skin settings options.', 'tain-core' ), 
			'id'	=> $prefix.'page_title_skin_opt',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default',
			'required'	=> array( $prefix.'header_page_title_opt', '1' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Font Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are page title font color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'color',
			'id'	=> $prefix.'page_title_font',
			'required'	=> array( $prefix.'page_title_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Description Color', 'tain-core' ),
			'desc'	=> esc_html__( 'This is page title description color.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'color',
			'id'	=> $prefix.'page_title_desc_color',
			'required'	=> array( $prefix.'page_title_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Background', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are page title background color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'page_title_bg',
			'required'	=> array( $prefix.'page_title_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Background Image', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter page title background image url.', 'tain-core' ), 
			'id'	=> $prefix.'page_title_bg_img',
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'url',
			'default'	=> '',
			'required'	=> array( $prefix.'page_title_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Link Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are page title link color settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'page_title_link',
			'required'	=> array( $prefix.'page_title_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Border', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are page title border settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'page_title_border',
			'required'	=> array( $prefix.'page_title_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Padding', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are page title padding settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'space',
			'id'	=> $prefix.'page_title_padding',
			'required'	=> array( $prefix.'page_title_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Page Title Overlay', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are page title overlay color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Header', 'tain-core' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'page_title_overlay',
			'required'	=> array( $prefix.'page_title_skin_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer General',
			'desc'	=> esc_html__( 'These all are header footer settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Footer Layout', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose footer layout for current page.', 'tain-core' ), 
			'id'	=> $prefix.'footer_layout',
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'wide' => esc_html__( 'Wide', 'tain-core' ),
				'boxed' => esc_html__( 'Boxed', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Hidden Footer', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose hidden footer option.', 'tain-core' ), 
			'id'	=> $prefix.'hidden_footer',
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'1' => esc_html__( 'Enable', 'tain-core' ),
				'0' => esc_html__( 'Disable', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> '',
			'desc'	=> esc_html__( 'These all are footer skin settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Footer Skin Settings', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose footer skin settings options.', 'tain-core' ), 
			'id'	=> $prefix.'footer_skin_opt',
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Footer Font Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer font color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'color',
			'id'	=> $prefix.'footer_font',
			'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Background Image', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose footer background image for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'image',
			'id'	=> $prefix.'footer_bg_img',
			'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Background Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer background color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'color',
			'id'	=> $prefix.'footer_bg',
			'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Background Overlay', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer background overlay color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'footer_bg_overlay',
			'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Link Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer link color settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'footer_link',
			'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Border', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer border settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'footer_border',
			'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Padding', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer padding settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'space',
			'id'	=> $prefix.'footer_padding',
			'required'	=> array( $prefix.'footer_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Items Option', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose footer items enable options.', 'tain-core' ), 
			'id'	=> $prefix.'footer_items_opt',
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Footer Items', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer items for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'footer_items',
			'dd_fields' => array ( 
				'Enabled'  => array(
					'footer-bottom'	=> esc_html__( 'Footer Bottom', 'tain-core' )
				),
				'disabled' => array(
					'footer-top' => esc_html__( 'Footer Top', 'tain-core' ),
					'footer-middle'	=> esc_html__( 'Footer Middle', 'tain-core' )
				)
			),
			'required'	=> array( $prefix.'footer_items_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Top',
			'desc'	=> esc_html__( 'These all are footer top settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Footer Top Skin', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose footer top skin options.', 'tain-core' ), 
			'id'	=> $prefix.'footer_top_skin_opt',
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Footer Top Font Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer top font color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'color',
			'id'	=> $prefix.'footer_top_font',
			'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Top Widget Title color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer top widget title color.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'color',
			'id'	=> $prefix.'footer_top_widget_title_color',
			'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Top Background', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer background color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'footer_top_bg',
			'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Top Link Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer top link color settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'footer_top_link',
			'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Top Border', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer top border settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'footer_top_border',
			'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Top Padding', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer top padding settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'space',
			'id'	=> $prefix.'footer_top_padding',
			'required'	=> array( $prefix.'footer_top_skin_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Top Columns and Sidebars Settings',
			'desc'	=> esc_html__( 'These all are footer top columns and sidebar settings.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Footer Layout Option', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose footer layout option.', 'tain-core' ), 
			'id'	=> $prefix.'footer_top_layout_opt',
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Footer Layout', 'tain-core' ),
			'id'	=> $prefix.'footer_top_layout',
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'image_select',
			'options' => array(
				'3-3-3-3'	=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-1.png',
				'4-4-4'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-2.png',
				'3-6-3'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-3.png',
				'6-6'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-4.png',
				'9-3'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-5.png',
				'3-9'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-6.png',
				'4-2-2-2-2'	=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-8.png',
				'6-2-2-2'	=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-9.png',
				'12'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-7.png'
			),
			'default'	=> '4-4-4',
			'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer First Column',
			'desc'	=> esc_html__( 'Select footer first column widget.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'id'	=> $prefix.'footer_top_sidebar_1',
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Second Column',
			'desc'	=> esc_html__( 'Select footer second column widget.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'id'	=> $prefix.'footer_top_sidebar_2',
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Third Column',
			'desc'	=> esc_html__( 'Select footer third column widget.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'id'	=> $prefix.'footer_top_sidebar_3',
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Fourth Column',
			'desc'	=> esc_html__( 'Select footer fourth column widget.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'id'	=> $prefix.'footer_top_sidebar_4',
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'footer_top_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Middle',
			'desc'	=> esc_html__( 'These all are footer middle settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Footer Middle Skin', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose footer middle skin options.', 'tain-core' ), 
			'id'	=> $prefix.'footer_middle_skin_opt',
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Footer Middle Font Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer middle font color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'color',
			'id'	=> $prefix.'footer_middle_font',
			'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Middle Widget Title Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer middle widget title color.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'color',
			'id'	=> $prefix.'footer_middle_widget_title_color',
			'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Middle Background', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer background color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'footer_middle_bg',
			'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Middle Link Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer middle link color settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'footer_middle_link',
			'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Middle Border', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer middle border settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'footer_middle_border',
			'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Middle Padding', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer middle padding settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'space',
			'id'	=> $prefix.'footer_middle_padding',
			'required'	=> array( $prefix.'footer_middle_skin_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Middle Columns and Sidebars Settings',
			'desc'	=> esc_html__( 'These all are footer middle columns and sidebar settings.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Footer Layout Option', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose footer layout option.', 'tain-core' ), 
			'id'	=> $prefix.'footer_middle_layout_opt',
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Footer Layout', 'tain-core' ),
			'id'	=> $prefix.'footer_middle_layout',
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'image_select',
			'options' => array(
				'3-3-3-3'	=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-1.png',
				'4-4-4'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-2.png',
				'3-6-3'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-3.png',
				'6-6'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-4.png',
				'9-3'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-5.png',
				'3-9'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-6.png',
				'4-2-2-2-2'	=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-8.png',
				'6-2-2-2'	=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-9.png',
				'12'		=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/footer/footer-7.png'
			),
			'default'	=> '4-4-4',
			'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer First Column',
			'desc'	=> esc_html__( 'Select footer first column widget.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'id'	=> $prefix.'footer_middle_sidebar_1',
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Second Column',
			'desc'	=> esc_html__( 'Select footer second column widget.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'id'	=> $prefix.'footer_middle_sidebar_2',
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Third Column',
			'desc'	=> esc_html__( 'Select footer third column widget.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'id'	=> $prefix.'footer_middle_sidebar_3',
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Fourth Column',
			'desc'	=> esc_html__( 'Select footer fourth column widget.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'id'	=> $prefix.'footer_middle_sidebar_4',
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'footer_middle_layout_opt', 'custom' )
		),
		array( 
			'label'	=> 'Footer Bottom',
			'desc'	=> esc_html__( 'These all are footer bottom settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Fixed', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose footer bottom fixed option.', 'tain-core' ), 
			'id'	=> $prefix.'footer_bottom_fixed',
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'1' => esc_html__( 'Enable', 'tain-core' ),
				'0' => esc_html__( 'Disable', 'tain-core' )			
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> '',
			'desc'	=> esc_html__( 'These all are footer bottom skin settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Skin', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose footer bottom skin options.', 'tain-core' ), 
			'id'	=> $prefix.'footer_bottom_skin_opt',
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Font Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer bottom font color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'color',
			'id'	=> $prefix.'footer_bottom_font',
			'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Background', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer bottom background color for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'alpha_color',
			'id'	=> $prefix.'footer_bottom_bg',
			'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Link Color', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer bottom link color settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'link_color',
			'id'	=> $prefix.'footer_bottom_link',
			'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Border', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer bottom border settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'space',
			'color' => 1,
			'border_style' => 1,
			'id'	=> $prefix.'footer_bottom_border',
			'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Padding', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer bottom padding settings for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'space',
			'id'	=> $prefix.'footer_bottom_padding',
			'required'	=> array( $prefix.'footer_bottom_skin_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Widget Option', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose footer bottom widget options.', 'tain-core' ), 
			'id'	=> $prefix.'footer_bottom_widget_opt',
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> 'Footer Bottom Widget',
			'desc'	=> esc_html__( 'Select footer bottom widget.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'id'	=> $prefix.'footer_bottom_widget',
			'type'	=> 'sidebar',
			'required'	=> array( $prefix.'footer_bottom_widget_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Items Option', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose footer bottom items options.', 'tain-core' ), 
			'id'	=> $prefix.'footer_bottom_items_opt',
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Footer Bottom Items', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are footer bottom items for current page.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Footer', 'tain-core' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'footer_bottom_items',
			'dd_fields' => array ( 
				'Left'  => array(
					'copyright' => esc_html__( 'Copyright Text', 'tain-core' )
				),
				'Center'  => array(
					'menu'	=> esc_html__( 'Footer Menu', 'tain-core' )
				),
				'Right'  => array(),
				'disabled' => array(
					'social'	=> esc_html__( 'Footer Social Links', 'tain-core' ),
					'widget'	=> esc_html__( 'Custom Widget', 'tain-core' )
				)
			),
			'required'	=> array( $prefix.'footer_bottom_items_opt', 'custom' )
		),
		//Header Slider
		array( 
			'label'	=> esc_html__( 'Slider', 'tain-core' ),
			'desc'	=> esc_html__( 'This header slider settings.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Slider', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Slider Option', 'tain-core' ),
			'id'	=> $prefix.'header_slider_opt',
			'tab'	=> esc_html__( 'Slider', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'bottom' => esc_html__( 'Below Header', 'tain-core' ),
				'top' => esc_html__( 'Above Header', 'tain-core' ),
				'none' => esc_html__( 'None', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Slider Shortcode', 'tain-core' ),
			'desc'	=> esc_html__( 'This is the place for enter slider shortcode. Example revolution slider shortcodes.', 'tain-core' ), 
			'id'	=> $prefix.'header_slider',
			'tab'	=> esc_html__( 'Slider', 'tain-core' ),
			'type'	=> 'textarea',
			'default'	=> ''
		),
	);
	return $fields;
}
$page_fields = tain_metabox_fields( 'tain_page_' );
$page_box = new Custom_Add_Meta_Box( 'tain_page_metabox', esc_html__( 'Tain Page Options', 'tain-core' ), $page_fields, 'page', true );

/* Custom Post Type Options */
$tain_cpt = TainFamework::tain_static_theme_mod( 'cpt-opts' );

// Portfolio Options
if( is_array( $tain_cpt ) && in_array( "portfolio", $tain_cpt ) ){
	
	// CPT Portfolio Metabox
	$prefix = 'tain_portfolio_';
	$portfolio_fields = array(
		array( 
			'label'	=> esc_html__( 'Portfolio General Settings', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are single portfolio general settings.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Portfolio', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Portfolio Layout Option', 'tain-core' ),
			'id'	=> $prefix.'layout_opt',
			'tab'	=> esc_html__( 'Portfolio', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'		
		),
		array( 
			'label'	=> esc_html__( 'Portfolio Layout', 'tain-core' ),
			'id'	=> $prefix.'layout',
			'tab'	=> esc_html__( 'Portfolio', 'tain-core' ),
			'type'	=> 'image_select',
			'options' => array(
				'1'	=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/portfolio-layouts/1.png', 
				'2'	=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/portfolio-layouts/2.png',
				'3'	=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/portfolio-layouts/3.png',
				'4'	=> TAIN_THEME_ADMIN_URL . '/customizer/assets/images/portfolio-layouts/4.png'
	
			),
			'default'	=> '1',
			'required'	=> array( $prefix.'layout_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Sticky Column', 'tain-core' ),
			'id'	=> $prefix.'sticky',
			'tab'	=> esc_html__( 'Portfolio', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'none' => esc_html__( 'None', 'tain-core' ),
				'right' => esc_html__( 'Right Column', 'tain-core' ),
				'left' => esc_html__( 'Left Column', 'tain-core' )
			),
			'default'	=> 'none'		
		),
		array( 
			'label'	=> esc_html__( 'Portfolio Format', 'tain-core' ),
			'id'	=> $prefix.'format',
			'tab'	=> esc_html__( 'Portfolio', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'standard' => esc_html__( 'Standard', 'tain-core' ),
				'video' => esc_html__( 'Video', 'tain-core' ),
				'audio' => esc_html__( 'Audio', 'tain-core' ),
				'gallery' => esc_html__( 'Gallery', 'tain-core' ),
				'gmap' => esc_html__( 'Google Map', 'tain-core' )
			),
			'default'	=> 'standard'		
		),
		array( 
			'label'	=> esc_html__( 'Portfolio Meta Items Options', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose portfolio meta items option.', 'tain-core' ), 
			'id'	=> $prefix.'items_opt',
			'tab'	=> esc_html__( 'Portfolio', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'theme-default' => esc_html__( 'Theme Default', 'tain-core' ),
				'custom' => esc_html__( 'Custom', 'tain-core' )
			),
			'default'	=> 'theme-default'
		),
		array( 
			'label'	=> esc_html__( 'Portfolio Meta Items', 'tain-core' ),
			'desc'	=> esc_html__( 'These all are meta items for portfolio. drag and drop needed items from disabled part to enabled.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Portfolio', 'tain-core' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'items',
			'dd_fields' => array ( 
				'Enabled'  => array(
					'date'		=> esc_html__( 'Date', 'tain-core' ),
					'client'	=> esc_html__( 'Client', 'tain-core' ),
					'category'	=> esc_html__( 'Category', 'tain-core' ),
					'share'		=> esc_html__( 'Share', 'tain-core' ),
				),
				'disabled' => array(
					'tag'		=> esc_html__( 'Tags', 'tain-core' ),
					'duration'	=> esc_html__( 'Duration', 'tain-core' ),
					'url'		=> esc_html__( 'URL', 'tain-core' ),
					'place'		=> esc_html__( 'Place', 'tain-core' ),
					'estimation'=> esc_html__( 'Estimation', 'tain-core' ),
				)
			),
			'required'	=> array( $prefix.'items_opt', 'custom' )
		),
		array( 
			'label'	=> esc_html__( 'Custom Redirect URL', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter url for custom webpage redirection. This link only for portfolio archive layout not for single portfolio.', 'tain-core' ), 
			'id'	=> $prefix.'custom_url',
			'tab'	=> esc_html__( 'Portfolio', 'tain-core' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Custom Redirect URL Target', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose custom url page navigate to blank or same page.', 'tain-core' ), 
			'id'	=> $prefix.'custom_url_target',
			'tab'	=> esc_html__( 'Portfolio', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'_blank' => esc_html__( 'Blank', 'tain-core' ),
				'_self' => esc_html__( 'Self', 'tain-core' )
			),
			'default'	=> '_blank'
		),
		array( 
			'label'	=> esc_html__( 'Portfolio Date', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose/Enter portfolio date.', 'tain-core' ), 
			'id'	=> $prefix.'date',
			'tab'	=> esc_html__( 'Info', 'tain-core' ),
			'type'	=> 'date',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Date Format', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter date format to show selcted portfolio date. Example: F j, Y', 'tain-core' ), 
			'id'	=> $prefix.'date_format',
			'tab'	=> esc_html__( 'Info', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> 'F j, Y'
		),
		array( 
			'label'	=> esc_html__( 'Client Name', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter client name.', 'tain-core' ), 
			'id'	=> $prefix.'client_name',
			'tab'	=> esc_html__( 'Info', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Duration', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter duration years or months.', 'tain-core' ), 
			'id'	=> $prefix.'duration',
			'tab'	=> esc_html__( 'Info', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Estimation', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter project estimation.', 'tain-core' ), 
			'id'	=> $prefix.'estimation',
			'tab'	=> esc_html__( 'Info', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Place', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter project place.', 'tain-core' ), 
			'id'	=> $prefix.'place',
			'tab'	=> esc_html__( 'Info', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'URL', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter project URL.', 'tain-core' ), 
			'id'	=> $prefix.'url',
			'tab'	=> esc_html__( 'Info', 'tain-core' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		//Portfolio Format
		array( 
			'label'	=> esc_html__( 'Video', 'tain-core' ),
			'desc'	=> esc_html__( 'This part for if you choosed video format, then you must choose video type and give video id.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Format', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Video Modal', 'tain-core' ),
			'id'	=> $prefix.'video_modal',
			'tab'	=> esc_html__( 'Format', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'onclick' => esc_html__( 'On Click Run Video', 'tain-core' ),
				'overlay' => esc_html__( 'Modal Box Video', 'tain-core' ),
				'direct' => esc_html__( 'Direct Video', 'tain-core' )
			),
			'default'	=> 'direct'
		),
		array( 
			'label'	=> esc_html__( 'Video Type', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose video type.', 'tain-core' ), 
			'id'	=> $prefix.'video_type',
			'tab'	=> esc_html__( 'Format', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'' => esc_html__( 'None', 'tain-core' ),
				'youtube' => esc_html__( 'Youtube', 'tain-core' ),
				'vimeo' => esc_html__( 'Vimeo', 'tain-core' ),
				'custom' => esc_html__( 'Custom Video', 'tain-core' )
			),
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Video ID', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter Video ID Example: ZSt9tm3RoUU. If you choose custom video type then you enter custom video url and video must be mp4 format.', 'tain-core' ), 
			'id'	=> $prefix.'video_id',
			'tab'	=> esc_html__( 'Format', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'type'	=> 'line',
			'tab'	=> esc_html__( 'Format', 'tain-core' )
		),
		array( 
			'label'	=> esc_html__( 'Audio', 'tain-core' ),
			'desc'	=> esc_html__( 'This part for if you choosed audio format, then you must give audio id.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Format', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Audio Type', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose audio type.', 'tain-core' ), 
			'id'	=> $prefix.'audio_type',
			'tab'	=> esc_html__( 'Format', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'' => esc_html__( 'None', 'tain-core' ),
				'soundcloud' => esc_html__( 'Soundcloud', 'tain-core' ),
				'custom' => esc_html__( 'Custom Audio', 'tain-core' )
			),
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Audio ID', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter soundcloud audio ID. Example: 315307209.', 'tain-core' ), 
			'id'	=> $prefix.'audio_id',
			'tab'	=> esc_html__( 'Format', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'type'	=> 'line',
			'tab'	=> esc_html__( 'Format', 'tain-core' )
		),
		array( 
			'label'	=> esc_html__( 'Gallery', 'tain-core' ),
			'desc'	=> esc_html__( 'This part for if you choosed gallery format, then you must choose gallery images here.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Format', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Gallery Modal', 'tain-core' ),
			'id'	=> $prefix.'gallery_modal',
			'tab'	=> esc_html__( 'Format', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'default' => esc_html__( 'Default Gallery', 'tain-core' ),
				'normal' => esc_html__( 'Normal Gallery', 'tain-core' ),
				'grid' => esc_html__( 'Grid/Masonry Gallery', 'tain-core' )
			),
			'default'	=> 'default'
		),
		array( 
			'label'	=> esc_html__( 'Grid Gutter Size', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter gallery grid gutter size. Example 20', 'tain-core' ), 
			'id'	=> $prefix.'grid_gutter',
			'tab'	=> esc_html__( 'Format', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> '',
			'required'	=> array( $prefix.'gallery_modal', 'grid' )
		),
		array( 
			'label'	=> esc_html__( 'Grid Columns', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter gallery grid columns count. Example 2', 'tain-core' ), 
			'id'	=> $prefix.'grid_cols',
			'tab'	=> esc_html__( 'Format', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> '',
			'required'	=> array( $prefix.'gallery_modal', 'grid' )
		),
		array( 
			'label'	=> esc_html__( 'Choose Gallery Images', 'tain-core' ),
			'id'	=> $prefix.'gallery',
			'type'	=> 'gallery',
			'tab'	=> esc_html__( 'Format', 'tain-core' )
		),
		array( 
			'type'	=> 'line',
			'tab'	=> esc_html__( 'Format', 'tain-core' )
		),
		array( 
			'label'	=> esc_html__( 'Google Map', 'tain-core' ),
			'desc'	=> esc_html__( 'This part for if you choosed google map format, then you must give google map lat, lang and map style.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Format', 'tain-core' ),
			'type'	=> 'label'
		),
		array( 
			'label'	=> esc_html__( 'Google Map Latitude', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter google latitude.', 'tain-core' ), 
			'id'	=> $prefix.'gmap_latitude',
			'tab'	=> esc_html__( 'Format', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Google Map Longitude', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter google longitude.', 'tain-core' ), 
			'id'	=> $prefix.'gmap_longitude',
			'tab'	=> esc_html__( 'Format', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Google Map Marker URL', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter google map custom marker url.', 'tain-core' ), 
			'id'	=> $prefix.'gmap_marker',
			'tab'	=> esc_html__( 'Format', 'tain-core' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Google Map Style', 'tain-core' ),
			'id'	=> $prefix.'gmap_style',
			'tab'	=> esc_html__( 'Format', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'standard' => esc_html__( 'Standard', 'tain-core' ),
				'silver' => esc_html__( 'Silver', 'tain-core' ),
				'retro' => esc_html__( 'Retro', 'tain-core' ),
				'dark' => esc_html__( 'Dark', 'tain-core' ),
				'night' => esc_html__( 'Night', 'tain-core' ),
				'aubergine' => esc_html__( 'Aubergine', 'tain-core' )
			),
			'default'	=> 'standard'
		),
		array( 
			'type'	=> 'line',
			'tab'	=> esc_html__( 'Format', 'tain-core' )
		),
	);
	// CPT Portfolio Options
	$portfolio_box = new Custom_Add_Meta_Box( 'tain_portfolio_metabox', esc_html__( 'Tain Portfolio Options', 'tain-core' ), $portfolio_fields, 'tain-portfolio', true );
	
	// CPT Portfolio Page Options
	$portfolio_page_box = new Custom_Add_Meta_Box( 'tain_portfolio_page_metabox', esc_html__( 'Tain Page Options', 'tain-core' ), $page_fields, 'tain-portfolio', true );
} // In theme option CPT option if portfolio exists
// Testimonial Options
if( is_array( $tain_cpt ) && in_array( "testimonial", $tain_cpt ) ){
	
	$prefix = 'tain_testimonial_';
	$testimonial_fields = array(	
		array( 
			'label'	=> esc_html__( 'Author Designation', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter author designation.', 'tain-core' ), 
			'id'	=> $prefix.'designation',
			'tab'	=> esc_html__( 'Testimonial', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Company Name', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter company name.', 'tain-core' ), 
			'id'	=> $prefix.'company_name',
			'tab'	=> esc_html__( 'Testimonial', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Company URL', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter company URL.', 'tain-core' ), 
			'id'	=> $prefix.'company_url',
			'tab'	=> esc_html__( 'Testimonial', 'tain-core' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Rating', 'tain-core' ),
			'desc'	=> esc_html__( 'Set user rating.', 'tain-core' ), 
			'id'	=> $prefix.'rating',
			'tab'	=> esc_html__( 'Testimonial', 'tain-core' ),
			'type'	=> 'rating',
			'default'	=> ''
		)
	);
	
	// CPT Testimonial Options
	$testimonial_box = new Custom_Add_Meta_Box( 'tain_testimonial_metabox', esc_html__( 'Tain Testimonial Options', 'tain-core' ), $testimonial_fields, 'tain-testimonial', true );
	
	// CPT Testimonial Page Options
	$testimonial_page_box = new Custom_Add_Meta_Box( 'tain_testimonial_page_metabox', esc_html__( 'Tain Page Options', 'tain-core' ), $page_fields, 'tain-testimonial', true );
	
} // In theme option CPT option if testimonial exists
// Team Options
if( is_array( $tain_cpt ) && in_array( "team", $tain_cpt ) ){
	
	$prefix = 'tain_team_';
	$team_fields = array(	
		array( 
			'label'	=> esc_html__( 'Member Designation', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter member designation.', 'tain-core' ), 
			'id'	=> $prefix.'designation',
			'tab'	=> esc_html__( 'Team', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Member Email', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter member email.', 'tain-core' ), 
			'id'	=> $prefix.'email',
			'tab'	=> esc_html__( 'Team', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Link Target', 'tain-core' ),
			'id'	=> $prefix.'link_target',
			'tab'	=> esc_html__( 'Social', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'_blank' => esc_html__( 'New Window', 'tain-core' ),
				'_self' => esc_html__( 'Self Window', 'tain-core' )
			),
			'default'	=> '_blank'
		),
		array( 
			'label'	=> esc_html__( 'Facebook', 'tain-core' ),
			'desc'	=> esc_html__( 'Facebook profile link.', 'tain-core' ), 
			'id'	=> $prefix.'facebook',
			'tab'	=> esc_html__( 'Social', 'tain-core' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Twitter', 'tain-core' ),
			'desc'	=> esc_html__( 'Twitter profile link.', 'tain-core' ), 
			'id'	=> $prefix.'twitter',
			'tab'	=> esc_html__( 'Social', 'tain-core' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Instagram', 'tain-core' ),
			'desc'	=> esc_html__( 'Instagram profile link.', 'tain-core' ), 
			'id'	=> $prefix.'instagram',
			'tab'	=> esc_html__( 'Social', 'tain-core' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Linkedin', 'tain-core' ),
			'desc'	=> esc_html__( 'Linkedin profile link.', 'tain-core' ), 
			'id'	=> $prefix.'linkedin',
			'tab'	=> esc_html__( 'Social', 'tain-core' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Pinterest', 'tain-core' ),
			'desc'	=> esc_html__( 'Pinterest profile link.', 'tain-core' ), 
			'id'	=> $prefix.'pinterest',
			'tab'	=> esc_html__( 'Social', 'tain-core' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Dribbble', 'tain-core' ),
			'desc'	=> esc_html__( 'Dribbble profile link.', 'tain-core' ), 
			'id'	=> $prefix.'dribbble',
			'tab'	=> esc_html__( 'Social', 'tain-core' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Flickr', 'tain-core' ),
			'desc'	=> esc_html__( 'Flickr profile link.', 'tain-core' ), 
			'id'	=> $prefix.'flickr',
			'tab'	=> esc_html__( 'Social', 'tain-core' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Youtube', 'tain-core' ),
			'desc'	=> esc_html__( 'Youtube profile link.', 'tain-core' ), 
			'id'	=> $prefix.'youtube',
			'tab'	=> esc_html__( 'Social', 'tain-core' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Vimeo', 'tain-core' ),
			'desc'	=> esc_html__( 'Vimeo profile link.', 'tain-core' ), 
			'id'	=> $prefix.'vimeo',
			'tab'	=> esc_html__( 'Social', 'tain-core' ),
			'type'	=> 'url',
			'default'	=> ''
		)
	);
	
	// CPT Team Options
	$team_box = new Custom_Add_Meta_Box( 'tain_team_metabox', esc_html__( 'Tain Team Options', 'tain-core' ), $team_fields, 'tain-team', true );
	
	// CPT Team Page Options
	$team_page_box = new Custom_Add_Meta_Box( 'tain_team_page_metabox', esc_html__( 'Tain Page Options', 'tain-core' ), $page_fields, 'tain-team', true );
	
} // In theme option CPT option if team exists
// Event Options
if( is_array( $tain_cpt ) && in_array( "events", $tain_cpt ) ){
	
	$prefix = 'tain_event_';
	$event_fields = array(	
		array( 
			'label'	=> esc_html__( 'Event Organiser Name', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter event organiser name.', 'tain-core' ), 
			'id'	=> $prefix.'organiser_name',
			'tab'	=> esc_html__( 'Events', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Event Organiser Designation', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter event organiser designation.', 'tain-core' ), 
			'id'	=> $prefix.'organiser_designation',
			'tab'	=> esc_html__( 'Events', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Event Start Date', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter event start date.', 'tain-core' ), 
			'id'	=> $prefix.'start_date',
			'tab'	=> esc_html__( 'Events', 'tain-core' ),
			'type'	=> 'date',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Event End Date', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter event end date.', 'tain-core' ), 
			'id'	=> $prefix.'end_date',
			'tab'	=> esc_html__( 'Events', 'tain-core' ),
			'type'	=> 'date',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Date Format', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter date format to show selcted event date. Example: F j, Y', 'tain-core' ), 
			'id'	=> $prefix.'date_format',
			'tab'	=> esc_html__( 'Events', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> 'F j, Y'
		),
		array( 
			'label'	=> esc_html__( 'Event Start Time', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter event start time.', 'tain-core' ), 
			'id'	=> $prefix.'time',
			'tab'	=> esc_html__( 'Events', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Event Cost', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter event cost.', 'tain-core' ), 
			'id'	=> $prefix.'cost',
			'tab'	=> esc_html__( 'Events', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Custom Link for Event Item', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter custom link to redirect custom event page.', 'tain-core' ), 
			'id'	=> $prefix.'link',
			'tab'	=> esc_html__( 'Events', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Custom Link Target', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose custom link target to new window or self window.', 'tain-core' ), 
			'id'	=> $prefix.'link_target',
			'tab'	=> esc_html__( 'Events', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'_blank' => esc_html__( 'New Window', 'tain-core' ),
				'_self' => esc_html__( 'Self Window', 'tain-core' )
			),
			'default'	=> '_blank'
		),
		array( 
			'label'	=> esc_html__( 'Custom Link Button Text', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter custom link buttom text: Example More About Event.', 'tain-core' ), 
			'id'	=> $prefix.'link_text',
			'tab'	=> esc_html__( 'Events', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Event Schedule Content', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter event schedule content. You can place here Shortcodes.', 'tain-core' ), 
			'id'	=> $prefix.'schedule_content',
			'tab'	=> esc_html__( 'Events', 'tain-core' ),
			'type'	=> 'textarea',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Venue Name', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter event venue name.', 'tain-core' ), 
			'id'	=> $prefix.'venue_name',
			'tab'	=> esc_html__( 'Address', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Venue Address', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter event venue address.', 'tain-core' ), 
			'id'	=> $prefix.'venue_address',
			'tab'	=> esc_html__( 'Address', 'tain-core' ),
			'type'	=> 'textarea',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'E-mail', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter email id for clarification about event.', 'tain-core' ), 
			'id'	=> $prefix.'email',
			'tab'	=> esc_html__( 'Address', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Phone', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter phone number for contact about event.', 'tain-core' ), 
			'id'	=> $prefix.'phone',
			'tab'	=> esc_html__( 'Address', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Website', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter event website.', 'tain-core' ), 
			'id'	=> $prefix.'website',
			'tab'	=> esc_html__( 'Address', 'tain-core' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Latitude', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter map latitude.', 'tain-core' ), 
			'id'	=> $prefix.'gmap_latitude',
			'tab'	=> esc_html__( 'GMap', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Longitude', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter map longitude.', 'tain-core' ), 
			'id'	=> $prefix.'gmap_longitude',
			'tab'	=> esc_html__( 'GMap', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Google Map Marker URL', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter google map custom marker url.', 'tain-core' ), 
			'id'	=> $prefix.'gmap_marker',
			'tab'	=> esc_html__( 'GMap', 'tain-core' ),
			'type'	=> 'url',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Google Map Style', 'tain-core' ),
			'id'	=> $prefix.'gmap_style',
			'tab'	=> esc_html__( 'GMap', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'standard' => esc_html__( 'Standard', 'tain-core' ),
				'silver' => esc_html__( 'Silver', 'tain-core' ),
				'retro' => esc_html__( 'Retro', 'tain-core' ),
				'dark' => esc_html__( 'Dark', 'tain-core' ),
				'night' => esc_html__( 'Night', 'tain-core' ),
				'aubergine' => esc_html__( 'Aubergine', 'tain-core' )
			),
			'default'	=> 'standard'
		),
		array( 
			'label'	=> esc_html__( 'Google Map Height', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter map height in values. Example 400', 'tain-core' ), 
			'id'	=> $prefix.'gmap_height',
			'tab'	=> esc_html__( 'GMap', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> '400'
		),
		array( 
			'label'	=> esc_html__( 'Contact Form', 'tain-core' ),
			'desc'	=> esc_html__( 'Contact form shortcode here.', 'tain-core' ), 
			'id'	=> $prefix.'contact_form',
			'tab'	=> esc_html__( 'Contact', 'tain-core' ),
			'type'	=> 'textarea',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Event Info Columns', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter column division values like given format. Example 3-3-6', 'tain-core' ), 
			'id'	=> $prefix.'col_layout',
			'tab'	=> esc_html__( 'Layout', 'tain-core' ),
			'type'	=> 'text',
			'default'	=> '3-3-6'
		),
		array( 
			'label'	=> esc_html__( 'Event Detail Items', 'tain-core' ),
			'desc'	=> esc_html__( 'This is layout settings for event.', 'tain-core' ), 
			'tab'	=> esc_html__( 'Layout', 'tain-core' ),
			'type'	=> 'dragdrop_multi',
			'id'	=> $prefix.'event_info_items',
			'dd_fields' => array ( 
				'Enable'  => array(
					'event-details' => esc_html__( 'Event Details', 'tain-core' ),
					'event-venue' => esc_html__( 'Event Venue', 'tain-core' ),
					'event-map' => esc_html__( 'Event Map', 'tain-core' )
				),
				'disabled' => array(
					'event-form'	=> esc_html__( 'Event Form', 'tain-core' ),
				)
			),
		),
		array( 
			'label'	=> esc_html__( 'Navigation', 'tain-core' ),
			'id'	=> $prefix.'nav_position',
			'tab'	=> esc_html__( 'Layout', 'tain-core' ),
			'type'	=> 'select',
			'options' => array ( 
				'top' => esc_html__( 'Top', 'tain-core' ),
				'bottom' => esc_html__( 'Bottom', 'tain-core' )
			),
			'default'	=> 'top'
		),
	);
	
	// CPT Events Options
	$event_box = new Custom_Add_Meta_Box( 'tain_event_metabox', esc_html__( 'Tain Event Options', 'tain-core' ), $event_fields, 'tain-events', true );
	
	// CPT Events Page Options
	$event_page_box = new Custom_Add_Meta_Box( 'tain_event_page_metabox', esc_html__( 'Tain Page Options', 'tain-core' ), $page_fields, 'tain-events', true );
	
} // In theme option CPT option if event exists
// Course Options
if( is_array( $tain_cpt ) && in_array( "courses", $tain_cpt ) ){
	
	$prefix = 'tain_course_';
	
	$course_fields = array(	
		array( 
			'label'	=> esc_html__( 'Course Price', 'tain-core' ),
			'desc'	=> esc_html__( 'Enter course price with your currency symbol. like <i class="dolor-icon-class"><i>150', 'tain-core' ), 
			'id'	=> $prefix.'price',
			'tab'	=> esc_html__( 'General', 'tain-core' ),
			'type'	=> 'textarea_with_icon_html',
			'default'	=> ''
		),
		array( 
			'label'	=> esc_html__( 'Course Image', 'tain-core' ),
			'desc'	=> esc_html__( 'Choose course image for show front.', 'tain-core' ), 
			'id'	=> $prefix.'title_img',
			'tab'	=> esc_html__( 'General', 'tain-core' ),
			'type'	=> 'image',
			'default'	=> ''
		)
	);
	
	// CPT Course Options
	$course_box = new Custom_Add_Meta_Box( 'tain_course_metabox', esc_html__( 'Tain Course Options', 'tain-core' ), $course_fields, 'tain-courses', true );
	
	// CPT Events Page Options
	$course_page_box = new Custom_Add_Meta_Box( 'tain_course_page_metabox', esc_html__( 'Tain Page Options', 'tain-core' ), $page_fields, 'tain-courses', true );
	
}