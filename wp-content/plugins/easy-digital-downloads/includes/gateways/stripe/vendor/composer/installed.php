<?php return array(
    'root' => array(
        'name' => 'easy-digital-downloads/edd-stripe',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => '24964d034941e0d3d07390ecc39570bfc64e7369',
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => false,
    ),
    'versions' => array(
        'easy-digital-downloads/edd-stripe' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '24964d034941e0d3d07390ecc39570bfc64e7369',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'stripe/stripe-php' => array(
            'pretty_version' => 'v7.47.0',
            'version' => '7.47.0.0',
            'reference' => 'b51656cb398d081fcee53a76f6edb8fd5c1a5306',
            'type' => 'library',
            'install_path' => __DIR__ . '/../stripe/stripe-php',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
