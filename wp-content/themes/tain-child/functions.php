<?php
/* =========================================
 * Enqueues parent theme stylesheet
 * ========================================= */

add_action( 'wp_enqueue_scripts', 'tain_enqueue_child_theme_styles', 30 );
function tain_enqueue_child_theme_styles() {
	wp_enqueue_style( 'tain-child-theme-style', get_stylesheet_uri(), array(), null );
}

function tain_child_theme_header_metadata() {

    // Post object if needed
    // global $post;

    // Page conditional if needed
    // if( is_page() ){}

  ?>

	<meta name="google-site-verification" content="4vMDpFgoXaRka4Dj5vtHOK8n0kH2fIny05tfhsMxhnY" />

  <?php

}
add_action( 'wp_head', 'tain_child_theme_header_metadata' );