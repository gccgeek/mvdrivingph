<?php
/**
 * The template for displaying 404 pages (not found)
 *
 */
get_header();

$ahe = new TainHeaderElements;
$aps = new TainPostSettings;
$template = 'page'; // template id
$aps->tain_set_post_template( $template );
$template_class = $aps->tain_template_content_class();
?>
<div class="wrap">
	<div id="primary" class="content-area error-404-area tain-page">
		<main id="main" class="site-main">
		
			<?php $ahe->tain_page_title( $template ); ?>
		
			<section class="error-404 not-found text-center">
				<div class="container">
					<header class="page-header">
						
						<div class="image-wrap-404">
							<img src="<?php echo esc_url( TAIN_ASSETS . '/images/404.png' ); ?>" alt="<?php esc_attr_e( 'Page Not Found', 'tain' ); ?>">
							
						</div>	
						
						<div class="relative mb-2">
							<h3 class="page-title"><?php esc_html_e( 'Page Not Found', 'tain' ); ?></h3>
						</div>
						<?php 
							$home_url = home_url( '/' ); 
						?>
							<p class="error-description">
								<?php esc_html_e( 'Sorry we cannot find that page!', 'tain' ); ?>
								<?php esc_html_e( 'Go back to home', 'tain' ); ?>
							</p>							
							<a class="home-link" href="<?php echo esc_url( $home_url ); ?>">
								<?php esc_html_e( 'Home Page', 'tain' ); ?>
							</a>
					</header><!-- .page-header -->
				</div><!-- .container -->
			</section><!-- .error-404 -->
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->
<?php get_footer();