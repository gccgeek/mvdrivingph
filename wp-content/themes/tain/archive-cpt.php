<?php
/**
 * The arcihve template for displaying all custom post types
 */
 
get_header(); 
$ahe = new TainHeaderElements;
$aps = new TainPostSettings;
$template = 'blog'; // template id
if( $aps->tain_check_template_exists( 'archive' ) ){
	$template = 'archive';
}
$aps->tain_set_post_template( $template );
$template_class = $aps->tain_template_content_class();
$full_width_class = '';
$acpt = new TainCPT;

?>
<div class="tain-content <?php echo esc_attr( 'tain-' . $template ); ?>">
		
		<?php $ahe->tain_header_slider('bottom'); ?>
		
		<?php $ahe->tain_page_title( $template ); ?>
		<div class="tain-content-inner">
			<div class="container">
	
				<div class="row">
					
					<div class="<?php echo esc_attr( $template_class['content_class'] ); ?>">
						<div id="primary" class="content-area">
							<?php
								$q_object = get_queried_object();
								$cpt = '';
								if( isset($q_object->name) )
									$cpt = $q_object->name;

								if( $cpt == 'tain-portfolio' ){
									$acpt->tain_cpt_call_tax_template( 'portfolio-archive' );
								}elseif( $cpt == 'tain-team' ){
									$acpt->tain_cpt_call_tax_template( 'team-archive' );
								}elseif( $cpt == 'tain-courses' ){
									$acpt->tain_cpt_call_tax_template( 'courses-archive' );
								}elseif( $cpt == 'tain-events' ){
									$acpt->tain_cpt_call_tax_template( 'events-archive' );
								}elseif( $cpt == 'tain-testimonial' ){
									$acpt->tain_cpt_call_tax_template( 'testimonial-archive' );
								}else{
									require_once get_template_directory() . '/template-parts/post/content-none.php';
								}
							?>				
						</div><!-- #primary -->
					</div><!-- main col -->
					
					<?php if( $template_class['lsidebar_class'] != '' ) : ?>
					<div class="<?php echo esc_attr( $template_class['lsidebar_class'] ); ?>">
						<aside class="widget-area left-widget-area<?php echo esc_attr( $template_class['sticky_class'] ); ?>">
							<?php dynamic_sidebar( $template_class['left_sidebar'] ); ?>
						</aside>
					</div><!-- sidebar col -->
					<?php endif; ?>
					
					<?php if( $template_class['rsidebar_class'] != '' ) : ?>
					<div class="<?php echo esc_attr( $template_class['rsidebar_class'] ); ?>">
						<aside class="widget-area right-widget-area<?php echo esc_attr( $template_class['sticky_class'] ); ?>">
							<?php dynamic_sidebar( $template_class['right_sidebar'] ); ?>
						</aside>
					</div><!-- sidebar col -->
					<?php endif; ?>
					
				</div><!-- row -->
			
		</div><!-- .container -->
	</div><!-- .tain-content-inner -->
</div><!-- .tain-content -->
<?php get_footer();