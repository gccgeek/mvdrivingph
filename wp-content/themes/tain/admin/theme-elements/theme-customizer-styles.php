<?php
if( !class_exists( "TainThemeStyles" ) ){
	require_once TAIN_INC . '/theme-class/theme-style-class.php';
}
$ats = new TainThemeStyles;
$theme_color = $ats->tain_theme_color();
$secondary_color = $ats->tain_secondary_color();

echo '.editor-styles-wrapper .wp-block, .editor-styles-wrapper .editor-block-list__block.wp-block[data-align=wide], .wp-block[data-align="wide"] {
	max-width: '. $ats->tain_container_width() .';
}';

echo '.editor-styles-wrapper .wp-block:not([data-align="full"]) {
	max-width: '. $ats->tain_container_width() .';
}';

echo '.editor-block-list__layout .editor-block-list__block,
.editor-styles-wrapper .editor-block-list__layout .editor-block-list__block p,
.block-editor__container .editor-styles-wrapper .mce-content-body,
.editor-styles-wrapper p,
.wp-block-button__link,.editor-styles-wrapper .wp-block-table__cell-content,
.block-editor-block-list__block {';
	$ats->tain_typo_ouput( 'body-typography' );
echo '
}';

echo '.post-type-post .editor-block-list__layout .editor-block-list__block {
	color: '. TainThemeStyles::tain_static_theme_mod( 'single-post-article-color' ) .'; }';

$ats->tain_custom_font_check( 'h1-typography' );
echo '.editor-block-list__layout .editor-block-list__block h1,
.editor-styles-wrapper .editor-post-title__block .editor-post-title__input, h1 {';
	$ats->tain_typo_ouput( 'h1-typography' );
echo '
}';
$ats->tain_custom_font_check( 'h2-typography' );
echo '.editor-block-list__layout .editor-block-list__block h2, h2{';
	$ats->tain_typo_ouput( 'h2-typography' );
echo '
}';
$ats->tain_custom_font_check( 'h3-typography' );
echo '.editor-block-list__layout .editor-block-list__block h3, h3{';
	$ats->tain_typo_ouput( 'h3-typography' );
echo '
}';
$ats->tain_custom_font_check( 'h4-typography' );
echo '.editor-block-list__layout .editor-block-list__block h4, h4, blockquote, blockquote p{';
	$ats->tain_typo_ouput( 'h4-typography' );
echo '
}';
$ats->tain_custom_font_check( 'h5-typography' );
echo '.editor-block-list__layout .editor-block-list__block h5, h5{';
	$ats->tain_typo_ouput( 'h5-typography' );
echo '
}';
$ats->tain_custom_font_check( 'h6-typography' );
echo '.editor-block-list__layout .editor-block-list__block h6, h6{';
	$ats->tain_typo_ouput( 'h6-typography' );
echo '
}';

$gen_link = TainThemeStyles::tain_static_theme_mod('single-post-article-link-color');
if( $gen_link ):
echo '.post-type-post .editor-block-list__layout .editor-block-list__block a{';
	$ats->tain_link_color( 'single-post-article-link-color', 'regular' );
echo '
}';
echo '.post-type-post .editor-block-list__layout .editor-block-list__block a:hover{';
	$ats->tain_link_color( 'single-post-article-link-color', 'hover' );
echo '
}';
echo '.post-type-post .editor-block-list__layout .editor-block-list__block a:active{';
	$ats->tain_link_color( 'single-post-article-link-color', 'active' );
echo '
}';
endif;

$gen_link = TainThemeStyles::tain_static_theme_mod('theme-link-color');
if( $gen_link ):
echo '.editor-block-list__layout .editor-block-list__block a{';
	$ats->tain_link_color( 'theme-link-color', 'regular' );
echo '
}';
echo '.editor-block-list__layout .editor-block-list__block a:hover{';
	$ats->tain_link_color( 'theme-link-color', 'hover' );
echo '
}';
echo '.editor-block-list__layout .editor-block-list__block a:active{';
	$ats->tain_link_color( 'theme-link-color', 'active' );
echo '
}';
endif;


echo '.editor-block-list__layout .editor-block-list__block .wp-block-button.is-style-outline .wp-block-button__link:not(.has-text-color),
.editor-block-list__layout .editor-block-list__block .wp-block-button.is-style-outline:hover .wp-block-button__link:not(.has-text-color),
.editor-block-list__layout .editor-block-list__block .wp-block-button.is-style-outline:focus .wp-block-button__link:not(.has-text-color),
.editor-block-list__layout .editor-block-list__block .wp-block-button.is-style-outline:active .wp-block-button__link:not(.has-text-color) {
color: '. esc_attr( $theme_color ) .'; /* base: #0073a8; */
}


/* Button colors */
.entry-content .wp-block-button.is-style-outline .wp-block-button__link:not(.has-text-color), 
.entry-content .wp-block-button.is-style-outline:hover .wp-block-button__link:not(.has-text-color) {
	color: '. esc_attr( $theme_color ) .'; /* base: #005177; */
}

.editor-block-list__layout .editor-block-list__block .wp-block-quote:not(.is-large):not(.is-style-large),
.editor-styles-wrapper blockquote.wp-block-quote.is-large,
.editor-styles-wrapper blockquote.wp-block-quote.is-style-large,body .editor-styles-wrapper blockquote,
.editor-styles-wrapper .wp-block-pullquote blockquote.has-light-gray-color {
border-color: '. esc_attr( $theme_color ) .' !important; /* base: #0073a8; */
}
.editor-block-list__layout .editor-block-list__block blockquote.wp-block-quote.is-large, 
.editor-block-list__layout .editor-block-list__block blockquote.wp-block-quote.is-style-large {
	border-left-color: '. esc_attr( $theme_color ) .';
}
.wp-block-quote[style*="text-align:right"], .wp-block-quote[style*="text-align: right"] {
	border-right-color: '. esc_attr( $theme_color ) .';
}
.editor-block-list__layout .editor-block-list__block .wp-block-pullquote.is-style-solid-color:not(.has-background-color) {
background-color: '. esc_attr( $theme_color ) .'; /* base: #0073a8; */
}

.editor-block-list__layout .editor-block-list__block .wp-block-file .wp-block-file__button,
.wp-block-button .wp-block-button__link, 
.editor-block-list__layout .editor-block-list__block .wp-block-button:not(.is-style-outline) .wp-block-button__link,
.editor-block-list__layout .editor-block-list__block .wp-block-button:not(.is-style-outline) .wp-block-button__link:active,
.editor-block-list__layout .editor-block-list__block .wp-block-button:not(.is-style-outline) .wp-block-button__link:focus,
.editor-block-list__layout .editor-block-list__block .wp-block-button:not(.is-style-outline) .wp-block-button__link:hover,
.wp-block-file .wp-block-file__button {
background-color: '. esc_attr( $theme_color ) .'; /* base: #0073a8; */
}


.wp-block-quote__citation:before, 
.wp-block-pullquote .wp-block-pullquote__citation:before,
blockquote cite a:before {
	background-color: '. esc_attr( $theme_color ) .'; /* base: #0073a8; */
}
.wp-block-quote__citation, 
.wp-block-freeform.block-library-rich-text__tinymce blockquote cite a, 
cite.wp-block-quote__citation,.wp-block-pullquote .wp-block-pullquote__citation,
.block-editor-block-list__block.wp-block-button.is-style-outline .wp-block-button__link {
	color: '. esc_attr( $theme_color ) .'; /* base: #005177; */
}
.editor-block-list__layout .editor-block-list__block .wp-block-file .wp-block-file__button:hover,
.editor-block-list__layout .editor-block-list__block .wp-block-button:not(.is-style-outline) .wp-block-button__link:active,
.editor-block-list__layout .editor-block-list__block .wp-block-button:not(.is-style-outline) .wp-block-button__link:focus,
.editor-block-list__layout .editor-block-list__block .wp-block-button:not(.is-style-outline) .wp-block-button__link:hover {
background-color: #333; /* base: #0073a8; */
}

/* Hover colors */
.editor-block-list__layout .editor-block-list__block .wp-block-file .wp-block-file__textlink:hover {
color: '. esc_attr( $theme_color ) .'; /* base: #005177; */
}
.editor-block-list__layout figure.wp-block-pullquote {
    border-bottom: none;
    border-top: none;
}
.wp-block-button__link {
	color: #fff;
}
body .wp-block-button__link:hover, body a.wp-block-button__link:hover {
    background: #333 !important;
    color: #fff !important;
}
.block-editor-block-list__layout .block-editor-block-list__block {
    line-height: 1.5em;
}
/* Do not overwrite solid color pullquote or cover links */
.editor-block-list__layout .editor-block-list__block .wp-block-pullquote.is-style-solid-color a,
.editor-block-list__layout .editor-block-list__block .wp-block-cover a {
color: inherit;
}';

