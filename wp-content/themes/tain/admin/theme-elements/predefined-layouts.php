<?php

/*
 * Customizer Header/Footer Pre-Defined Layouts
 */
 
if( is_customize_preview() ){
	add_action( 'init', 'tain_demo_header', 10 );
	add_action( 'customize_save_after', 'tain_demo_header_save' );
	function tain_demo_header_save(){ tain_demo_header( 'save' ); }
}
function tain_demo_header( $stat = '' ){

	$tain_options = '';
	if( is_customize_preview() ){
		$tain_mod_t = get_option( 'tain_theme_options_t');
		$tain_options = !empty( $tain_mod_t ) ? $tain_mod_t : get_option( 'tain_theme_options_new');
	}

	$header_items = $topbar_items = '';
	$header_template = isset( $tain_options['header-template'] ) && $tain_options['header-template'] ? $tain_options['header-template'] : '';
	
	//Header 1
	if( $header_template == '1' ) {
		
		$header_items = '{"Normal":{"header-topbar":"Top Bar","header-logo":"Logo Section","header-nav":"Nav Bar"},"Sticky":{},"disabled":{}}';
		$tain_options['header-items'] = json_decode( $header_items, true );
		
		$header_topbar_items = '{"disabled":{"header-topbar-text-2":"Custom Text 2","header-topbar-text-3":"Custom Text 3","header-topbar-search":"Search","header-cart":"Cart","header-topbar-date":"Date","header-topbar-search-toggle":"Search Toggle","header-topbar-menu":"Top Bar Menu","header-phone":"Phone Number","header-address":"Address Text","header-email":"Email"},"Left":{"header-topbar-text-1":"Custom Text 1"},"Center":{},"Right":{"header-topbar-social":"Social"}}';
		$tain_options['header-topbar-items'] = json_decode( $header_topbar_items, true );

		$header_logobar_items = '{"disabled":{"header-logobar-social":"Social","header-logobar-search":"Search","header-logobar-secondary-toggle":"Secondary Toggle","header-phone":"Phone Number","header-address":"Address Text","header-email":"Email","header-logobar-menu":"Main Menu","header-logobar-search-toggle":"Search Toggle","header-logobar-text-1":"Custom Text 1","header-logobar-text-2":"Custom Text 2","header-logobar-text-3":"Custom Text 3","header-cart":"Cart"},"Left":{"header-logobar-logo":"Logo","header-logobar-sticky-logo":"Sticky Logo"},"Center":{},"Right":{}}';
		$tain_options['header-logobar-items'] = json_decode( $header_logobar_items, true );
		
		$header_navbar_items = '{"disabled":{"header-navbar-logo":"Logo","header-navbar-sticky-logo":"Stikcy Logo","header-navbar-text-1":"Custom Text 1","header-navbar-text-2":"Custom Text 2","header-navbar-text-3":"Custom Text 3","header-navbar-social":"Social","header-navbar-search":"Search","header-phone":"Phone Number","header-address":"Address Text","header-email":"Email","header-cart":"Cart"},"Left":{"header-navbar-menu":"Main Menu"},"Center":{},"Right":{"header-navbar-secondary-toggle":"Secondary Toggle","header-navbar-search-toggle":"Search Toggle"}}';
		$tain_options['header-navbar-items'] = json_decode( $header_navbar_items, true );
		
		$tain_options['header-topbar-text-1'] = isset( $tain_options['header-topbar-text-1'] ) && !empty( $tain_options['header-topbar-text-1'] ) ? $tain_options['header-topbar-text-1'] : 'Welcome to our website';
		$tain_options['social-icons-type'] = isset( $tain_options['social-icons-type'] ) ? 'squared' : '';
		
	//Header 2
	}elseif( $header_template == '2' ) {
		$header_items = '{"Normal":{"header-topbar":"Top Bar","header-nav":"Nav Bar"},"Sticky":{},"disabled":{"header-logo":"Logo Section"}}';
		$tain_options['header-items'] = json_decode( $header_items, true );

		$header_topbar_items = '{"disabled":{"header-topbar-text-2":"Custom Text 2","header-topbar-text-3":"Custom Text 3","header-topbar-search":"Search","header-cart":"Cart","header-topbar-date":"Date","header-topbar-search-toggle":"Search Toggle","header-topbar-menu":"Top Bar Menu","header-topbar-social":"Social"},"Left":{"header-topbar-text-1":"Custom Text 1"},"Center":{},"Right":{"header-address":"Address Text","header-phone":"Phone Number","header-email":"Email"}}';
		$tain_options['header-topbar-items'] = json_decode( $header_topbar_items, true );
		
		$header_navbar_items = '{"disabled":{"header-navbar-text-1":"Custom Text 1","header-navbar-text-2":"Custom Text 2","header-navbar-text-3":"Custom Text 3","header-navbar-social":"Social","header-navbar-search":"Search","header-phone":"Phone Number","header-address":"Address Text","header-email":"Email","header-cart":"Cart"},"Left":{"header-navbar-logo":"Logo","header-navbar-sticky-logo":"Stikcy Logo"},"Center":{},"Right":{"header-navbar-menu":"Main Menu","header-navbar-search-toggle":"Search Toggle","header-navbar-secondary-toggle":"Secondary Toggle"}}';
		$tain_options['header-navbar-items'] = json_decode( $header_navbar_items, true );
		
		$tain_options['header-topbar-text-1'] = isset( $tain_options['header-topbar-text-1'] ) && !empty( $tain_options['header-topbar-text-1'] ) ? $tain_options['header-topbar-text-1'] : 'Welcome to our website';
		$tain_options['header-phone-text'] = isset( $tain_options['header-phone-text'] ) && !empty( $tain_options['header-phone-text'] ) ? $tain_options['header-phone-text'] : '1234567890';
		$tain_options['header-address-text'] = isset( $tain_options['header-address-text'] ) && !empty( $tain_options['header-address-text'] ) ? $tain_options['header-address-text'] : 'Street name, City name';
		$tain_options['header-email-text'] = isset( $tain_options['header-email-text'] ) && !empty( $tain_options['header-email-text'] ) ? $tain_options['header-email-text'] : 'info@website.com';
		
	//Header 3
	}elseif( $header_template == '3' ) {
		$header_items = '{"Normal":{"header-logo":"Logo Section","header-nav":"Nav Bar"},"Sticky":{},"disabled":{"header-topbar":"Top Bar"}}';
		$tain_options['header-items'] = json_decode( $header_items, true );
		
		$header_logobar_items = '{"disabled":{"header-logobar-search":"Search","header-phone":"Phone Number","header-address":"Address Text","header-logobar-search-toggle":"Search Toggle","header-email":"Email","header-logobar-text-1":"Custom Text 1","header-logobar-text-2":"Custom Text 2","header-logobar-text-3":"Custom Text 3","header-logobar-menu":"Main Menu","header-cart":"Cart"},"Left":{"header-logobar-social":"Social"},"Center":{"header-logobar-logo":"Logo","header-logobar-sticky-logo":"Sticky Logo"},"Right":{"header-logobar-secondary-toggle":"Secondary Toggle"}}';
		$tain_options['header-logobar-items'] = json_decode( $header_logobar_items, true );

		$header_navbar_items = '{"disabled":{"header-navbar-text-1":"Custom Text 1","header-navbar-text-2":"Custom Text 2","header-navbar-text-3":"Custom Text 3","header-navbar-social":"Social","header-navbar-search":"Search","header-phone":"Phone Number","header-address":"Address Text","header-navbar-logo":"Logo","header-email":"Email","header-cart":"Cart","header-navbar-secondary-toggle":"Secondary Toggle","header-navbar-sticky-logo":"Stikcy Logo"},"Left":{},"Center":{"header-navbar-menu":"Main Menu","header-navbar-search-toggle":"Search Toggle"},"Right":{}}';
		$tain_options['header-navbar-items'] = json_decode( $header_navbar_items, true );
		
	//Header 5
	}elseif( $header_template == '4' ) {
		$header_items = '{"Normal":{"header-logo":"Logo Section","header-nav":"Nav Bar"},"Sticky":{},"disabled":{"header-topbar":"Top Bar"}}';
		$tain_options['header-items'] = json_decode( $header_items, true );
		
		$header_logobar_items = '{"disabled":{"header-logobar-search":"Search","header-phone":"Phone Number","header-logobar-text-1":"Custom Text 1","header-address":"Address Text","header-logobar-search-toggle":"Search Toggle","header-email":"Email","header-logobar-text-2":"Custom Text 2","header-logobar-social":"Social","header-logobar-text-3":"Custom Text 3","header-logobar-menu":"Main Menu","header-logobar-secondary-toggle":"Secondary Toggle","header-cart":"Cart"},"Left":{"header-logobar-sticky-logo":"Sticky Logo","header-logobar-logo":"Logo"},"Center":{},"Right":{"multi-info":"Address, Phone, Email"}}';
		$tain_options['header-logobar-items'] = json_decode( $header_logobar_items, true );
		
		$header_navbar_items = '{"disabled":{"header-navbar-text-1":"Custom Text 1","header-navbar-text-2":"Custom Text 2","header-navbar-text-3":"Custom Text 3","header-navbar-social":"Social","header-navbar-search":"Search","header-phone":"Phone Number","header-address":"Address Text","header-navbar-logo":"Logo","header-email":"Email","header-cart":"Cart","header-navbar-secondary-toggle":"Secondary Toggle","header-navbar-sticky-logo":"Stikcy Logo"},"Left":{"header-navbar-menu":"Main Menu"},"Center":{},"Right":{"header-navbar-search-toggle":"Search Toggle"}}';
		$tain_options['header-navbar-items'] = json_decode( $header_navbar_items, true );
		
		$tain_options['header-address-label'] = isset( $tain_options['header-address-label'] ) && !empty( $tain_options['header-address-label'] ) ? $tain_options['header-address-label'] : 'Street';
		$tain_options['header-phone-label'] = isset( $tain_options['header-phone-label'] ) && !empty( $tain_options['header-phone-label'] ) ? $tain_options['header-phone-label'] : 'Contact';
		$tain_options['header-email-label'] = isset( $tain_options['header-email-label'] ) && !empty( $tain_options['header-email-label'] ) ? $tain_options['header-email-label'] : 'Email';
		$tain_options['header-address-text'] = isset( $tain_options['header-address-text'] ) && !empty( $tain_options['header-address-text'] ) ? $tain_options['header-address-text'] : '123, Snow Street, CA';
		$tain_options['header-phone-text'] = isset( $tain_options['header-phone-text'] ) && !empty( $tain_options['header-phone-text'] ) ? $tain_options['header-phone-text'] : '+123 456 789';
		$tain_options['header-email-text'] = isset( $tain_options['header-email-text'] ) && !empty( $tain_options['header-email-text'] ) ? $tain_options['header-email-text'] : 'info@email.com';
	
	//Header 6
	}elseif( $header_template == '5' ) {
		$header_items = '{"Normal":{"header-nav":"Nav Bar"},"Sticky":{},"disabled":{"header-logo":"Logo Section","header-topbar":"Top Bar"}}';
		$tain_options['header-items'] = json_decode( $header_items, true );
		
		$header_navbar_items = '{"disabled":{"header-navbar-text-1":"Custom Text 1","header-navbar-text-2":"Custom Text 2","header-navbar-text-3":"Custom Text 3","header-navbar-social":"Social","header-navbar-menu":"Main Menu","header-navbar-search":"Search","header-phone":"Phone Number","header-address":"Address Text","header-navbar-search-toggle":"Search Toggle","header-email":"Email","header-cart":"Cart"},"Left":{"header-navbar-logo":"Logo","header-navbar-sticky-logo":"Stikcy Logo"},"Center":{},"Right":{"header-navbar-secondary-toggle":"Secondary Toggle"}}';
		$tain_options['header-navbar-items'] = json_decode( $header_navbar_items, true );
		
	//Header 7
	}elseif( $header_template == '6' ) {
		$header_items = '{"Normal":{"header-nav":"Nav Bar"},"Sticky":{},"disabled":{"header-logo":"Logo Section","header-topbar":"Top Bar"}}';
		$tain_options['header-items'] = json_decode( $header_items, true );
		
		$header_navbar_items = '{"disabled":{"header-navbar-text-1":"Custom Text 1","header-navbar-text-2":"Custom Text 2","header-navbar-text-3":"Custom Text 3","header-navbar-social":"Social","header-navbar-search":"Search","header-phone":"Phone Number","header-navbar-secondary-toggle":"Secondary Toggle","header-address":"Address Text","header-email":"Email"},"Left":{"header-navbar-logo":"Logo","header-navbar-sticky-logo":"Stikcy Logo"},"Center":{},"Right":{"header-navbar-menu":"Main Menu","header-cart":"Cart","header-navbar-search-toggle":"Search Toggle"}}';
		$tain_options['header-navbar-items'] = json_decode( $header_navbar_items, true );
		
	//Header 8
	}elseif( $header_template == '7' ) {
		$header_items = '{"Normal":{"header-nav":"Nav Bar"},"Sticky":{},"disabled":{"header-logo":"Logo Section","header-topbar":"Top Bar"}}';
		$tain_options['header-items'] = json_decode( $header_items, true );
		
		$header_navbar_items = '{"disabled":{"header-navbar-text-1":"Custom Text 1","header-navbar-text-2":"Custom Text 2","header-navbar-text-3":"Custom Text 3","header-navbar-social":"Social","header-navbar-search":"Search","header-phone":"Phone Number","header-navbar-secondary-toggle":"Secondary Toggle","header-address":"Address Text","header-email":"Email"},"Left":{"header-navbar-logo":"Logo","header-navbar-sticky-logo":"Stikcy Logo"},"Center":{},"Right":{"header-navbar-menu":"Main Menu","header-cart":"Cart","header-navbar-search-toggle":"Search Toggle"}}';
		$tain_options['header-navbar-items'] = json_decode( $header_navbar_items, true );

	//Header 9
	}elseif( $header_template == '8' ) {
		$header_items = '{"Normal":{"header-nav":"Nav Bar"},"Sticky":{},"disabled":{"header-logo":"Logo Section","header-topbar":"Top Bar"}}';
		$tain_options['header-items'] = json_decode( $header_items, true );
		
		$header_navbar_items = '{"disabled":{"header-navbar-text-1":"Custom Text 1","header-navbar-text-2":"Custom Text 2","header-navbar-text-3":"Custom Text 3","header-navbar-social":"Social","header-navbar-search":"Search","header-phone":"Phone Number","header-navbar-secondary-toggle":"Secondary Toggle","header-address":"Address Text","header-email":"Email"},"Left":{"header-navbar-logo":"Logo","header-navbar-sticky-logo":"Stikcy Logo"},"Center":{},"Right":{"header-navbar-menu":"Main Menu","header-cart":"Cart","header-navbar-search-toggle":"Search Toggle"}}';
		$tain_options['header-navbar-items'] = json_decode( $header_navbar_items, true );
	
	//Header 10
	}elseif( $header_template == '9' ) {
		$header_items = '{"Normal":{"header-logo":"Logo Section","header-nav":"Nav Bar"},"Sticky":{},"disabled":{"header-topbar":"Top Bar"}}';
		$tain_options['header-items'] = json_decode( $header_items, true );
		
		$header_logobar_items = '{"disabled":{"header-logobar-search":"Search","header-phone":"Phone Number","header-address":"Address Text","header-logobar-search-toggle":"Search Toggle","header-email":"Email","header-logobar-text-2":"Custom Text 2","header-logobar-social":"Social","header-logobar-text-3":"Custom Text 3","header-logobar-menu":"Main Menu","header-logobar-secondary-toggle":"Secondary Toggle","header-cart":"Cart","header-logobar-text-1":"Custom Text 1"},"Left":{},"Center":{"header-logobar-logo":"Logo","header-logobar-sticky-logo":"Sticky Logo"},"Right":{}}';
		$tain_options['header-logobar-items'] = json_decode( $header_logobar_items, true );
		
		$header_navbar_items = '{"disabled":{"header-navbar-text-1":"Custom Text 1","header-navbar-text-2":"Custom Text 2","header-navbar-text-3":"Custom Text 3","header-navbar-social":"Social","header-navbar-search":"Search","header-phone":"Phone Number","header-navbar-secondary-toggle":"Secondary Toggle","header-address":"Address Text","header-email":"Email","header-cart":"Cart","header-navbar-logo":"Logo","header-navbar-sticky-logo":"Stikcy Logo"},"Left":{},"Center":{"header-navbar-menu":"Main Menu","header-navbar-search-toggle":"Search Toggle"},"Right":{}}';
		$tain_options['header-navbar-items'] = json_decode( $header_navbar_items, true );
	
	//Header 11
	}elseif( $header_template == '10' ) {
		$header_items = '{"Normal":{"header-topbar":"Top Bar","header-nav":"Nav Bar"},"Sticky":{},"disabled":{"header-logo":"Logo Section"}}';
		$tain_options['header-items'] = json_decode( $header_items, true );
		
		$header_topbar_items = '{"disabled":{"header-topbar-text-2":"Custom Text 2","header-topbar-text-3":"Custom Text 3","header-topbar-search":"Search","header-cart":"Cart","header-topbar-date":"Date","header-topbar-search-toggle":"Search Toggle","header-topbar-menu":"Top Bar Menu","header-topbar-social":"Social"},"Left":{"header-topbar-text-1":"Custom Text 1"},"Center":{},"Right":{"header-address":"Address Text","header-phone":"Phone Number","header-email":"Email"}}';
		$tain_options['header-topbar-items'] = json_decode( $header_topbar_items, true );
		
		$header_navbar_items = '{"disabled":{"header-navbar-secondary-toggle":"Secondary Toggle","header-navbar-text-1":"Custom Text 1","header-navbar-text-2":"Custom Text 2","header-navbar-text-3":"Custom Text 3","header-navbar-social":"Social","header-navbar-search":"Search","header-phone":"Phone Number","header-address":"Address Text","header-email":"Email"},"Left":{"header-navbar-logo":"Logo","header-navbar-sticky-logo":"Stikcy Logo"},"Center":{},"Right":{"header-navbar-menu":"Main Menu","header-cart":"Cart","header-navbar-search-toggle":"Search Toggle"}}';
		$tain_options['header-navbar-items'] = json_decode( $header_navbar_items, true );
		
		$tain_options['header-topbar-text-1'] = isset( $tain_options['header-topbar-text-1'] ) && !empty( $tain_options['header-topbar-text-1'] ) ? $tain_options['header-topbar-text-1'] : 'Welcome to our website';
		$tain_options['header-phone-text'] = isset( $tain_options['header-phone-text'] ) && !empty( $tain_options['header-phone-text'] ) ? $tain_options['header-phone-text'] : '1234567890';
		$tain_options['header-address-text'] = isset( $tain_options['header-address-text'] ) && !empty( $tain_options['header-address-text'] ) ? $tain_options['header-address-text'] : 'Street name, City name';
		$tain_options['header-email-text'] = isset( $tain_options['header-email-text'] ) && !empty( $tain_options['header-email-text'] ) ? $tain_options['header-email-text'] : 'info@website.com';
	
	//Header 12
	}elseif( $header_template == '11' ) {
		$header_items = '{"Normal":{"header-nav":"Nav Bar"},"Sticky":{},"disabled":{"header-topbar":"Top Bar","header-logo":"Logo Section"}}';
		$tain_options['header-items'] = json_decode( $header_items, true );
		
		$header_navbar_items = '{"disabled":{"header-navbar-secondary-toggle":"Secondary Toggle","header-navbar-text-1":"Custom Text 1","header-navbar-text-2":"Custom Text 2","header-navbar-text-3":"Custom Text 3","header-navbar-social":"Social","header-navbar-search":"Search","header-phone":"Phone Number","header-address":"Address Text","header-email":"Email"},"Left":{"header-navbar-logo":"Logo","header-navbar-sticky-logo":"Stikcy Logo"},"Center":{},"Right":{"header-navbar-menu":"Main Menu","header-navbar-search-toggle":"Search Toggle","header-cart":"Cart"}}';
		$tain_options['header-navbar-items'] = json_decode( $header_navbar_items, true );

	//Header 15
	}elseif( $header_template == '12' ) {
		$header_items = '{"Normal":{"header-logo":"Logo Section","header-nav":"Nav Bar"},"Sticky":{},"disabled":{"header-topbar":"Top Bar"}}';
		$tain_options['header-items'] = json_decode( $header_items, true );
		
		$header_logobar_items = '{"disabled":{"header-logobar-search":"Search","header-logobar-search-toggle":"Search Toggle","header-email":"Email","header-logobar-text-2":"Custom Text 2","header-logobar-social":"Social","header-logobar-text-3":"Custom Text 3","header-logobar-menu":"Main Menu","header-logobar-secondary-toggle":"Secondary Toggle","header-cart":"Cart","header-logobar-text-1":"Custom Text 1"},"Left":{"header-phone":"Phone Number"},"Center":{"header-logobar-logo":"Logo","header-logobar-sticky-logo":"Sticky Logo"},"Right":{"header-address":"Address Text"}}';
		$tain_options['header-logobar-items'] = json_decode( $header_logobar_items, true );
		
		$header_navbar_items = '{"disabled":{"header-navbar-text-1":"Custom Text 1","header-navbar-text-2":"Custom Text 2","header-navbar-text-3":"Custom Text 3","header-navbar-social":"Social","header-navbar-search":"Search","header-phone":"Phone Number","header-navbar-secondary-toggle":"Secondary Toggle","header-address":"Address Text","header-email":"Email","header-cart":"Cart","header-navbar-logo":"Logo","header-navbar-sticky-logo":"Stikcy Logo"},"Left":{},"Center":{"header-navbar-menu":"Main Menu","header-navbar-search-toggle":"Search Toggle"},"Right":{}}';
		$tain_options['header-navbar-items'] = json_decode( $header_navbar_items, true );

	//Header 16
	}elseif( $header_template == '13' ) {
		$header_items = '{"Normal":{"header-topbar":"Top Bar","header-logo":"Logo Section","header-nav":"Nav Bar"},"Sticky":{},"disabled":{}}';
		$tain_options['header-items'] = json_decode( $header_items, true );
		
		$header_topbar_items = '{"disabled":{"header-topbar-text-2":"Custom Text 2","header-topbar-text-3":"Custom Text 3","header-topbar-search":"Search","header-cart":"Cart","header-topbar-date":"Date","header-topbar-search-toggle":"Search Toggle","header-topbar-menu":"Top Bar Menu","header-topbar-social":"Social"},"Left":{"header-topbar-text-1":"Custom Text 1"},"Center":{},"Right":{"header-address":"Address Text","header-phone":"Phone Number","header-email":"Email"}}';
		$tain_options['header-topbar-items'] = json_decode( $header_topbar_items, true );
		
		$header_logobar_items = '{"disabled":{"header-logobar-search":"Search","header-phone":"Phone Number","header-logobar-text-1":"Custom Text 1","header-address":"Address Text","header-logobar-search-toggle":"Search Toggle","header-email":"Email","header-logobar-text-2":"Custom Text 2","header-logobar-social":"Social","header-logobar-text-3":"Custom Text 3","header-logobar-menu":"Main Menu","header-logobar-secondary-toggle":"Secondary Toggle","header-cart":"Cart"},"Left":{"header-logobar-sticky-logo":"Sticky Logo","header-logobar-logo":"Logo"},"Center":{},"Right":{"multi-info":"Address, Phone, Email"}}';
		$tain_options['header-logobar-items'] = json_decode( $header_logobar_items, true );
		
		$header_navbar_items = '{"disabled":{"header-navbar-text-1":"Custom Text 1","header-navbar-text-2":"Custom Text 2","header-navbar-text-3":"Custom Text 3","header-navbar-social":"Social","header-phone":"Phone Number","header-address":"Address Text","header-email":"Email","header-cart":"Cart","header-navbar-search-toggle":"Search Toggle","header-navbar-secondary-toggle":"Secondary Toggle","header-navbar-logo":"Logo","header-navbar-sticky-logo":"Stikcy Logo"},"Left":{"header-navbar-menu":"Main Menu"},"Center":{},"Right":{"header-navbar-search":"Search"}}';
		$tain_options['header-navbar-items'] = json_decode( $header_navbar_items, true );
		
		$tain_options['header-address-label'] = isset( $tain_options['header-address-label'] ) && !empty( $tain_options['header-address-label'] ) ? $tain_options['header-address-label'] : 'Street';
		$tain_options['header-phone-label'] = isset( $tain_options['header-phone-label'] ) && !empty( $tain_options['header-phone-label'] ) ? $tain_options['header-phone-label'] : 'Contact';
		$tain_options['header-email-label'] = isset( $tain_options['header-email-label'] ) && !empty( $tain_options['header-email-label'] ) ? $tain_options['header-email-label'] : 'Email';
		$tain_options['header-address-text'] = isset( $tain_options['header-address-text'] ) && !empty( $tain_options['header-address-text'] ) ? $tain_options['header-address-text'] : '123, Snow Street, CA';
		$tain_options['header-phone-text'] = isset( $tain_options['header-phone-text'] ) && !empty( $tain_options['header-phone-text'] ) ? $tain_options['header-phone-text'] : '+123 456 789';
		$tain_options['header-email-text'] = isset( $tain_options['header-email-text'] ) && !empty( $tain_options['header-email-text'] ) ? $tain_options['header-email-text'] : 'info@email.com';

	//Header 17
	}elseif( $header_template == 'vertical' ) {
		$tain_options['header-type'] = 'left-sticky';
		$header_fixed_items = '{"disabled":{"header-fixed-text-1":"Custom Text 1","header-fixed-text-2":"Custom Text 2","header-fixed-social":"Social"},"Top":{"header-fixed-logo":"Logo"},"Middle":{"header-fixed-menu":"Menu"},"Bottom":{"header-fixed-search":"Search Form"}}';
		$tain_options['header-fixed-items'] = json_decode( $header_fixed_items, true );
	}
	
	if( $header_template != 'vertical' && $header_template != 'custom' ) $tain_options['header-type'] = 'default';
	
	//Footer Template
	$footer_template = isset( $tain_options['footer-template'] ) && $tain_options['footer-template'] ? $tain_options['footer-template'] : '';
	if( $footer_template == '1' ) {
		$tain_options['footer-top-layout'] = '12';
		$tain_options['footer-top-sidebar-1'] = 'sidebar-6';
		$tain_options['footer-top-container'] = 'wide';
		$tain_options['footer-middle-sidebar-1'] = 'sidebar-2';
		
		$footer_items = '{"Enabled":{"footer-top":"Footer Top","footer-middle":"Footer Middle","footer-bottom":"Footer Bottom"},"disabled":{}}';
		$tain_options['footer-items'] = json_decode( $footer_items, true );
		
		$footer_bottom_items = '{"disabled":{"social":"Footer Social Links","widget":"Custom Widget"},"Left":[],"Center":{"menu":"Footer Menu","copyright":"Copyright Text"},"Right":[]}';
		$tain_options['footer-bottom-items'] = json_decode( $footer_bottom_items, true );
		
	}elseif( $footer_template == '2' ) {
		$tain_options['footer-bottom-container'] = 'boxed';
		$footer_items = '{"Enabled":{"footer-bottom":"Footer Bottom"},"disabled":{"footer-top":"Footer Top","footer-middle":"Footer Middle"}}';
		$tain_options['footer-items'] = json_decode( $footer_items, true );
		
		$footer_bottom_items = '{"disabled":{"widget":"Custom Widget","menu":"Footer Menu"},"Left":{"copyright":"Copyright Text"},"Center":[],"Right":{"social":"Footer Social Links"}}';
		$tain_options['footer-bottom-items'] = json_decode( $footer_bottom_items, true );
		
	}elseif( $footer_template == '3' ) {
		$tain_options['footer-middle-container'] = 'boxed';
		$tain_options['footer-middle-sidebar-1'] = 'sidebar-2';
		$tain_options['footer-middle-sidebar-2'] = 'sidebar-3';
		$tain_options['footer-middle-sidebar-3'] = 'sidebar-4';
		
		$footer_items = '{"Enabled":{"footer-middle":"Footer Middle","footer-bottom":"Footer Bottom"},"disabled":{"footer-top":"Footer Top"}}';
		$tain_options['footer-items'] = json_decode( $footer_items, true );
		
		$footer_bottom_items = '{"disabled":{"widget":"Custom Widget","menu":"Footer Menu","social":"Footer Social Links"},"Left":[],"Center":{"copyright":"Copyright Text"},"Right":[]}';
		$tain_options['footer-bottom-items'] = json_decode( $footer_bottom_items, true );
		
	}elseif( $footer_template == '4' ) {
		$tain_options['footer-middle-layout'] = '3-3-3-3';
		$tain_options['footer-middle-container'] = 'boxed';
		$tain_options['footer-middle-sidebar-1'] = 'sidebar-2';
		$tain_options['footer-middle-sidebar-2'] = 'sidebar-3';
		$tain_options['footer-middle-sidebar-3'] = 'sidebar-4';
		$tain_options['footer-middle-sidebar-4'] = 'sidebar-5';
		
		$footer_items = '{"Enabled":{"footer-middle":"Footer Middle","footer-bottom":"Footer Bottom"},"disabled":{"footer-top":"Footer Top"}}';
		$tain_options['footer-items'] = json_decode( $footer_items, true );
		
		$footer_bottom_items = '{"disabled":{"widget":"Custom Widget","menu":"Footer Menu","social":"Footer Social Links"},"Left":[],"Center":{"copyright":"Copyright Text"},"Right":[]}';
		$tain_options['footer-bottom-items'] = json_decode( $footer_bottom_items, true );
		
	}elseif( $footer_template == '5' ) {
		$tain_options['footer-middle-layout'] = '3-3-3-3';
		$tain_options['footer-middle-container'] = 'boxed';
		$tain_options['footer-middle-sidebar-1'] = 'sidebar-2';
		$tain_options['footer-middle-sidebar-2'] = 'sidebar-3';
		$tain_options['footer-middle-sidebar-3'] = 'sidebar-4';
		$tain_options['footer-middle-sidebar-4'] = 'sidebar-5';
		
		$footer_items = '{"Enabled":{"footer-middle":"Footer Middle","footer-bottom":"Footer Bottom"},"disabled":{"footer-top":"Footer Top"}}';
		$tain_options['footer-items'] = json_decode( $footer_items, true );
		
		$footer_bottom_items = '{"disabled":{"widget":"Custom Widget","menu":"Footer Menu"},"Left":{"social":"Footer Social Links"},"Center":[],"Right":{"copyright":"Copyright Text"}}';
		$tain_options['footer-bottom-items'] = json_decode( $footer_bottom_items, true );
		
	}elseif( $footer_template == '6' ) {
		$tain_options['footer-top-layout'] = '12';
		$tain_options['footer-top-sidebar-1'] = 'sidebar-6'; 
		$tain_options['footer-middle-layout'] = '3-6-3';
		$tain_options['footer-middle-container'] = 'boxed';
		$tain_options['footer-middle-sidebar-1'] = 'sidebar-2';
		$tain_options['footer-middle-sidebar-2'] = 'sidebar-3';
		$tain_options['footer-middle-sidebar-3'] = 'sidebar-4';
		$tain_options['footer-middle-sidebar-4'] = 'sidebar-5';
		
		$footer_items = '{"Enabled":{"footer-top":"Footer Top","footer-middle":"Footer Middle","footer-bottom":"Footer Bottom"},"disabled":[]}';
		$tain_options['footer-items'] = json_decode( $footer_items, true );
		
		$footer_bottom_items = '{"disabled":{"widget":"Custom Widget","menu":"Footer Menu"},"Left":{"social":"Footer Social Links"},"Center":[],"Right":{"copyright":"Copyright Text"}}';
		$tain_options['footer-bottom-items'] = json_decode( $footer_bottom_items, true );
		
	}elseif( $footer_template == '7' ) {
		$tain_options['footer-middle-layout'] = '6-6';
		$tain_options['footer-middle-container'] = 'boxed';
		$tain_options['footer-middle-sidebar-1'] = 'sidebar-2';
		$tain_options['footer-middle-sidebar-2'] = 'sidebar-3';
		
		$footer_items = '{"Enabled":{"footer-middle":"Footer Middle","footer-bottom":"Footer Bottom"},"disabled":{"footer-top":"Footer Top"}}';
		$tain_options['footer-items'] = json_decode( $footer_items, true );
		
		$footer_bottom_items = '{"disabled":{"widget":"Custom Widget","menu":"Footer Menu","social":"Footer Social Links"},"Left":{},"Center":{"copyright":"Copyright Text"},"Right":[]}';
		$tain_options['footer-bottom-items'] = json_decode( $footer_bottom_items, true );
		
	}elseif( $footer_template == '8' ) {
		$tain_options['footer-middle-layout'] = '12';
		$tain_options['footer-middle-container'] = 'boxed';
		$tain_options['footer-middle-sidebar-1'] = 'sidebar-2';
		
		$footer_items = '{"Enabled":{"footer-middle":"Footer Middle","footer-bottom":"Footer Bottom"},"disabled":{"footer-top":"Footer Top"}}';
		$tain_options['footer-items'] = json_decode( $footer_items, true );
		
		$footer_bottom_items = '{"disabled":{"widget":"Custom Widget","menu":"Footer Menu","social":"Footer Social Links"},"Left":{},"Center":{"copyright":"Copyright Text"},"Right":[]}';
		$tain_options['footer-bottom-items'] = json_decode( $footer_bottom_items, true );
		
	}
	
	if( $stat == 'save' ){
		update_option( 'tain_theme_options_new', $tain_options );
		TainThemeOpt::$tain_mod = $tain_options;
	}else{
		update_option( 'tain_theme_options_t', $tain_options );
		TainThemeOpt::$tain_mod = $tain_options;
	}

}