<?php

//Theme Option -> Skin
$theme_skin_panel = new Tain_WP_Customize_Panel( $wp_customize, 'theme_skin_panel', array(
	'title'			=> esc_html__( 'Skin', 'tain' ),
	'description'	=> esc_html__( 'These are theme skin/color options', 'tain' ),
	'priority'		=> 3,
	'panel'			=> 'tain_theme_panel'
));
$wp_customize->add_panel( $theme_skin_panel );

//General -> Theme Skin
$tain_theme_skin_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_theme_skin_section', array(
	'title'			=> esc_html__( 'Theme Skin', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for theme skin', 'tain' ),
	'priority'		=> 1,
	'panel'			=> 'theme_skin_panel'
));
$wp_customize->add_section( $tain_theme_skin_section );

//Theme Skin
$wp_customize->add_setting('ajax_trigger_tain_theme_skin_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_theme_skin_section', array(
	'section'		=> 'tain_theme_skin_section'
)));

//General -> Body Background
$tain_body_skin_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_body_skin_section', array(
	'title'			=> esc_html__( 'Body Background', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for theme body background.', 'tain' ),
	'priority'		=> 2,
	'panel'			=> 'theme_skin_panel'
));
$wp_customize->add_section( $tain_body_skin_section );

//Body Background
$wp_customize->add_setting('ajax_trigger_tain_body_skin_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_body_skin_section', array(
	'section'		=> 'tain_body_skin_section'
)));