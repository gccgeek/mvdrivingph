<?php

//Theme Option -> Footer
$theme_footer_panel = new Tain_WP_Customize_Panel( $wp_customize, 'theme_footer_panel', array(
	'title'			=> esc_html__( 'Footer', 'tain' ),
	'description'	=> esc_html__( 'These are header general settings of tain theme', 'tain' ),
	'priority'		=> 6,
	'panel'			=> 'tain_theme_panel'
));
$wp_customize->add_panel( $theme_footer_panel );

//Footer -> Footer General
$tain_footer_general_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_footer_general_section', array(
	'title'			=> esc_html__( 'Footer General', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for general footer.', 'tain' ),
	'priority'		=> 1,
	'panel'			=> 'theme_footer_panel'
));
$wp_customize->add_section( $tain_footer_general_section );

//Footer General
$wp_customize->add_setting('ajax_trigger_tain_footer_general_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_footer_general_section', array(
	'section'		=> 'tain_footer_general_section'
)));

//Footer -> Footer Top
$tain_footer_top_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_footer_top_section', array(
	'title'			=> esc_html__( 'Footer Top', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for footer top section.', 'tain' ),
	'priority'		=> 2,
	'panel'			=> 'theme_footer_panel'
));
$wp_customize->add_section( $tain_footer_top_section );

//Footer Top
$wp_customize->add_setting('ajax_trigger_tain_footer_top_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_footer_top_section', array(
	'section'		=> 'tain_footer_top_section'
)));

//Footer -> Footer Middle
$tain_footer_middle_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_footer_middle_section', array(
	'title'			=> esc_html__( 'Footer Middle', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for footer middle section.', 'tain' ),
	'priority'		=> 3,
	'panel'			=> 'theme_footer_panel'
));
$wp_customize->add_section( $tain_footer_middle_section );

//Footer Middle
$wp_customize->add_setting('ajax_trigger_tain_footer_middle_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_footer_middle_section', array(
	'section'		=> 'tain_footer_middle_section'
)));

//Footer -> Footer Bottom
$tain_footer_bottom_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_footer_bottom_section', array(
	'title'			=> esc_html__( 'Footer Bottom', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for footer bottom section.', 'tain' ),
	'priority'		=> 4,
	'panel'			=> 'theme_footer_panel'
));
$wp_customize->add_section( $tain_footer_bottom_section );

//Footer Bottom
$wp_customize->add_setting('ajax_trigger_tain_footer_bottom_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_footer_bottom_section', array(
	'section'		=> 'tain_footer_bottom_section'
)));