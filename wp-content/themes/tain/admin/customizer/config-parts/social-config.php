<?php

//Theme Option -> Social
$theme_social_panel = new Tain_WP_Customize_Panel( $wp_customize, 'theme_social_panel', array(
	'title'			=> esc_html__( 'Social', 'tain' ),
	'description'	=> esc_html__( 'These are the social settings of tain Theme', 'tain' ),
	'priority'		=> 9,
	'panel'			=> 'tain_theme_panel'
));
$wp_customize->add_panel( $theme_social_panel );

//Header -> Social Links
$tain_social_links_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_social_links_section', array(
	'title'			=> esc_html__( 'Social Links', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for social links.', 'tain' ),
	'priority'		=> 1,
	'panel'			=> 'theme_social_panel'
));
$wp_customize->add_section( $tain_social_links_section );

//Social Links
$wp_customize->add_setting('ajax_trigger_tain_social_links_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_social_links_section', array(
	'section'		=> 'tain_social_links_section'
)));

//Header -> Social Share
$tain_social_share_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_social_share_section', array(
	'title'			=> esc_html__( 'Social Share', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for social share.', 'tain' ),
	'priority'		=> 2,
	'panel'			=> 'theme_social_panel'
));
$wp_customize->add_section( $tain_social_share_section );

//Social Share
$wp_customize->add_setting('ajax_trigger_tain_social_share_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_social_share_section', array(
	'section'		=> 'tain_social_share_section'
)));