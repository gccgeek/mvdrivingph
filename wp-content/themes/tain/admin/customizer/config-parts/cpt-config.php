<?php

//Theme Option -> Custom Post Type
$cpt_config_panel = new Tain_WP_Customize_Panel( $wp_customize, 'cpt_config_panel', array(
	'title'			=> esc_html__( 'Custom Post Types', 'tain' ),
	'description'	=> esc_html__( 'Custom Post Type Settings Area.', 'tain' ),
	'priority'		=> 10,
	'panel'			=> 'tain_theme_panel'
));
$wp_customize->add_panel( $cpt_config_panel );

//Custom Post Type -> General Settings
$tain_cpt_general_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_cpt_general_section', array(
	'title'			=> esc_html__( 'General Settings', 'tain' ),
	'description'	=> esc_html__( 'This is custom post type settings area.', 'tain' ),
	'priority'		=> 1,
	'panel'			=> 'cpt_config_panel'
));
$wp_customize->add_section( $tain_cpt_general_section );

//General Settings
$wp_customize->add_setting('ajax_trigger_tain_cpt_general_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_cpt_general_section', array(
	'section'		=> 'tain_cpt_general_section'
)));

//Custom Post Types
$tain_options = get_option( 'tain_theme_options_new' );
$selected_cpt = isset( $tain_options['cpt-opts'] ) && !empty( $tain_options['cpt-opts'] ) ? $tain_options['cpt-opts'] : '';

//Custom Post Type -> Team
if( !empty( $selected_cpt ) && is_array( $selected_cpt ) && in_array( 'team', $selected_cpt ) ){
	
	$tain_cpt_team_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_cpt_team_section', array(
		'title'			=> esc_html__( 'Team', 'tain' ),
		'description'	=> esc_html__( 'This is custom post type team setting.', 'tain' ),
		'priority'		=> 2,
		'panel'			=> 'cpt_config_panel'
	));
	$wp_customize->add_section( $tain_cpt_team_section );

	//Team
	$wp_customize->add_setting('ajax_trigger_tain_cpt_team_section', array(
		'default'           => '',
		'sanitize_callback' 	=> 'esc_attr'
	));
	$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_cpt_team_section', array(
		'section'		=> 'tain_cpt_team_section'
	)));

}// team CPT check end

//Custom Post Type -> Testimonial
if( !empty( $selected_cpt ) && is_array( $selected_cpt ) && in_array( 'testimonial', $selected_cpt ) ){
	$tain_cpt_testimonial_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_cpt_testimonial_section', array(
		'title'			=> esc_html__( 'Testimonial', 'tain' ),
		'description'	=> esc_html__( 'This is custom post type testimonial setting.', 'tain' ),
		'priority'		=> 3,
		'panel'			=> 'cpt_config_panel'
	));
	$wp_customize->add_section( $tain_cpt_testimonial_section );
	
	//Testimonial
	$wp_customize->add_setting('ajax_trigger_tain_cpt_testimonial_section', array(
		'default'           => '',
		'sanitize_callback' 	=> 'esc_attr'
	));
	$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_cpt_testimonial_section', array(
		'section'		=> 'tain_cpt_testimonial_section'
	)));
	
}// testimonial CPT check end

//Custom Post Type -> Events
if( !empty( $selected_cpt ) && is_array( $selected_cpt ) && in_array( 'events', $selected_cpt ) ){
	$tain_cpt_events_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_cpt_events_section', array(
		'title'			=> esc_html__( 'Events', 'tain' ),
		'description'	=> esc_html__( 'This is custom post type events setting.', 'tain' ),
		'priority'		=> 4,
		'panel'			=> 'cpt_config_panel'
	));
	$wp_customize->add_section( $tain_cpt_events_section );

	//Events
	$wp_customize->add_setting('ajax_trigger_tain_cpt_events_section', array(
		'default'           => '',
		'sanitize_callback' 	=> 'esc_attr'
	));
	$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_cpt_events_section', array(
		'section'		=> 'tain_cpt_events_section'
	)));
	
}// events CPT check end

//Custom Post Type -> Courses
if( !empty( $selected_cpt ) && is_array( $selected_cpt ) && in_array( 'courses', $selected_cpt ) ){
	$tain_cpt_courses_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_cpt_courses_section', array(
		'title'			=> esc_html__( 'Courses', 'tain' ),
		'description'	=> esc_html__( 'This is custom post type Courses setting.', 'tain' ),
		'priority'		=> 5,
		'panel'			=> 'cpt_config_panel'
	));
	$wp_customize->add_section( $tain_cpt_courses_section );
	
	//Courses
	$wp_customize->add_setting('ajax_trigger_tain_cpt_courses_section', array(
		'default'           => '',
		'sanitize_callback' 	=> 'esc_attr'
	));
	$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_cpt_courses_section', array(
		'section'		=> 'tain_cpt_courses_section'
	)));
	
}// courses CPT check end

//Custom Post Type -> Portfolio
if( !empty( $selected_cpt ) && is_array( $selected_cpt ) && in_array( 'portfolio', $selected_cpt ) ){
	$tain_cpt_portfolio_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_cpt_portfolio_section', array(
		'title'			=> esc_html__( 'Portfolio', 'tain' ),
		'description'	=> esc_html__( 'This is custom post type portfolio setting.', 'tain' ),
		'priority'		=> 6,
		'panel'			=> 'cpt_config_panel'
	));
	$wp_customize->add_section( $tain_cpt_portfolio_section );
	
	//Portfolio
	$wp_customize->add_setting('ajax_trigger_tain_cpt_portfolio_section', array(
		'default'           => '',
		'sanitize_callback' 	=> 'esc_attr'
	));
	$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_cpt_portfolio_section', array(
		'section'		=> 'tain_cpt_portfolio_section'
	)));
	
}// portfolio CPT check end