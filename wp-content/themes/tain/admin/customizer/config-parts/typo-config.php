<?php

//Theme Option -> Typography
$theme_typography_panel = new Tain_WP_Customize_Panel( $wp_customize, 'theme_typography_panel', array(
	'title'			=> esc_html__( 'Typography', 'tain' ),
	'description'	=> esc_html__( 'These are the theme typography options', 'tain' ),
	'priority'		=> 4,
	'panel'			=> 'tain_theme_panel'
));
$wp_customize->add_panel( $theme_typography_panel );

//Typography -> General Typography
$tain_general_typography_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_general_typography_section', array(
	'title'			=> esc_html__( 'General Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for theme general typography', 'tain' ),
	'priority'		=> 1,
	'panel'			=> 'theme_typography_panel'
));
$wp_customize->add_section( $tain_general_typography_section );

//General Typography
$wp_customize->add_setting('ajax_trigger_tain_general_typography_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_general_typography_section', array(
	'section'		=> 'tain_general_typography_section'
)));

//Typography -> Widgets Typography
$tain_widget_typography_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_widget_typography_section', array(
	'title'			=> esc_html__( 'Widgets Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for theme widgets typography', 'tain' ),
	'priority'		=> 2,
	'panel'			=> 'theme_typography_panel'
));
$wp_customize->add_section( $tain_widget_typography_section );

//Widgets Typography
$wp_customize->add_setting('ajax_trigger_tain_widget_typography_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_widget_typography_section', array(
	'section'		=> 'tain_widget_typography_section'
)));

//Typography -> Menu Typography
$tain_menu_typography_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_menu_typography_section', array(
	'title'			=> esc_html__( 'Menu Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for theme menu typography', 'tain' ),
	'priority'		=> 3,
	'panel'			=> 'theme_typography_panel'
));
$wp_customize->add_section( $tain_menu_typography_section );

//Menu Typography
$wp_customize->add_setting('ajax_trigger_tain_menu_typography_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_menu_typography_section', array(
	'section'		=> 'tain_menu_typography_section'
)));


//Typography -> Other Typography
$tain_menu_other_typography_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_menu_other_typography_section', array(
	'title'			=> esc_html__( 'Other Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for other typography', 'tain' ),
	'priority'		=> 4,
	'panel'			=> 'theme_typography_panel'
));
$wp_customize->add_section( $tain_menu_other_typography_section );

//Other Typography
$wp_customize->add_setting('ajax_trigger_tain_menu_other_typography_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_menu_other_typography_section', array(
	'section'		=> 'tain_menu_other_typography_section'
)));