<?php

//Logo
$settings = array(
	'id'			=> 'logo',
	'type'			=> 'image',
	'title'			=> esc_html__( 'Logo', 'tain' ),
	'description'	=> esc_html__( 'Upload site logo', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Logo Height
$settings = array(
	'id'			=> 'logo-height',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Header Logo Height', 'tain' ),
	'description'	=> esc_html__( 'Here you can set your maximum height of your logo. Here no need to put dimension units like px, em etc. Example 50', 'tain' ),
	'default'		=> '70',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Sticky Logo
$settings = array(
	'id'			=> 'sticky-logo',
	'type'			=> 'image',
	'title'			=> esc_html__( 'Sticky Logo', 'tain' ),
	'description'	=> esc_html__( 'Upload site sticky logo', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Sticky Logo Height
$settings = array(
	'id'			=> 'sticky-logo-height',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Header Sticky Logo Height', 'tain' ),
	'description'	=> esc_html__( 'Here you can set your maximum height of your Sticky logo. Here no need to put dimension units like px, em etc. Example 50', 'tain' ),
	'default'		=> '60',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Mobile Logo
$settings = array(
	'id'			=> 'mobile-logo',
	'type'			=> 'image',
	'title'			=> esc_html__( 'Mobile Logo', 'tain' ),
	'description'	=> esc_html__( 'Upload site mobile logo', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Mobile Logo Height
$settings = array(
	'id'			=> 'mobile-logo-height',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Header Mobile Logo Height', 'tain' ),
	'description'	=> esc_html__( 'Here you can set your maximum height of your mobile logo. Here no need to put dimension units like px, em etc. Example 50', 'tain' ),
	'default'		=> '50',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );