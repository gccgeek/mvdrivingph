<?php

//Header Layout Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Layout', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Header Layout
$settings = array(
	'id'			=> 'header-layout',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Header Width', 'tain' ),
	'description'	=> esc_html__( 'Choose header width boxed or wide.', 'tain' ),
	'choices'		=> array(
		'boxed'		=> esc_html__( 'Boxed', 'tain' ),
		'wide'		=> esc_html__( 'Wide', 'tain' ),
		'full'		=> esc_html__( 'Full Width', 'tain' )
	),
	'default'		=> 'wide',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Template
$settings = array(
	'id'			=> 'header-template',
	'type'			=> 'radioimage',
	'title'			=> esc_html__( 'Header Template', 'tain' ),
	'description'	=> esc_html__( 'Choose header template. Current header layout needed option values you can see after refresh this customizer page.', 'tain' ),
	'default'		=> '1',
	'items' 		=> array(
		'1'		=> TAIN_ADMIN_URL . '/customizer/assets/images/header-layouts/1.jpg',
		'2'		=> TAIN_ADMIN_URL . '/customizer/assets/images/header-layouts/2.jpg',
		'3'		=> TAIN_ADMIN_URL . '/customizer/assets/images/header-layouts/3.jpg',
		'4'		=> TAIN_ADMIN_URL . '/customizer/assets/images/header-layouts/4.jpg',
		'5'		=> TAIN_ADMIN_URL . '/customizer/assets/images/header-layouts/5.jpg',
		'6'		=> TAIN_ADMIN_URL . '/customizer/assets/images/header-layouts/6.jpg',
		'7'		=> TAIN_ADMIN_URL . '/customizer/assets/images/header-layouts/7.jpg',
		'8'		=> TAIN_ADMIN_URL . '/customizer/assets/images/header-layouts/8.jpg',
		'9'		=> TAIN_ADMIN_URL . '/customizer/assets/images/header-layouts/9.jpg',
		'10'	=> TAIN_ADMIN_URL . '/customizer/assets/images/header-layouts/10.jpg',
		'11'	=> TAIN_ADMIN_URL . '/customizer/assets/images/header-layouts/11.jpg',
		'12'	=> TAIN_ADMIN_URL . '/customizer/assets/images/header-layouts/12.jpg',
		'13'	=> TAIN_ADMIN_URL . '/customizer/assets/images/header-layouts/13.jpg',
		'vertical'	=> TAIN_ADMIN_URL . '/customizer/assets/images/header-layouts/14.jpg',
		'custom'=> TAIN_ADMIN_URL . '/customizer/assets/images/header-layouts/custom.jpg'
	),
	'cols'			=> '1',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Type
$settings = array(
	'id'			=> 'header-type',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Header Type', 'tain' ),
	'description'	=> esc_html__( 'Select header type for matching your site. If you choose left/right header type, manage under "Header -> Left/Right Navbar"', 'tain' ),
	'choices'		=> array(
		'default'		=> esc_html__( 'Default', 'tain' ),
		'left-sticky'	=> esc_html__( 'Left Nav', 'tain' ),
		'right-sticky'	=> esc_html__( 'Right Nav', 'tain' ),
	),
	'default'		=> 'default',
	'required'		=> array( 'header-template', '=', 'custom' ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Items
$settings = array(
	'id'			=> 'header-items',
	'type'			=> 'dragdrop',
	'title'			=> esc_html__( 'Header Items', 'tain' ),
	'description'	=> esc_html__( 'Needed items for header, drag from disabled and put enabled.', 'tain' ),
	'default' 		=> array(
		'Normal' 	=> array(
			'header-nav'	=> esc_html__( 'Nav Bar', 'tain' )
		),
		'Sticky' 	=> array(),
		'disabled' 	=> array(
			'header-topbar'	=> esc_html__( 'Top Bar', 'tain' ),
			'header-logo'	=> esc_html__( 'Logo Section', 'tain' )
		)
	),
	'required'		=> array( 'header-type', '=', 'default' ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Clik and Edit Header Layouts End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Clik and Edit Header Style
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Style', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Header Background Settings
$settings = array(
	'id'			=> 'header-background',
	'type'			=> 'background',
	'title'			=> esc_html__( 'Header Background Settings', 'tain' ),
	'description'	=> esc_html__( 'This is settings for header background with image, color, etc.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Dropdown Menu Background
$settings = array(
	'id'			=> 'dropdown-menu-background',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Dropdown Menu Background', 'tain' ),
	'description'	=> esc_html__( 'Choose dropdown menu background color.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Dropdown Menu Color
$settings = array(
	'id'			=> 'dropdown-menu-link-color',
	'type'			=> 'link',
	'title'			=> esc_html__( 'Dropdown Menu Color', 'tain' ),
	'description'	=> esc_html__( 'Choose dropdown menu link color.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Dropdown Menu Border
$settings = array(
	'id'			=> 'dropdown-menu-border',
	'type'			=> 'border',
	'title'			=> esc_html__( 'Dropdown Menu Border', 'tain' ),
	'description'	=> esc_html__( 'Here you can set border. No need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Clik and Edit Header Style End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Header Label Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Custom Text', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Address Label
$settings = array(
	'id'			=> 'header-address-label',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Address Label', 'tain' ),
	'description'	=> esc_html__( 'This is the address label field, you can assign here any label custom text.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Address Custom Text
$settings = array(
	'id'			=> 'header-address-text',
	'type'			=> 'textarea',
	'title'			=> esc_html__( 'Address Custom Text', 'tain' ),
	'description'	=> esc_html__( 'This is the address field, you can assign here any custom text. Few HTML allowed here.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Phone Number Label
$settings = array(
	'id'			=> 'header-phone-label',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Phone Number Label', 'tain' ),
	'description'	=> esc_html__( 'This is the phone number label field, you can assign here any phone label custom text.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Phone Number Custom Text
$settings = array(
	'id'			=> 'header-phone-text',
	'type'			=> 'textarea',
	'title'			=> esc_html__( 'Phone Number Custom Text', 'tain' ),
	'description'	=> esc_html__( 'This is the phone number field, you can assign here any custom text. Few HTML allowed here.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Email Label
$settings = array(
	'id'			=> 'header-email-label',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Email Label', 'tain' ),
	'description'	=> esc_html__( 'This is the email label field, you can assign here any email custom label text.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Email Custom Text
$settings = array(
	'id'			=> 'header-email-text',
	'type'			=> 'textarea',
	'title'			=> esc_html__( 'Email Custom Text', 'tain' ),
	'description'	=> esc_html__( 'This is the email field, you can assign here any email id. Example companyname@yourdomain.com', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Label End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Header Advanced Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Advanced', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Header Absolute
$settings = array(
	'id'			=> 'header-absolute',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Header Absolute', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable header absolute option to show transparent header for page.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Sticky Part
$settings = array(
	'id'			=> 'sticky-part',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Header Sticky Part', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable stciky part to sticky which items are placed in sticky part at header items.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Sticky Scroll Up
$settings = array(
	'id'			=> 'sticky-part-scrollup',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Sticky Scroll Up', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable stciky part to sticky only scroll up. This also only sticky which items are placed in Sticky Part at Header Items.', 'tain' ),
	'default'		=> 0,
	'required'		=> array( 'sticky-part', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Main Menu Type
$settings = array(
	'id'			=> 'mainmenu-menutype',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Main Menu Type', 'tain' ),
	'description'	=> esc_html__( 'This is settings for mainmenu.', 'tain' ),
	'choices'		=> array(
		'advanced' => esc_html__( 'Advanced Menu', 'tain' ),
		'normal' => esc_html__( 'Normal Menu', 'tain' ),
	),
	'default'		=> 'normal',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Menu Tag
$settings = array(
	'id'			=> 'menu-tag',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Menu Tag', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable menu tag for menu items like Hot, Trend, New.', 'tain' ),
	'default'		=> 0,
	'required'		=> array( 'mainmenu-menutype', '=', 'advanced' ),
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Hot Menu Tag Text
$settings = array(
	'id'			=> 'menu-tag-hot-text',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Hot Menu Tag Text', 'tain' ),
	'description'	=> esc_html__( 'Set this text to show hot menu tag.', 'tain' ),
	'default'		=> esc_html__( 'Hot', 'tain' ),
	'required'		=> array( 'menu-tag', '=', 1 ),
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Hot Menu Tag Background
$settings = array(
	'id'			=> 'menu-tag-hot-bg',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Hot Menu Tag Background', 'tain' ),
	'description'	=> esc_html__( 'Set this text to show hot menu tag.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'menu-tag', '=', 1 ),
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//New Menu Tag Text
$settings = array(
	'id'			=> 'menu-tag-new-text',
	'type'			=> 'text',
	'title'			=> esc_html__( 'New Menu Tag Text', 'tain' ),
	'description'	=> esc_html__( 'Set this text to show new menu tag.', 'tain' ),
	'default'		=> esc_html__( 'New', 'tain' ),
	'required'		=> array( 'menu-tag', '=', 1 ),
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//New Menu Tag Background
$settings = array(
	'id'			=> 'menu-tag-new-bg',
	'type'			=> 'color',
	'title'			=> esc_html__( 'New Menu Tag Background', 'tain' ),
	'description'	=> esc_html__( 'Set new menu tag background color.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'menu-tag', '=', 1 ),
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Trend Menu Tag Text
$settings = array(
	'id'			=> 'menu-tag-trend-text',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Trend Menu Tag Text', 'tain' ),
	'description'	=> esc_html__( 'Set this text to show trend menu tag.', 'tain' ),
	'default'		=> esc_html__( 'Trend', 'tain' ),
	'required'		=> array( 'menu-tag', '=', 1 ),
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Trend Menu Tag Background
$settings = array(
	'id'			=> 'menu-tag-trend-bg',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Trend Menu Tag Background', 'tain' ),
	'description'	=> esc_html__( 'Set trend menu tag background color.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'menu-tag', '=', 1 ),
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Secondary Menu
$settings = array(
	'id'			=> 'secondary-menu',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Secondary Menu', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable secondary menu.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Secondary Menu Type
$settings = array(
	'id'			=> 'secondary-menu-type',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Secondary Menu Type', 'tain' ),
	'description'	=> esc_html__( 'Choose secondary menu type.', 'tain' ),
	'choices'		=> array(
		'left-push'		=> esc_html__( 'Left Push', 'tain' ),
		'left-overlay'	=> esc_html__( 'Left Overlay', 'tain' ),
		'right-push'	=> esc_html__( 'Right Push', 'tain' ),
		'right-overlay'	=> esc_html__( 'Right Overlay', 'tain' ),
		'full-overlay'	=> esc_html__( 'Full Page Overlay', 'tain' )
	),
	'default'		=> 'right-overlay',
	'required'		=> array( 'secondary-menu', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Secondary Menu Space Width
$settings = array(
	'id'			=> 'secondary-menu-space-width',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Secondary Menu Space Width', 'tain' ),
	'description'	=> esc_html__( 'Increase or decrease secondary menu space width. this options only use if you enable secondary menu. Here no need to specify dimensions like px, em, etc.. Example 350', 'tain' ),
	'default'		=> '350',
	'required'		=> array( 'secondary-menu', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Slider Position
$settings = array(
	'id'			=> 'header-slider-position',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Header Slider Position', 'tain' ),
	'description'	=> esc_html__( 'Select header slider position matching your page.', 'tain' ),
	'choices'		=> array(
		'bottom'	=> esc_html__( 'Below Header', 'tain' ),
		'top'		=> esc_html__( 'Above Header', 'tain' ),
		'none'		=> esc_html__( 'None', 'tain' )
	),
	'default'		=> 'none',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Search Toggle Modal
$settings = array(
	'id'			=> 'search-toggle-form',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Search Toggle Modal', 'tain' ),
	'description'	=> esc_html__( 'Select serach box toggle modal.', 'tain' ),
	'choices'		=> array(
		'1'	=> esc_html__( 'Full Screen Search', 'tain' ),
		'2' => esc_html__( 'Text Box Toggle Search', 'tain' ),
		'3' => esc_html__( 'Full Bar Toggle Search', 'tain' ),
		'4' => esc_html__( 'Bottom Seach Box Toggle', 'tain' ),
	),
	'default'		=> '1',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Advanced End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );