<?php

//Layout Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Layout', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Top Sliding Bar Enable
$settings = array(
	'id'			=> 'header-top-sliding-switch',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Top Sliding Bar Enable', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable top sliding bar. Here you can show you sidebars width column based.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Top Sliding Bar Show on Devices
$settings = array(
	'id'			=> 'header-top-sliding-device',
	'type'			=> 'multicheck',
	'title'			=> esc_html__( 'Top Sliding Bar Show on Devices', 'tain' ),
	'description'	=> esc_html__( 'Enable or disable top sliding bar for mobile, tab or desktop. This option from big devices. If desktop not enable and tab enable means it\'s hide sliding bar all the devices.', 'tain' ),
	'default'		=> array( 'desktop', 'tab' ),
	'items' 		=> array(
		'desktop' 	=> esc_html__( 'Desktop', 'tain' ),
		'tab' 		=> esc_html__( 'Tablet', 'tain' ),
		'mobile' 	=> esc_html__( 'Mobile', 'tain' )
	),
	'required'		=> array( 'header-top-sliding-switch', '=', 1 ),
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Header Top Slide Columns
$settings = array(
	'id'			=> 'header-top-sliding-cols',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Header Top Slide Columns', 'tain' ),
	'description'	=> esc_html__( 'Choose top sliding bar columns.', 'tain' ),
	'choices'		=> array(
		'3'		=> esc_html__( '4 Columns', 'tain' ),
		'4'		=> esc_html__( '3 Columns', 'tain' ),
		'6'		=> esc_html__( '2 Columns', 'tain' ),
		'12'	=> esc_html__( '1 Column', 'tain' )
	),
	'default'		=> '3',
	'required'		=> array( 'header-top-sliding-switch', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Choose First Column
$settings = array(
	'id'			=> 'header-top-sliding-sidebar-1',
	'type'			=> 'sidebars',
	'title'			=> esc_html__( 'Choose First Column', 'tain' ),
	'description'	=> esc_html__( 'Select widget area for showing first column of top sliding bar.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'header-top-sliding-switch', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Choose Second Column
$settings = array(
	'id'			=> 'header-top-sliding-sidebar-2',
	'type'			=> 'sidebars',
	'title'			=> esc_html__( 'Choose Second Column', 'tain' ),
	'description'	=> esc_html__( 'Select widget area for showing second column of top sliding bar.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'header-top-sliding-switch', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Choose Third Column
$settings = array(
	'id'			=> 'header-top-sliding-sidebar-3',
	'type'			=> 'sidebars',
	'title'			=> esc_html__( 'Choose Third Column', 'tain' ),
	'description'	=> esc_html__( 'Select widget area for showing third column of top sliding bar.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'header-top-sliding-switch', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Choose Fourth Column
$settings = array(
	'id'			=> 'header-top-sliding-sidebar-4',
	'type'			=> 'sidebars',
	'title'			=> esc_html__( 'Choose Fourth Column', 'tain' ),
	'description'	=> esc_html__( 'Select widget area for showing fourth column of top sliding bar.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'header-top-sliding-switch', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Layout End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Style Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Style', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Top Sliding Bar Background
$settings = array(
	'id'			=> 'top-sliding-background',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Top Sliding Bar Background', 'tain' ),
	'description'	=> esc_html__( 'Choose top sliding bar background color.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'header-top-sliding-switch', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Top Sliding Bar Link Color
$settings = array(
	'id'			=> 'top-sliding-link-color',
	'type'			=> 'link',
	'title'			=> esc_html__( 'Top Sliding Bar Link Color', 'tain' ),
	'description'	=> esc_html__( 'Choose top sliding bar link color.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'header-top-sliding-switch', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Top Sliding Bar Border
$settings = array(
	'id'			=> 'top-sliding-border',
	'type'			=> 'border',
	'title'			=> esc_html__( 'Top Sliding Bar Border', 'tain' ),
	'description'	=> esc_html__( 'Here you can set border. No need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'header-top-sliding-switch', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Top Sliding Bar Padding Option
$settings = array(
	'id'			=> 'top-sliding-padding',
	'type'			=> 'dimension',
	'title'			=> esc_html__( 'Top Sliding Bar Padding Option', 'tain' ),
	'description'	=> esc_html__( 'Here no need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'header-top-sliding-switch', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Style End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );