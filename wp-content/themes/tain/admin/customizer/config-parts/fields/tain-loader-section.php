<?php

//Page Loader
$settings = array(
	'id'			=> 'page-loader',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Page Loader', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable page loader', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Page Loader Image
$settings = array(
	'id'			=> 'page-loader-img',
	'type'			=> 'image',
	'title'			=> esc_html__( 'Page Loader Image', 'tain' ),
	'description'	=> esc_html__( 'Upload Page Loader Image', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1,
	'required'		=> array( 'page-loader', '=', 1 )
);
TainCustomizerConfig::buildFields( $settings );

//Infinite Scroll Image
$settings = array(
	'id'			=> 'infinite-loader-img',
	'type'			=> 'image',
	'title'			=> esc_html__( 'Infinite Scroll Image', 'tain' ),
	'description'	=> esc_html__( 'Upload Infinite Scroll Image', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );