<?php

//Search Content
$settings = array(
	'id'			=> 'search-content',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Search Content', 'tain' ),
	'description'	=> esc_html__( 'Choose this option for search content from site.', 'tain' ),
	'choices'		=> array(
		'all'		=> esc_html__( 'All', 'tain' ),
		'post'		=> esc_html__( 'Post Content Only', 'tain' ),
		'page'		=> esc_html__( 'Page Content Only', 'tain' ),
		'post_page'	=> esc_html__( 'Post and Page Content Only', 'tain' )
	),
	'default'		=> 'all',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );