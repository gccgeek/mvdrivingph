<?php

//Theme Color
$settings = array(
	'id'			=> 'theme-color',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Theme Color', 'tain' ),
	'description'	=> esc_html__( 'Choose theme color.', 'tain' ),
	'default'		=> '#e8c020',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Secondary Color
$settings = array(
	'id'			=> 'secondary-color',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Secondary Color', 'tain' ),
	'description'	=> esc_html__( 'Choose secondary color. This option for switch theme to gradient mode. If leave this color to empty, Single color will appear as theme color.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//General Links Color
$settings = array(
	'id'			=> 'theme-link-color',
	'type'			=> 'link',
	'title'			=> esc_html__( 'General Links Color', 'tain' ),
	'description'	=> esc_html__( 'Choose theme link color.', 'tain' ),
	'default'		=> array(
		'regular'	=> '#e8c020',
		'hover'		=> '#fc7223',
		'active'	=> '#fc7223'
	),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//General Button Color
$settings = array(
	'id'			=> 'theme-btn-color',
	'type'			=> 'link',
	'title'			=> esc_html__( 'General Button Color', 'tain' ),
	'description'	=> esc_html__( 'Choose theme button color.', 'tain' ),
	'default'		=> array(
		'regular'	=> '#e8c020',
		'hover'		=> '#fc7223',
		'active'	=> '#fc7223'
	),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

