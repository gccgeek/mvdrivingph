<?php

//Layout Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Layout', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Team Template
$settings = array(
	'id'			=> 'team-page-template',
	'type'			=> 'radioimage',
	'title'			=> esc_html__( 'Team Template', 'tain' ),
	'description'	=> esc_html__( 'Choose your current team single outer template.', 'tain' ),
	'default'		=> 'no-sidebar',
	'items' 		=> array(
		'no-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/1.png',
		'right-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/2.png',
		'left-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/3.png',
		'both-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/4.png'		
	),
	'cols'			=> '4',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Left Sidebar
$settings = array(
	'id'			=> 'team-left-sidebar',
	'type'			=> 'sidebars',
	'title'			=> esc_html__( 'Left Sidebar', 'tain' ),
	'description'	=> esc_html__( 'Select widget area for showing on left side.', 'tain' ),
	'default'		=> 'sidebar-1',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Right Sidebar
$settings = array(
	'id'			=> 'team-right-sidebar',
	'type'			=> 'sidebars',
	'title'			=> esc_html__( 'Right Sidebar', 'tain' ),
	'description'	=> esc_html__( 'Select widget area for showing on right side.', 'tain' ),
	'default'		=> 'sidebar-1',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Custom Post Type Team Layout
$settings = array(
	'id'			=> 'cpt-team-layout',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Team Layout', 'tain' ),
	'description'	=> esc_html__( 'Select single team layout model.', 'tain' ),
	'choices' 		=> array(
			'default' 		=> esc_html__( 'Default', 'tain' ),
			'classic' 		=> esc_html__( 'Classic', 'tain' ),
			'modern' 		=> esc_html__( 'Modern', 'tain' )
	),
	'default'		=> 'default',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Custom Post Type Team Settings
$settings = array(
	'id'			=> 'team-title-opt',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Team Title', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable page title on single team page.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Layout End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Advanced Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Advanced', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Custom Post Type Team Slug
$settings = array(
	'id'			=> 'cpt-team-slug',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Team Slug', 'tain' ),
	'description'	=> esc_html__( 'Enter team slug for register custom post type, after entered new slug click Publish button. Then go to WordPress Settings -> Permalinks -> Click save changes button to regenerate the permalinks.', 'tain' ),
	'default'		=> 'team',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Sidebar on Mobile
$settings = array(
	'id'			=> 'team-page-hide-sidebar',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Sidebar on Mobile', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable to show or hide sidebar on mobile.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Advanced End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );