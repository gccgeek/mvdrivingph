<?php

//Layout Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Layout', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Footer Bottom Inner Layout
$settings = array(
	'id'			=> 'footer-bottom-container',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Footer Bottom Inner Layout', 'tain' ),
	'description'	=> esc_html__( 'Choose footer bottom layout boxed or wide.', 'tain' ),
	'choices'		=> array(
		'boxed'		=> esc_html__( 'Boxed', 'tain' ),
		'wide'		=> esc_html__( 'Wide', 'tain' )
	),
	'default'		=> 'wide',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Bottom Fixed
$settings = array(
	'id'			=> 'footer-bottom-fixed',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Footer Bottom Fixed', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable footer bottom to fixed at bottom of page.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Sidebars
$settings = array(
	'id'			=> 'footer-bottom-widget',
	'type'			=> 'sidebars',
	'title'			=> esc_html__( 'Footer Bottom Widget', 'tain' ),
	'description'	=> esc_html__( 'Select widget area for showing on footer copyright section.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Bottom Items
$settings = array(
	'id'			=> 'footer-bottom-items',
	'type'			=> 'dragdrop',
	'title'			=> esc_html__( 'Footer Bottom Items', 'tain' ),
	'description'	=> esc_html__( 'Needed footer bottom items drag from disabled and put enabled.', 'tain' ),
	'default' 		=> array(
		'disabled' => array(
			'social'	=> esc_html__( 'Footer Social Links', 'tain' ),
			'widget'	=> esc_html__( 'Custom Widget', 'tain' ),
			'menu'		=> esc_html__( 'Footer Menu', 'tain' )
		),
		'Left'  => array(),
		'Center'  => array(
			'copyright' => esc_html__( 'Copyright Text', 'tain' )
		),
		'Right'  => array()
	),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Layout End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Style Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Style', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Footer Bottom Link Color
$settings = array(
	'id'			=> 'footer-bottom-link-color',
	'type'			=> 'link',
	'title'			=> esc_html__( 'Footer Bottom Link Color', 'tain' ),
	'description'	=> esc_html__( 'Choose footer bottomlink color.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Bottom Border
$settings = array(
	'id'			=> 'footer-bottom-border',
	'type'			=> 'border',
	'title'			=> esc_html__( 'Footer Bottom Border', 'tain' ),
	'description'	=> esc_html__( 'Here you can set border. No need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Bottom Padding Option
$settings = array(
	'id'			=> 'footer-bottom-padding',
	'type'			=> 'dimension',
	'title'			=> esc_html__( 'Footer Bottom Padding Option', 'tain' ),
	'description'	=> esc_html__( 'Here no need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Bottom Margin Option
$settings = array(
	'id'			=> 'footer-bottom-margin',
	'type'			=> 'dimension',
	'title'			=> esc_html__( 'Footer Bottom Margin Option', 'tain' ),
	'description'	=> esc_html__( 'Here no need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Bottom Background
$settings = array(
	'id'			=> 'footer-bottom-background',
	'type'			=> 'background',
	'title'			=> esc_html__( 'Footer Bottom Background', 'tain' ),
	'description'	=> esc_html__( 'This is settings for footer background.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Bottom Background Overlay
$settings = array(
	'id'			=> 'footer-bottom-background-overlay',
	'type'			=> 'alpha',
	'title'			=> esc_html__( 'Footer Bottom Background Overlay', 'tain' ),
	'description'	=> esc_html__( 'Choose footer bottom background overlay color.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Widget Title Color
$settings = array(
	'id'			=> 'footer-bottom-title-color',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Widget Title Color', 'tain' ),
	'description'	=> esc_html__( 'Choose footer bottom widgets title color.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Style End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Custom Text Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Custom Text', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Copyright Text
$settings = array(
	'id'			=> 'copyright-text',
	'type'			=> 'textarea',
	'title'			=> esc_html__( 'Copyright Text', 'tain' ),
	'description'	=> esc_html__( 'This is the copyright text. Shown on footer bottom if enable footer bottom in footer items', 'tain' ),
	'default'		=> esc_html( 'Copyright 2022 Theme by zozothemes' ),
	'refresh'		=> 0,
	'instant'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Custom Text End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );