<?php

//Layout Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Layout', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Header Navbar Items
$settings = array(
	'id'			=> 'header-navbar-items',
	'type'			=> 'dragdrop',
	'title'			=> esc_html__( 'Header Navbar Items', 'tain' ),
	'description'	=> esc_html__( 'Needed header navbar items drag from disabled and put enabled.', 'tain' ),
	'default' 		=> array(
		'disabled' => array(					
			'header-navbar-text-1'		=> esc_html__( 'Custom Text 1', 'tain' ),
			'header-navbar-text-2'		=> esc_html__( 'Custom Text 2', 'tain' ),
			'header-navbar-text-3'		=> esc_html__( 'Custom Text 3', 'tain' ),					
			'header-navbar-social'		=> esc_html__( 'Social', 'tain' ),
			'header-navbar-secondary-toggle'	=> esc_html__( 'Secondary Toggle', 'tain' ),					
			'header-navbar-search'		=> esc_html__( 'Search', 'tain' ),
			'header-phone'   			=> esc_html__( 'Phone Number', 'tain' ),
			'header-address'  			=> esc_html__( 'Address Text', 'tain' ),
			'header-email'   			=> esc_html__( 'Email', 'tain' ),
			'header-navbar-search-toggle'	=> esc_html__( 'Search Toggle', 'tain' ),
			'header-cart'   			=> esc_html__( 'Cart', 'tain' )
		),
		'Left'  => array(
			'header-navbar-logo'		=> esc_html__( 'Logo', 'tain' ),
			'header-navbar-sticky-logo'	=> esc_html__( 'Stikcy Logo', 'tain' ),
			'header-navbar-menu'    	=> esc_html__( 'Main Menu', 'tain' )										
		),
		'Center' => array(),
		'Right' => array()
	),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Layout End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Style Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Style', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Header Navbar Height
$settings = array(
	'id'			=> 'header-navbar-height',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Header Navbar Height', 'tain' ),
	'description'	=> esc_html__( 'Increase or decrease header navbar height. Here no need to put dimension units like px, em etc. Example 50', 'tain' ),
	'default'		=> '80',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Navbar Sticky Height
$settings = array(
	'id'			=> 'header-navbar-sticky-height',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Header Navbar Sticky Height', 'tain' ),
	'description'	=> esc_html__( 'Increase or decrease header navbar sticky height. Here no need to put dimension units like px, em etc. Example 50', 'tain' ),
	'default'		=> '80',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Navbar Background
$settings = array(
	'id'			=> 'header-navbar-background',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Header Navbar Background', 'tain' ),
	'description'	=> esc_html__( 'Choose navbar background color.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Navbar Link Color
$settings = array(
	'id'			=> 'header-navbar-link-color',
	'type'			=> 'link',
	'title'			=> esc_html__( 'Header Navbar Link Color', 'tain' ),
	'description'	=> esc_html__( 'Choose navbar link color.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Navbar Border
$settings = array(
	'id'			=> 'header-navbar-border',
	'type'			=> 'border',
	'title'			=> esc_html__( 'Header Navbar Border', 'tain' ),
	'description'	=> esc_html__( 'Here you can set border. No need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Navbar Padding Option
$settings = array(
	'id'			=> 'header-navbar-padding',
	'type'			=> 'dimension',
	'title'			=> esc_html__( 'Header Navbar Padding Option', 'tain' ),
	'description'	=> esc_html__( 'Here no need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Sticky Navbar Font Color
$settings = array(
	'id'			=> 'sticky-header-navbar-color',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Header Sticky Navbar Font Color', 'tain' ),
	'description'	=> esc_html__( 'Set header sticky navbar font color.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Sticky Navbar Background
$settings = array(
	'id'			=> 'sticky-header-navbar-color',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Header Sticky Navbar Background', 'tain' ),
	'description'	=> esc_html__( 'Choose sticky navbar background color.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Sticky Navbar Link Color
$settings = array(
	'id'			=> 'sticky-header-navbar-link-color',
	'type'			=> 'link',
	'title'			=> esc_html__( 'Header Sticky Navbar Link Color', 'tain' ),
	'description'	=> esc_html__( 'Choose sticky navbar link color.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Sticky Navbar Border
$settings = array(
	'id'			=> 'sticky-header-navbar-border',
	'type'			=> 'border',
	'title'			=> esc_html__( 'Header Sticky Navbar Border', 'tain' ),
	'description'	=> esc_html__( 'Here you can set border. No need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Sticky Navbar Padding Option
$settings = array(
	'id'			=> 'sticky-header-navbar-padding',
	'type'			=> 'dimension',
	'title'			=> esc_html__( 'Header Sticky Navbar Padding Option', 'tain' ),
	'description'	=> esc_html__( 'Here no need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Style End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Custom Text Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Custom Text', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Navbar Custom Text 1
$settings = array(
	'id'			=> 'header-navbar-text-1',
	'type'			=> 'textarea',
	'title'			=> esc_html__( 'Navbar Custom Text 1', 'tain' ),
	'description'	=> esc_html__( 'Custom text shows header navbar. Here, you can place shortcode.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Navbar Custom Text 2
$settings = array(
	'id'			=> 'header-navbar-text-2',
	'type'			=> 'textarea',
	'title'			=> esc_html__( 'Navbar Custom Text 2', 'tain' ),
	'description'	=> esc_html__( 'Custom text shows header navbar. Here, you can place shortcode.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Navbar Custom Text 3
$settings = array(
	'id'			=> 'header-navbar-text-3',
	'type'			=> 'textarea',
	'title'			=> esc_html__( 'Navbar Custom Text 3', 'tain' ),
	'description'	=> esc_html__( 'Custom text shows header navbar. Here, you can place shortcode.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Custom Text End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );