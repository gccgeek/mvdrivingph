<?php

//Body Typography
$settings = array(
	'id'			=> 'body-typography',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'Body Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for body general typography', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//H1 Typography
$settings = array(
	'id'			=> 'h1-typography',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'H1 Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for theme H1 typography', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//H2 Typography
$settings = array(
	'id'			=> 'h2-typography',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'H2 Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for theme H2 typography', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//H3 Typography
$settings = array(
	'id'			=> 'h3-typography',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'H3 Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for theme H3 typography', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//H4 Typography
$settings = array(
	'id'			=> 'h4-typography',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'H4 Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for theme H4 typography', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//H5 Typography
$settings = array(
	'id'			=> 'h5-typography',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'H5 Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for theme H5 typography', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//H6 Typography
$settings = array(
	'id'			=> 'h6-typography',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'H6 Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for theme H6 typography', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );