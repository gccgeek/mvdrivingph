<?php

//Widgets Title Typography
$settings = array(
	'id'			=> 'widgets-title',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'Widgets Title Typography', 'tain' ),
	'description'	=> esc_html__( 'Specify the widget title typography properties.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Widgets Content Typography
$settings = array(
	'id'			=> 'widgets-content',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'Widgets Content Typography', 'tain' ),
	'description'	=> esc_html__( 'Specify the widget content typography properties.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );