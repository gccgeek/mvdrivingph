<?php

//Layout Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Layout', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Page Title Option
$settings = array(
	'id'			=> 'blog-page-title-opt',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Page Title Option', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable page title.', 'tain' ),
	'default'		=> 1,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Page Title Items
$settings = array(
	'id'			=> 'template-blog-pagetitle-items',
	'type'			=> 'dragdrop',
	'title'			=> esc_html__( 'Page Title Items', 'tain' ),
	'description'	=> esc_html__( 'Needed items for page title wrap, drag from disabled and put enabled.', 'tain' ),
	'default' 		=> array(
		'disabled' => array(
			'description' => esc_html__( 'Page Title Description', 'tain' )
		),
		'Left'  => array(
			'title' => esc_html__( 'Page Title Text', 'tain' ),
		),
		'Center' => array(),
		'Right'  => array(
			'breadcrumb'	=> esc_html__( 'Breadcrumb', 'tain' )
		)
	),
	'required'		=> array( 'blog-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Settings
$settings = array(
	'type'			=> 'section',
	'label'			=> esc_html__( 'Blog Settings', 'tain' ),
	'description'	=> esc_html__( 'This is settings for blog page layout, sidebar sticky and etc.', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Blog Template
$settings = array(
	'id'			=> 'blog-page-template',
	'type'			=> 'radioimage',
	'title'			=> esc_html__( 'Blog Template', 'tain' ),
	'description'	=> esc_html__( 'Choose your current blog page template.', 'tain' ),
	'default'		=> 'right-sidebar',
	'items' 		=> array(
		'no-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/1.png',
		'right-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/2.png',
		'left-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/3.png',
		'both-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/4.png'		
	),
	'cols'			=> '4',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Left Sidebar
$settings = array(
	'id'			=> 'blog-left-sidebar',
	'type'			=> 'sidebars',
	'title'			=> esc_html__( 'Left Sidebar', 'tain' ),
	'description'	=> esc_html__( 'Select widget area for showing on left side.', 'tain' ),
	'default'		=> 'sidebar-1',
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Right Sidebar
$settings = array(
	'id'			=> 'blog-right-sidebar',
	'type'			=> 'sidebars',
	'title'			=> esc_html__( 'Right Sidebar', 'tain' ),
	'description'	=> esc_html__( 'Select widget area for showing on right side.', 'tain' ),
	'default'		=> 'sidebar-1',
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Blog Post Template
$settings = array(
	'id'			=> 'blog-post-template',
	'type'			=> 'radioimage',
	'title'			=> esc_html__( 'Blog Post Template', 'tain' ),
	'description'	=> esc_html__( 'Choose your current blog post template.', 'tain' ),
	'default'		=> 'standard',
	'items' 		=> array(
		'standard'	=> TAIN_ADMIN_URL . '/customizer/assets/images/post-layouts/1.png',
		'grid'		=> TAIN_ADMIN_URL . '/customizer/assets/images/post-layouts/2.png',
		'list'		=> TAIN_ADMIN_URL . '/customizer/assets/images/post-layouts/3.png'		
	),
	'cols'			=> '3',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Top Standard Post
$settings = array(
	'id'			=> 'blog-top-standard-post',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Top Standard Post', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable to show top post layout standard others selected layout.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Grid Columns
$settings = array(
	'id'			=> 'blog-grid-cols',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Grid Columns', 'tain' ),
	'description'	=> esc_html__( 'Select grid columns.', 'tain' ),
	'choices'		=> array(
		'4'		=> esc_html__( '4 Columns', 'tain' ),
		'3'		=> esc_html__( '3 Columns', 'tain' ),
		'2'		=> esc_html__( '2 Columns', 'tain' ),
	),
	'default'		=> '2',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Post Grid Gutter
$settings = array(
	'id'			=> 'blog-grid-gutter',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Blog Post Grid Gutter', 'tain' ),
	'description'	=> esc_html__( 'Enter grid gutter size. Example 20', 'tain' ),
	'default'		=> '30',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Grid Type
$settings = array(
	'id'			=> 'blog-grid-type',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Grid Type', 'tain' ),
	'description'	=> esc_html__( 'Select grid type normal or isotope.', 'tain' ),
	'choices'		=> array(
		'normal'		=> esc_html__( 'Normal Grid', 'tain' ),
		'isotope'		=> esc_html__( 'Isotope Grid', 'tain' ),
	),
	'default'		=> 'normal',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Infinite Scroll
$settings = array(
	'id'			=> 'blog-infinite-scroll',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Infinite Scroll', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable infinite scroll for blog post.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Post Excerpt Length
$settings = array(
	'id'			=> 'blog-excerpt',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Post Excerpt Length', 'tain' ),
	'description'	=> esc_html__( 'Set blog post excerpt length. Example 15', 'tain' ),
	'default'		=> '15',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Post Top Meta Items
$settings = array(
	'id'			=> 'blog-topmeta-items',
	'type'			=> 'dragdrop',
	'title'			=> esc_html__( 'Blog Post Top Meta Items', 'tain' ),
	'description'	=> esc_html__( 'Needed blog post top meta items drag from disabled and put enabled part. ie: Left or Right.', 'tain' ),
	'default' 		=> array(
		'Left'  => array(
			'date'		=> esc_html__( 'Date', 'tain' )
		),
		'Right'  => array(
			'category'	=> esc_html__( 'Category', 'tain' )
		),
		'disabled' => array(
			'social'	=> esc_html__( 'Social Share', 'tain' ),
			'comments'	=> esc_html__( 'Comments', 'tain' ),
			'likes'		=> esc_html__( 'Likes', 'tain' ),
			'author'	=> esc_html__( 'Author', 'tain' ),
			'views'		=> esc_html__( 'Views', 'tain' ),
			'more'		=> esc_html__( 'Read More', 'tain' ),
			'favourite'	=> esc_html__( 'Favourite', 'tain' )
		)
	),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Post Bottom Meta Items
$settings = array(
	'id'			=> 'blog-bottommeta-items',
	'type'			=> 'dragdrop',
	'title'			=> esc_html__( 'Blog Post Bottom Meta Items', 'tain' ),
	'description'	=> esc_html__( 'Needed blog post bottom meta items drag from disabled and put enabled part. ie: Left or Right.', 'tain' ),
	'default' 		=> array(
		'Left'  => array(
			'more'		=> esc_html__( 'Read More', 'tain' ),
		),
		'Right'  => array(),
		'disabled' => array(
			'comments'	=> esc_html__( 'Comments', 'tain' ),
			'category'	=> esc_html__( 'Category', 'tain' ),
			'social'	=> esc_html__( 'Social Share', 'tain' ),
			'comments'	=> esc_html__( 'Comments', 'tain' ),
			'likes'		=> esc_html__( 'Likes', 'tain' ),
			'author'	=> esc_html__( 'Author', 'tain' ),
			'views'		=> esc_html__( 'Views', 'tain' ),
			'date'		=> esc_html__( 'Date', 'tain' ),
			'favourite'	=> esc_html__( 'Favourite', 'tain' )
		)
	),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Post Items
$settings = array(
	'id'			=> 'blog-items',
	'type'			=> 'dragdrop',
	'title'			=> esc_html__( 'Blog Post Items', 'tain' ),
	'description'	=> esc_html__( 'Needed blog post items drag from disabled and put enabled part. Thumbnail part covers the post format either image/audio/video/gallery/quote/link.', 'tain' ),
	'default' 		=> array(
		'Enabled'  		=> array(
			'title'			=> esc_html__( 'Title', 'tain' ),
			'top-meta'		=> esc_html__( 'Top Meta', 'tain' ),
			'thumb'			=> esc_html__( 'Thumbnail', 'tain' ),
			'content'		=> esc_html__( 'Content', 'tain' ),
			'bottom-meta'	=> esc_html__( 'Bottom Meta', 'tain' )
		),
		'disabled' => array()
	),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Post Overlay
$settings = array(
	'id'			=> 'blog-overlay-opt',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Blog Post Overlay', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable blog post overlay.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Post Overlay Items
$settings = array(
	'id'			=> 'blog-overlay-items',
	'type'			=> 'dragdrop',
	'title'			=> esc_html__( 'Blog Post Overlay Items', 'tain' ),
	'description'	=> esc_html__( 'Needed blog post overlay items drag from disabled and put enabled part.', 'tain' ),
	'default' 		=> array(
		'Enabled'  	=> array(
			'title'			=> esc_html__( 'Title', 'tain' ),
		),
		'disabled' => array(
			'top-meta'		=> esc_html__( 'Top Meta', 'tain' ),
			'bottom-meta'	=> esc_html__( 'Bottom Meta', 'tain' )
		)
	),
	'required'		=> array( 'blog-overlay-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Settings End
$settings = array(
	'type'			=> 'section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Layout End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Style Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Style', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Page Title Settings
$settings = array(
	'type'			=> 'section',
	'label'			=> esc_html__( 'Page Title Settings', 'tain' ),
	'description'	=> esc_html__( 'This is page title style settings shows only when page title option active.', 'tain' ),
	'section_stat'	=> true,
);
TainCustomizerConfig::buildFields( $settings );

//Font Color
$settings = array(
	'id'			=> 'template-blog-color',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Font Color', 'tain' ),
	'description'	=> esc_html__( 'This is font color for current field.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'blog-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Template Link Color
$settings = array(
	'id'			=> 'template-blog-link-color',
	'type'			=> 'link',
	'title'			=> esc_html__( 'Blog Template Link Color', 'tain' ),
	'description'	=> esc_html__( 'Choose Blog template color.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'blog-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Template Border
$settings = array(
	'id'			=> 'template-blog-border',
	'type'			=> 'border',
	'title'			=> esc_html__( 'Blog Template Border', 'tain' ),
	'description'	=> esc_html__( 'Here you can set border. No need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'blog-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Template Padding Option
$settings = array(
	'id'			=> 'template-blog-padding',
	'type'			=> 'dimension',
	'title'			=> esc_html__( 'Blog Template Padding Option', 'tain' ),
	'description'	=> esc_html__( 'Here no need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'blog-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Template Background
$settings = array(
	'id'			=> 'template-blog-background-all',
	'type'			=> 'background',
	'title'			=> esc_html__( 'Blog Template Background', 'tain' ),
	'description'	=> esc_html__( 'This is settings for footer background.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'blog-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Background Parallax
$settings = array(
	'id'			=> 'blog-page-title-parallax',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Background Parallax', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable page title background parallax.', 'tain' ),
	'default'		=> 0,
	'required'		=> array( 'blog-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Background Video
$settings = array(
	'id'			=> 'blog-page-title-bg',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Background Video', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable page title background video.', 'tain' ),
	'default'		=> 0,
	'required'		=> array( 'blog-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Page Title Background Video
$settings = array(
	'id'			=> 'blog-page-title-video',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Page Title Background Video', 'tain' ),
	'description'	=> esc_html__( 'Set page title background video for page. Only allowed youtube video id. Example: UWF7dZTLW4c', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'blog-page-title-bg', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Page Title Overlay
$settings = array(
	'id'			=> 'blog-page-title-overlay',
	'type'			=> 'alpha',
	'title'			=> esc_html__( 'Page Title Overlay', 'tain' ),
	'description'	=> esc_html__( 'Choose page title overlay rgba color.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'blog-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Page Title Settings End
$settings = array(
	'type'			=> 'section',
	'section_stat'	=> false,
	'required'		=> array( 'blog-page-title-opt', '=', 1 )
);
TainCustomizerConfig::buildFields( $settings );

//Blog Article Skin Settings
$settings = array(
	'type'			=> 'section',
	'label'			=> esc_html__( 'Blog Article Skin Settings', 'tain' ),
	'description'	=> esc_html__( 'This is skin settings for each blog article.', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Article Font Color
$settings = array(
	'id'			=> 'blog-article-color',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Article Font Color', 'tain' ),
	'description'	=> esc_html__( 'This is font color for blog article.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Article Link Color
$settings = array(
	'id'			=> 'blog-article-link-color',
	'type'			=> 'link',
	'title'			=> esc_html__( 'Article Link Color', 'tain' ),
	'description'	=> esc_html__( 'Choose Article Link color for blog article.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Article Border
$settings = array(
	'id'			=> 'blog-article-border',
	'type'			=> 'border',
	'title'			=> esc_html__( 'Article Border', 'tain' ),
	'description'	=> esc_html__( 'Here you can set border. No need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Article Padding Option
$settings = array(
	'id'			=> 'blog-article-padding',
	'type'			=> 'dimension',
	'title'			=> esc_html__( 'Article Padding Option', 'tain' ),
	'description'	=> esc_html__( 'Here no need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Article Background Color
$settings = array(
	'id'			=> 'blog-article-background',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Article Background Color', 'tain' ),
	'description'	=> esc_html__( 'This is background color for blog article.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Article Skin Settings End
$settings = array(
	'type'			=> 'section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Style End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Custom Text Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Custom Text', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Post Read More Text
$settings = array(
	'id'			=> 'blog-more-text',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Post Read More Text', 'tain' ),
	'description'	=> esc_html__( 'Set blog post read more text. Example Read More', 'tain' ),
	'default'		=> esc_html__( 'Read More', 'tain' ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Page Title
$settings = array(
	'id'			=> 'blog-page-title',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Blog Page Title', 'tain' ),
	'description'	=> esc_html__( 'This is a title for blog page. HTML code allowed here.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1,
	//'instant'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Page Description
$settings = array(
	'id'			=> 'blog-page-desc',
	'type'			=> 'textarea',
	'title'			=> esc_html__( 'Blog Page Description', 'tain' ),
	'description'	=> esc_html__( 'This is description for blog page. HTML code allowed here.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 0,
	'instant'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Custom Text End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Advanced Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Advanced', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Post Format Settings
$settings = array(
	'type'			=> 'section',
	'label'			=> esc_html__( 'Post Format Settings', 'tain' ),
	'description'	=> esc_html__( 'This is post format settings for blog.', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Video Format
$settings = array(
	'id'			=> 'blog-video-format',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Video Format', 'tain' ),
	'description'	=> esc_html__( 'Choose blog page video post format settings.', 'tain' ),
	'choices'		=> array(
		'onclick' 	=> esc_html__( 'On Click Run Video', 'tain' ),
		'overlay' 	=> esc_html__( 'Modal Box Video', 'tain' ),
		'direct' 	=> esc_html__( 'Direct Video', 'tain' )
	),
	'default'		=> 'onclick',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Quote Format
$settings = array(
	'id'			=> 'blog-quote-format',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Quote Format', 'tain' ),
	'description'	=> esc_html__( 'Choose blog page quote post format settings.', 'tain' ),
	'choices'		=> array(
		'featured' 		=> esc_html__( 'Dark Overlay', 'tain' ),
		'theme-overlay' => esc_html__( 'Theme Overlay', 'tain' ),
		'theme' 		=> esc_html__( 'Theme Color Background', 'tain' ),
		'none' 			=> esc_html__( 'None', 'tain' )
	),
	'default'		=> 'featured',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Link Format
$settings = array(
	'id'			=> 'blog-link-format',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Link Format', 'tain' ),
	'description'	=> esc_html__( 'Choose blog page link post format settings.', 'tain' ),
	'choices'		=> array(
		'featured' 		=> esc_html__( 'Dark Overlay', 'tain' ),
		'theme-overlay' => esc_html__( 'Theme Overlay', 'tain' ),
		'theme' 		=> esc_html__( 'Theme Color Background', 'tain' ),
		'none' 			=> esc_html__( 'None', 'tain' )
	),
	'default'		=> 'featured',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Gallery Format
$settings = array(
	'id'			=> 'blog-gallery-format',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Gallery Format', 'tain' ),
	'description'	=> esc_html__( 'Choose blog page gallery post format settings.', 'tain' ),
	'choices'		=> array(
		'default'	=> esc_html__( 'Default Gallery', 'tain' ),
		'popup' 	=> esc_html__( 'Popup Gallery', 'tain' ),
		'grid' 		=> esc_html__( 'Grid Popup Gallery', 'tain' )
	),
	'default'		=> 'default',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Post Format Settings End
$settings = array(
	'type'			=> 'section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Blog Featured Slider
$settings = array(
	'id'			=> 'blog-featured-slider',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Blog Featured Slider', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable blog featured slider.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Sidebar Sticky
$settings = array(
	'id'			=> 'blog-sidebar-sticky',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Sidebar Sticky', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable sidebar sticky.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Sidebar on Mobile
$settings = array(
	'id'			=> 'blog-page-hide-sidebar',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Sidebar on Mobile', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable to show or hide sidebar on mobile.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Advanced End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );