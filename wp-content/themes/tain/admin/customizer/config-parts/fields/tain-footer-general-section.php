<?php

//Layout Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Layout', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Footer Layout
$settings = array(
	'id'			=> 'footer-layout',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Footer Layout', 'tain' ),
	'description'	=> esc_html__( 'Choose footer layout boxed or wide.', 'tain' ),
	'choices'		=> array(
		'boxed'		=> esc_html__( 'Boxed', 'tain' ),
		'wide'		=> esc_html__( 'Wide', 'tain' )
	),
	'default'		=> 'wide',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Template
$settings = array(
	'id'			=> 'footer-template',
	'type'			=> 'radioimage',
	'title'			=> esc_html__( 'Footer Template', 'tain' ),
	'description'	=> esc_html__( 'Choose footer template. Current footer layout needed option values you can see after refresh this customizer page.', 'tain' ),
	'default'		=> '1',
	'items' 		=> array(
		'1'		=> TAIN_ADMIN_URL . '/customizer/assets/images/footer-layouts/1.jpg',
		'2'		=> TAIN_ADMIN_URL . '/customizer/assets/images/footer-layouts/2.jpg',
		'3'		=> TAIN_ADMIN_URL . '/customizer/assets/images/footer-layouts/3.jpg',
		'4'		=> TAIN_ADMIN_URL . '/customizer/assets/images/footer-layouts/4.jpg',
		'5'		=> TAIN_ADMIN_URL . '/customizer/assets/images/footer-layouts/5.jpg',
		'6'		=> TAIN_ADMIN_URL . '/customizer/assets/images/footer-layouts/6.jpg',
		'7'		=> TAIN_ADMIN_URL . '/customizer/assets/images/footer-layouts/7.jpg',
		'8'		=> TAIN_ADMIN_URL . '/customizer/assets/images/footer-layouts/8.jpg',
		'custom'=> TAIN_ADMIN_URL . '/customizer/assets/images/footer-layouts/custom.jpg'
	),
	'cols'			=> '1',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Items
$settings = array(
	'id'			=> 'footer-items',
	'type'			=> 'dragdrop',
	'title'			=> esc_html__( 'Footer Items', 'tain' ),
	'description'	=> esc_html__( 'Needed footer items drag from disabled and put enabled.', 'tain' ),
	'default' 		=> array(
		'Enabled'  => array(
			'footer-middle'	=> esc_html__( 'Footer Middle', 'tain' ),
			'footer-bottom'	=> esc_html__( 'Footer Bottom', 'tain' )
		),
		'disabled' => array(
			'footer-top' 	=> esc_html__( 'Footer Top', 'tain' )
		)
	),
	'required'		=> array( 'footer-template', '=', 'custom' ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Layout End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Style Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Style', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Back to Top
$settings = array(
	'id'			=> 'back-to-top',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Back to Top', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable back to top icon.', 'tain' ),
	'default'		=> 1,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Back to Top Button Position
$settings = array(
	'id'			=> 'back-to-top-position',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Back to Top Button Position', 'tain' ),
	'description'	=> esc_html__( 'Choose position right/left for back to top button.', 'tain' ),
	'choices'		=> array(
		'right'		=> esc_html__( 'Right', 'tain' ),
		'left'		=> esc_html__( 'Left', 'tain' )
	),
	'default'		=> 'right',
	'required'		=> array( 'back-to-top', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Hidden Footer
$settings = array(
	'id'			=> 'hidden-footer',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Hidden Footer', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable hidden footer.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Link Color
$settings = array(
	'id'			=> 'footer-link-color',
	'type'			=> 'link',
	'title'			=> esc_html__( 'Footer Link Color', 'tain' ),
	'description'	=> esc_html__( 'Choose footer general link color.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Border
$settings = array(
	'id'			=> 'footer-border',
	'type'			=> 'border',
	'title'			=> esc_html__( 'Footer Border', 'tain' ),
	'description'	=> esc_html__( 'Here you can set border. No need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Padding Option
$settings = array(
	'id'			=> 'footer-padding',
	'type'			=> 'dimension',
	'title'			=> esc_html__( 'Footer Padding Option', 'tain' ),
	'description'	=> esc_html__( 'Here no need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Background
$settings = array(
	'id'			=> 'footer-background',
	'type'			=> 'background',
	'title'			=> esc_html__( 'Footer Background', 'tain' ),
	'description'	=> esc_html__( 'This is settings for footer background.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Background Overlay
$settings = array(
	'id'			=> 'footer-background-overlay',
	'type'			=> 'alpha',
	'title'			=> esc_html__( 'Footer Background Overlay', 'tain' ),
	'description'	=> esc_html__( 'Choose footer background overlay color and opacity.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Style End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );