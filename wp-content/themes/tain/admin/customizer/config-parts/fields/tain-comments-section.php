<?php

//Comments Type
$settings = array(
	'id'			=> 'comments-type',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Comments Type', 'tain' ),
	'description'	=> esc_html__( 'This option will be showing comment like facebook or default wordpress.', 'tain' ),
	'choices'		=> array(
		'wp' 	=> esc_html__( 'WordPress Comment', 'tain' ),
		'fb'  	=> esc_html__( 'Facebook Comment', 'tain' )
	),
	'default'		=> 'wp',
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Comments Like
$settings = array(
	'id'			=> 'comments-like',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Comments Like', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable to show or hide comments likes to single post comments.', 'tain' ),
	'default'		=> 0,
	'required'		=> array( 'comments-type', '=', 'wp' ),
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Comments Share
$settings = array(
	'id'			=> 'comments-share',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Comments Share', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable to show or hide comments share to single post comments.', 'tain' ),
	'default'		=> 0,
	'required'		=> array( 'comments-type', '=', 'wp' ),
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Facebook Developer API
$settings = array(
	'id'			=> 'fb-developer-key',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Facebook Developer API', 'tain' ),
	'description'	=> esc_html__( 'Enter facebook developer API key.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'comments-type', '=', 'fb' ),
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Number of Comments
$settings = array(
	'id'			=> 'fb-comments-number',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Number of Comments', 'tain' ),
	'description'	=> esc_html__( 'Enter number of comments to display.', 'tain' ),
	'default'		=> '5',
	'required'		=> array( 'comments-type', '=', 'fb' ),
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );