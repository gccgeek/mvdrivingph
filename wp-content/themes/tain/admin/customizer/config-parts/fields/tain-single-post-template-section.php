<?php

//Layout Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Layout', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Page Title Option
$settings = array(
	'id'			=> 'single-post-page-title-opt',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Page Title Option', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable page title.', 'tain' ),
	'default'		=> 1,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Page Title Items
$settings = array(
	'id'			=> 'template-single-post-pagetitle-items',
	'type'			=> 'dragdrop',
	'title'			=> esc_html__( 'Page Title Items', 'tain' ),
	'description'	=> esc_html__( 'Needed items for page title wrap, drag from disabled and put enabled.', 'tain' ),
	'default' 		=> array(
		'disabled' => array(),
		'Left'  => array(
			'title' => esc_html__( 'Page Title Text', 'tain' ),
		),
		'Center' => array(),
		'Right'  => array(
			'breadcrumb'	=> esc_html__( 'Breadcrumb', 'tain' )
		)
	),
	'required'		=> array( 'single-post-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Archive Settings
$settings = array(
	'type'			=> 'section',
	'label'			=> esc_html__( 'Archive Settings', 'tain' ),
	'description'	=> esc_html__( 'This is settings for single post page layout, sidebar sticky and etc.', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Archive Template
$settings = array(
	'id'			=> 'single-post-page-template',
	'type'			=> 'radioimage',
	'title'			=> esc_html__( 'Archive Template', 'tain' ),
	'description'	=> esc_html__( 'Choose your current single post page template.', 'tain' ),
	'default'		=> 'right-sidebar',
	'items' 		=> array(
		'no-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/1.png',
		'right-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/2.png',
		'left-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/3.png',
		'both-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/4.png'		
	),
	'cols'			=> '4',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Left Sidebar
$settings = array(
	'id'			=> 'single-post-left-sidebar',
	'type'			=> 'sidebars',
	'title'			=> esc_html__( 'Left Sidebar', 'tain' ),
	'description'	=> esc_html__( 'Select widget area for showing on left side.', 'tain' ),
	'default'		=> 'sidebar-1',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Right Sidebar
$settings = array(
	'id'			=> 'single-post-right-sidebar',
	'type'			=> 'sidebars',
	'title'			=> esc_html__( 'Right Sidebar', 'tain' ),
	'description'	=> esc_html__( 'Select widget area for showing on right side.', 'tain' ),
	'default'		=> 'sidebar-1',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Full Width Wrap
$settings = array(
	'id'			=> 'single-post-full-wrap',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Full Width Wrap', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable to show or hide full width post wrapper.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Article Top Meta Items
$settings = array(
	'id'			=> 'single-post-topmeta-items',
	'type'			=> 'dragdrop',
	'title'			=> esc_html__( 'Single Post Article Top Meta Items', 'tain' ),
	'description'	=> esc_html__( 'Needed single post article top meta items drag from disabled and put enabled part. ie: Left or Right.', 'tain' ),
	'default' 		=> array(
		'Left'  => array(
			'author'	=> esc_html__( 'Author', 'tain' )						
		),
		'Right'  => array(
			'date'		=> esc_html__( 'Date', 'tain' )
		),
		'disabled' => array(
			'social'	=> esc_html__( 'Social Share', 'tain' ),						
			'likes'		=> esc_html__( 'Likes', 'tain' ),
			'author'	=> esc_html__( 'Author', 'tain' ),
			'views'		=> esc_html__( 'Views', 'tain' ),
			'tag'		=> esc_html__( 'Tags', 'tain' ),
			'favourite'	=> esc_html__( 'Favourite', 'tain' ),						
			'comments'	=> esc_html__( 'Comments', 'tain' ),
			'category'	=> esc_html__( 'Category', 'tain' )
		)
	),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Article Bottom Meta Items
$settings = array(
	'id'			=> 'single-post-bottommeta-items',
	'type'			=> 'dragdrop',
	'title'			=> esc_html__( 'Single Post Article Bottom Meta Items', 'tain' ),
	'description'	=> esc_html__( 'Needed single post article bottom meta items drag from disabled and put enabled part. ie: Left or Right.', 'tain' ),
	'default' 		=> array(
		'Left'  => array(
			'category'	=> esc_html__( 'Category', 'tain' ),
		),
		'Right'  => array(),
		'disabled' => array(
			'social'	=> esc_html__( 'Social Share', 'tain' ),
			'date'		=> esc_html__( 'Date', 'tain' ),						
			'social'	=> esc_html__( 'Social Share', 'tain' ),						
			'likes'		=> esc_html__( 'Likes', 'tain' ),
			'author'	=> esc_html__( 'Author', 'tain' ),
			'views'		=> esc_html__( 'Views', 'tain' ),
			'favourite'	=> esc_html__( 'Favourite', 'tain' ),
			'comments'	=> esc_html__( 'Comments', 'tain' ),
			'tag'		=> esc_html__( 'Tags', 'tain' )
		)
	),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Article Items
$settings = array(
	'id'			=> 'single-post-items',
	'type'			=> 'dragdrop',
	'title'			=> esc_html__( 'Single Post Article Items', 'tain' ),
	'description'	=> esc_html__( 'Needed single post article items drag from disabled and put enabled part. Thumbnail part covers the post format either image/audio/video/gallery/quote/link.', 'tain' ),
	'default' 		=> array(
		'Enabled'  => array(
			'title'	=> esc_html__( 'Title', 'tain' ),
			'top-meta'	=> esc_html__( 'Top Meta', 'tain' ),
			'thumb'	=> esc_html__( 'Thumbnail', 'tain' ),
			'content'	=> esc_html__( 'Content', 'tain' ),
			'bottom-meta'	=> esc_html__( 'Bottom Meta', 'tain' ),
		),
		'disabled' => array()
	),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Article Overlay
$settings = array(
	'id'			=> 'single-post-overlay-opt',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Single Post Article Overlay', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable single post article overlay.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Article Overlay Items
$settings = array(
	'id'			=> 'single-post-overlay-items',
	'type'			=> 'dragdrop',
	'title'			=> esc_html__( 'Single Post Article Overlay Items', 'tain' ),
	'description'	=> esc_html__( 'Needed single post article overlay items drag from disabled and put enabled part.', 'tain' ),
	'default' 		=> array(
		'Enabled'  => array(
			'title'			=> esc_html__( 'Title', 'tain' ),
		),
		'disabled' => array(
			'top-meta'		=> esc_html__( 'Top Meta', 'tain' ),
			'bottom-meta'	=> esc_html__( 'Bottom Meta', 'tain' )
		)
	),
	'required'		=> array( 'single-post-overlay-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Page Items
$settings = array(
	'id'			=> 'single-post-page-items',
	'type'			=> 'dragdrop',
	'title'			=> esc_html__( 'Single Post Page Items', 'tain' ),
	'description'	=> esc_html__( 'Needed single post items drag from disabled and put enabled part.', 'tain' ),
	'default' 		=> array(
		'Enabled'  => array(
			'post-items'	=> esc_html__( 'Post Items', 'tain' ),
			'author-info'	=> esc_html__( 'Author Info', 'tain' ),
			'post-nav'		=> esc_html__( 'Post Navigation', 'tain' ),
			'related-slider'=> esc_html__( 'Related Slider', 'tain' ),
			'comment'		=> esc_html__( 'Comment', 'tain' )
		),
		'disabled' => array()
	),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Settings End
$settings = array(
	'type'			=> 'section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Layout End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Style Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Style', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Page Title Settings
$settings = array(
	'type'			=> 'section',
	'label'			=> esc_html__( 'Page Title Settings', 'tain' ),
	'description'	=> esc_html__( 'This is page title style settings shows only when page title option active.', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Font Color
$settings = array(
	'id'			=> 'template-single-post-color',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Font Color', 'tain' ),
	'description'	=> esc_html__( 'This is font color for current field.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'single-post-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Template  Link Color
$settings = array(
	'id'			=> 'template-single-post-link-color',
	'type'			=> 'link',
	'title'			=> esc_html__( 'Single Post Template  Link Color', 'tain' ),
	'description'	=> esc_html__( 'Choose Single post title bar link color.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'single-post-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Template  Border
$settings = array(
	'id'			=> 'template-single-post-border',
	'type'			=> 'border',
	'title'			=> esc_html__( 'Single Post Template  Border', 'tain' ),
	'description'	=> esc_html__( 'Here you can set border. No need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'single-post-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Template  Padding Option
$settings = array(
	'id'			=> 'template-single-post-padding',
	'type'			=> 'dimension',
	'title'			=> esc_html__( 'Single Post Template  Padding Option', 'tain' ),
	'description'	=> esc_html__( 'Here no need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'single-post-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Template  Background
$settings = array(
	'id'			=> 'template-single-post-background-all',
	'type'			=> 'background',
	'title'			=> esc_html__( 'Single Post Template  Background', 'tain' ),
	'description'	=> esc_html__( 'This is settings for footer background.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'single-post-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Background Parallax
$settings = array(
	'id'			=> 'single-post-page-title-parallax',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Background Parallax', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable page title background parallax.', 'tain' ),
	'default'		=> 0,
	'required'		=> array( 'single-post-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Background Video
$settings = array(
	'id'			=> 'single-post-page-title-bg',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Background Video', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable page title background video.', 'tain' ),
	'default'		=> 0,
	'required'		=> array( 'single-post-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Page Title Background Video
$settings = array(
	'id'			=> 'single-post-page-title-video',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Page Title Background Video', 'tain' ),
	'description'	=> esc_html__( 'Set page title background video for page. Only allowed youtube video id. Example: UWF7dZTLW4c', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'single-post-page-title-bg', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Page Title Overlay
$settings = array(
	'id'			=> 'single-post-page-title-overlay',
	'type'			=> 'alpha',
	'title'			=> esc_html__( 'Page Title Overlay', 'tain' ),
	'description'	=> esc_html__( 'Choose page title overlay rgba color.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'single-post-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Page Title Items Option
$settings = array(
	'id'			=> 'template-single-post-page-title-items-opt',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Page Title Items Option', 'tain' ),
	'description'	=> esc_html__( 'Enable to make page title items custom layout.', 'tain' ),
	'default'		=> 0,
	'required'		=> array( 'single-post-page-title-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Page Title Settings End
$settings = array(
	'type'			=> 'section',
	'section_stat'	=> false,
	'required'		=> array( 'single-post-page-title-opt', '=', 1 )
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Article Skin Settings
$settings = array(
	'type'			=> 'section',
	'label'			=> esc_html__( 'Single Post Article Skin Settings', 'tain' ),
	'description'	=> esc_html__( 'This is skin settings for each single post article.', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Article Font Color
$settings = array(
	'id'			=> 'single-post-article-color',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Article Font Color', 'tain' ),
	'description'	=> esc_html__( 'This is font color for single post article.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Article Link Color
$settings = array(
	'id'			=> 'single-post-article-link-color',
	'type'			=> 'link',
	'title'			=> esc_html__( 'Article Link Color', 'tain' ),
	'description'	=> esc_html__( 'Choose single post article link color for single post article.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Article Border
$settings = array(
	'id'			=> 'single-post-article-border',
	'type'			=> 'border',
	'title'			=> esc_html__( 'Article Border', 'tain' ),
	'description'	=> esc_html__( 'Here you can set border. No need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Article Padding Option
$settings = array(
	'id'			=> 'single-post-article-padding',
	'type'			=> 'dimension',
	'title'			=> esc_html__( 'Article Padding Option', 'tain' ),
	'description'	=> esc_html__( 'Here no need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Article Background
$settings = array(
	'id'			=> 'single-post-article-background',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Article Background Color', 'tain' ),
	'description'	=> esc_html__( 'This is background color for single post article.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Article Skin Settings End
$settings = array(
	'type'			=> 'section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Article Overlay Skin Settings
$settings = array(
	'type'			=> 'section',
	'label'			=> esc_html__( 'Single Post Article Overlay Skin Settings', 'tain' ),
	'description'	=> esc_html__( 'This is overlay skin settings for each single post article.', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Article Font Color
$settings = array(
	'id'			=> 'single-post-article-overlay-color',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Article Font Color', 'tain' ),
	'description'	=> esc_html__( 'This is font color for single post article.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Article Link Color
$settings = array(
	'id'			=> 'single-post-article-overlay-link-color',
	'type'			=> 'link',
	'title'			=> esc_html__( 'Article Link Color', 'tain' ),
	'description'	=> esc_html__( 'Choose single post article overlay link color for single post article.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Article Border
$settings = array(
	'id'			=> 'single-post-article-overlay-border',
	'type'			=> 'border',
	'title'			=> esc_html__( 'Article Border', 'tain' ),
	'description'	=> esc_html__( 'Here you can set border. No need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Article Padding Option
$settings = array(
	'id'			=> 'single-post-article-overlay-padding',
	'type'			=> 'dimension',
	'title'			=> esc_html__( 'Article Padding Option', 'tain' ),
	'description'	=> esc_html__( 'Here no need to put dimension units like px, em etc. Example 10 10 20 10.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Article Background
$settings = array(
	'id'			=> 'single-post-article-overlay-background',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Article Background Color', 'tain' ),
	'description'	=> esc_html__( 'This is background color for single post article.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Article Overlay Skin Settings End
$settings = array(
	'type'			=> 'section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Style End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Advanced Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Advanced', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Post Format Settings Start
$settings = array(
	'type'			=> 'section',
	'label'			=> esc_html__( 'Post Format Settings', 'tain' ),
	'description'	=> esc_html__( 'This is post format settings for single post.', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Video Format
$settings = array(
	'id'			=> 'single-post-video-format',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Video Format', 'tain' ),
	'description'	=> esc_html__( 'Choose single post page video post format settings.', 'tain' ),
	'choices'		=> array(
		'onclick' 	=> esc_html__( 'On Click Run Video', 'tain' ),
		'overlay' 	=> esc_html__( 'Modal Box Video', 'tain' ),
		'direct' 	=> esc_html__( 'Direct Video', 'tain' )
	),
	'default'		=> 'onclick',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Quote Format
$settings = array(
	'id'			=> 'single-post-quote-format',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Quote Format', 'tain' ),
	'description'	=> esc_html__( 'Choose single post page quote post format settings.', 'tain' ),
	'choices'		=> array(
		'featured' 		=> esc_html__( 'Dark Overlay', 'tain' ),
		'theme-overlay' => esc_html__( 'Theme Overlay', 'tain' ),
		'theme' 		=> esc_html__( 'Theme Color Background', 'tain' ),
		'none' 			=> esc_html__( 'None', 'tain' )
	),
	'default'		=> 'featured',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Link Format
$settings = array(
	'id'			=> 'single-post-link-format',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Link Format', 'tain' ),
	'description'	=> esc_html__( 'Choose single post page link post format settings.', 'tain' ),
	'choices'		=> array(
		'featured' 		=> esc_html__( 'Dark Overlay', 'tain' ),
		'theme-overlay' => esc_html__( 'Theme Overlay', 'tain' ),
		'theme' 		=> esc_html__( 'Theme Color Background', 'tain' ),
		'none' 			=> esc_html__( 'None', 'tain' )
	),
	'default'		=> 'featured',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Gallery Format
$settings = array(
	'id'			=> 'single-post-gallery-format',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Gallery Format', 'tain' ),
	'description'	=> esc_html__( 'Choose single post page gallery post format settings.', 'tain' ),
	'choices'		=> array(
		'default'	=> esc_html__( 'Default Gallery', 'tain' ),
		'popup' 	=> esc_html__( 'Popup Gallery', 'tain' ),
		'grid' 		=> esc_html__( 'Grid Popup Gallery', 'tain' )
	),
	'default'		=> 'default',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Post Format Settings End
$settings = array(
	'type'			=> 'section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Featured Slider
$settings = array(
	'id'			=> 'single-post-featured-slider',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Single Post Featured Slider', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable single post featured slider.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Sidebar Sticky
$settings = array(
	'id'			=> 'single-post-sidebar-sticky',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Sidebar Sticky', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable sidebar sticky.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Sidebar on Mobile
$settings = array(
	'id'			=> 'single-post-page-hide-sidebar',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Sidebar on Mobile', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable to show or hide sidebar on mobile.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Related Post Max Limit
$settings = array(
	'id'			=> 'related-max-posts',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Related Post Max Limit', 'tain' ),
	'description'	=> esc_html__( 'Enter related post maximum limit for get from posts query. Example 5', 'tain' ),
	'default'		=> '5',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Related Posts From
$settings = array(
	'id'			=> 'related-posts-filter',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Related Posts From', 'tain' ),
	'description'	=> esc_html__( 'Select related posts gets from category or tag.', 'tain' ),
	'choices'		=> array(
		'category'	=> esc_html__( 'Category', 'tain' ),
		'tag'		=> esc_html__( 'Tag', 'tain' )
	),
	'default'		=> 'category',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Advanced End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );