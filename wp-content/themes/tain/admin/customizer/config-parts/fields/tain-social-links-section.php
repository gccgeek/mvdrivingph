<?php

//Social Iocns Type
$settings = array(
	'id'			=> 'social-icons-type',
	'type'			=> 'radioimage',
	'title'			=> esc_html__( 'Social Iocns Type', 'tain' ),
	'description'	=> esc_html__( 'Choose your social icons type.', 'tain' ),
	'default'		=> 'circled',
	'items' 		=> array(
		'squared'	=> TAIN_ADMIN_URL . '/customizer/assets/images/social-icons/1.png',
		'rounded'	=> TAIN_ADMIN_URL . '/customizer/assets/images/social-icons/2.png',
		'circled'	=> TAIN_ADMIN_URL . '/customizer/assets/images/social-icons/3.png'		
	),
	'cols'			=> '1',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Bottom Social Iocns Type
$settings = array(
	'id'			=> 'social-icons-type-footer',
	'type'			=> 'radioimage',
	'title'			=> esc_html__( 'Footer Bottom Social Iocns Type', 'tain' ),
	'description'	=> esc_html__( 'Choose footer bottom social icons type.', 'tain' ),
	'default'		=> 'circled',
	'items' 		=> array(
		'squared'	=> TAIN_ADMIN_URL . '/customizer/assets/images/social-icons/1.png',
		'rounded'	=> TAIN_ADMIN_URL . '/customizer/assets/images/social-icons/2.png',
		'circled'	=> TAIN_ADMIN_URL . '/customizer/assets/images/social-icons/3.png'		
	),
	'cols'			=> '1',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Social Icons Fore
$settings = array(
	'id'			=> 'social-icons-fore',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Social Icons Fore', 'tain' ),
	'description'	=> esc_html__( 'Social icons fore color settings.', 'tain' ),
	'choices'		=> array(
		'black'		=> esc_html__( 'Black', 'tain' ),
		'white'		=> esc_html__( 'White', 'tain' ),
		'own'		=> esc_html__( 'Own Color', 'tain' )
	),
	'default'		=> 'white',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Social Icons Fore
$settings = array(
	'id'			=> 'social-icons-hfore',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Social Icons Fore Hover', 'tain' ),
	'description'	=> esc_html__( 'Social icons fore hover color settings.', 'tain' ),
	'choices'		=> array(
		'h-black'	=> esc_html__( 'Black', 'tain' ),
		'h-white'	=> esc_html__( 'White', 'tain' ),
		'h-own'		=> esc_html__( 'Own Color', 'tain' )
	),
	'default'		=> 'h-own',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Social Icons Background
$settings = array(
	'id'			=> 'social-icons-bg',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Social Icons Background', 'tain' ),
	'description'	=> esc_html__( 'Social icons background color settings.', 'tain' ),
	'choices'		=> array(
		'bg-black'		=> esc_html__( 'Black', 'tain' ),
		'bg-white'		=> esc_html__( 'White', 'tain' ),
		'bg-light'		=> esc_html__( 'RGBA Light', 'tain' ),
		'bg-dark'		=> esc_html__( 'RGBA Dark', 'tain' ),
		'bg-own'		=> esc_html__( 'Own Color', 'tain' ),
		'bg-transparent'=> esc_html__( 'Transparent', 'tain' )
	),
	'default'		=> 'bg-own',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Social Icons Background Hover
$settings = array(
	'id'			=> 'social-icons-hbg',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Social Icons Background Hover', 'tain' ),
	'description'	=> esc_html__( 'Social icons background hover color settings.', 'tain' ),
	'choices'		=> array(
		'hbg-black'		=> esc_html__( 'Black', 'tain' ),
		'hbg-white'		=> esc_html__( 'White', 'tain' ),
		'hbg-light'		=> esc_html__( 'RGBA Light', 'tain' ),
		'hbg-dark'		=> esc_html__( 'RGBA Dark', 'tain' ),
		'hbg-own'		=> esc_html__( 'Own Color', 'tain' ),
		'hbg-theme'		=> esc_html__( 'Theme Color', 'tain' ),
		'hbg-transparent'		=> esc_html__( 'Transparent', 'tain' )
	),
	'default'		=> 'hbg-white',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Facebook
$settings = array(
	'id'			=> 'social-fb',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Facebook', 'tain' ),
	'description'	=> esc_html__( 'Enter the facebook link. If no link means just leave it blank', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Twitter
$settings = array(
	'id'			=> 'social-twitter',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Twitter', 'tain' ),
	'description'	=> esc_html__( 'Enter the twitter link. If no link means just leave it blank', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Instagram
$settings = array(
	'id'			=> 'social-instagram',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Instagram', 'tain' ),
	'description'	=> esc_html__( 'Enter the instagram link. If no link means just leave it blank', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Pinterest
$settings = array(
	'id'			=> 'social-pinterest',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Pinterest', 'tain' ),
	'description'	=> esc_html__( 'Enter the pinterest link. If no link means just leave it blank', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Youtube
$settings = array(
	'id'			=> 'social-youtube',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Youtube', 'tain' ),
	'description'	=> esc_html__( 'Enter the youtube link. If no link means just leave it blank', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Vimeo
$settings = array(
	'id'			=> 'social-vimeo',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Vimeo', 'tain' ),
	'description'	=> esc_html__( 'Enter the vimeo link. If no link means just leave it blank', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Soundcloud
$settings = array(
	'id'			=> 'social-soundcloud',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Soundcloud', 'tain' ),
	'description'	=> esc_html__( 'Enter the soundcloud link. If no link means just leave it blank', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Yahoo
$settings = array(
	'id'			=> 'social-yahoo',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Yahoo', 'tain' ),
	'description'	=> esc_html__( 'Enter the yahoo link. If no link means just leave it blank', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Tumblr
$settings = array(
	'id'			=> 'social-tumblr',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Tumblr', 'tain' ),
	'description'	=> esc_html__( 'Enter the tumblr link. If no link means just leave it blank', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Mailto
$settings = array(
	'id'			=> 'social-mailto',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Mailto', 'tain' ),
	'description'	=> esc_html__( 'Enter the mailto link. If no link means just leave it blank', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Flickr
$settings = array(
	'id'			=> 'social-flickr',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Flickr', 'tain' ),
	'description'	=> esc_html__( 'Enter the flickr link. If no link means just leave it blank', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Dribbble
$settings = array(
	'id'			=> 'social-dribbble',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Dribbble', 'tain' ),
	'description'	=> esc_html__( 'Enter the dribbble link. If no link means just leave it blank', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//LinkedIn
$settings = array(
	'id'			=> 'social-linkedin',
	'type'			=> 'text',
	'title'			=> esc_html__( 'LinkedIn', 'tain' ),
	'description'	=> esc_html__( 'Enter the linkedin link. If no link means just leave it blank', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//RSS
$settings = array(
	'id'			=> 'social-rss',
	'type'			=> 'text',
	'title'			=> esc_html__( 'RSS', 'tain' ),
	'description'	=> esc_html__( 'Enter the rss link. If no link means just leave it blank', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );