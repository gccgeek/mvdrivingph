<?php

//Maintenance Mode Option
$settings = array(
	'id'			=> 'maintenance-mode',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Maintenance Mode Option', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable maintenance mode.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Maintenance Type
$settings = array(
	'id'			=> 'maintenance-type',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Maintenance Type', 'tain' ),
	'description'	=> esc_html__( 'Select maintenance mode page coming soon or maintenance.', 'tain' ),
	'choices'		=> array(
		'cs'		=> esc_html__( 'Coming Soon', 'tain' ),
		'mn'		=> esc_html__( 'Maintenance', 'tain' ),
		'cus'		=> esc_html__( 'Custom', 'tain' )
	),
	'default'		=> 'cs',
	'required'		=> array( 'maintenance-mode', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Maintenance Custom Page
$settings = array(
	'id'			=> 'maintenance-custom',
	'type'			=> 'pages',
	'title'			=> esc_html__( 'Maintenance Custom Page', 'tain' ),
	'description'	=> esc_html__( 'Enter course slug for register custom post type.', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'maintenance-mode', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Phone Number
$settings = array(
	'id'			=> 'maintenance-phone',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Phone Number', 'tain' ),
	'description'	=> esc_html__( 'Enter phone number shown on when maintenance mode actived', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'maintenance-mode', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Email Id
$settings = array(
	'id'			=> 'maintenance-email',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Email Id', 'tain' ),
	'description'	=> esc_html__( 'Enter email id shown on when maintenance mode actived', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'maintenance-mode', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Address
$settings = array(
	'id'			=> 'maintenance-address',
	'type'			=> 'textarea',
	'title'			=> esc_html__( 'Address', 'tain' ),
	'description'	=> esc_html__( 'Place here your address and info', 'tain' ),
	'default'		=> '',
	'required'		=> array( 'maintenance-mode', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );