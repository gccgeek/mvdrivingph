<?php

//Post Social Shares
$settings = array(
	'id'			=> 'post-social-shares',
	'type'			=> 'multicheck',
	'title'			=> esc_html__( 'Post Social Shares', 'tain' ),
	'description'	=> esc_html__( 'Actived social items only showing post share list.', 'tain' ),
	'default'		=> '',
	'items' 		=> array(
		'fb'		=> esc_html__( 'Facebook', 'tain' ),
		'twitter'	=> esc_html__( 'Twitter', 'tain' ),
		'linkedin'	=> esc_html__( 'Linkedin', 'tain' ),
		'pinterest'	=> esc_html__( 'Pinterest', 'tain' )
	),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Comments Social Shares
$settings = array(
	'id'			=> 'comments-social-shares',
	'type'			=> 'multicheck',
	'title'			=> esc_html__( 'Comments Social Shares', 'tain' ),
	'description'	=> esc_html__( 'Actived social items only showing comments share list.', 'tain' ),
	'default'		=> '',
	'items' 		=> array(
		'fb'		=> esc_html__( 'Facebook', 'tain' ),
		'twitter'	=> esc_html__( 'Twitter', 'tain' ),
		'linkedin'	=> esc_html__( 'Linkedin', 'tain' ),
		'pinterest'	=> esc_html__( 'Pinterest', 'tain' )
	),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );