<?php

//Smooth Scroll Option
$settings = array(
	'id'			=> 'smooth-opt',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Smooth Scroll Option', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable to append smooth scroll js to website.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Scroll Time
$settings = array(
	'id'			=> 'scroll-time',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Scroll Time', 'tain' ),
	'description'	=> esc_html__( 'Enter smooth scroll time in milliseconds. Example 600', 'tain' ),
	'default'		=> '600',
	'required'		=> array( 'smooth-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Scroll Distance
$settings = array(
	'id'			=> 'scroll-distance',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Scroll Distance', 'tain' ),
	'description'	=> esc_html__( 'Enter smooth scroll distance in value. Example 40', 'tain' ),
	'default'		=> '40',
	'required'		=> array( 'smooth-opt', '=', 1 ),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );