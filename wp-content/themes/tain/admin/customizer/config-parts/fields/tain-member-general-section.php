<?php

//Login/Signin Text
$settings = array(
	'id'			=> 'login-text',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Login/Signin Text', 'tain' ),
	'description'	=> esc_html__( 'Enter sign in text as per you choice.', 'tain' ),
	'default'		=> esc_html__( 'Login/Signin', 'tain' ),
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Logged Prefix Text
$settings = array(
	'id'			=> 'logged-text',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Logged Prefix Text', 'tain' ),
	'description'	=> esc_html__( 'Enter logged prefix text.', 'tain' ),
	'default'		=> esc_html__( 'Hello!', 'tain' ),
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Signout Prefix Text
$settings = array(
	'id'			=> 'signout-text',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Signout Text', 'tain' ),
	'description'	=> esc_html__( 'Enter signout text.', 'tain' ),
	'default'		=> esc_html__( 'Signout', 'tain' ),
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Google Login Option
$settings = array(
	'id'			=> 'google-logins-opt',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Google Login Option', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable google login option.', 'tain' ),
	'default'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Facebook Login Option
$settings = array(
	'id'			=> 'facebook-logins-opt',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Facebook Login Option', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable facebook login option.', 'tain' ),
	'default'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Admin Email Id
$settings = array(
	'id'			=> 'admin-email-id',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Admin Email Id', 'tain' ),
	'description'	=> esc_html__( 'Enter valid admin email for sending update emails of every process. If any user change their password, Then this will send a copy with this email too.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Login Form Description
$settings = array(
	'id'			=> 'login-description',
	'type'			=> 'textarea',
	'title'			=> esc_html__( 'Login Form Description', 'tain' ),
	'description'	=> esc_html__( 'You can paste shortcode here. Just display the description on login form.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );