<?php

//Woocommerce Shop Template
$settings = array(
	'id'			=> 'woo-page-template',
	'type'			=> 'radioimage',
	'title'			=> esc_html__( 'Woocommerce Shop Template', 'tain' ),
	'description'	=> esc_html__( 'Choose your current archive page template.', 'tain' ),
	'default'		=> 'right-sidebar',
	'items' 		=> array(
		'no-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/1.png',
		'right-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/2.png',
		'left-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/3.png',
		'both-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/4.png'		
	),
	'cols'			=> '4',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Left Sidebar
$settings = array(
	'id'			=> 'woo-left-sidebar',
	'type'			=> 'sidebars',
	'title'			=> esc_html__( 'Left Sidebar', 'tain' ),
	'description'	=> esc_html__( 'Select widget area for showing on left side.', 'tain' ),
	'default'		=> 'sidebar-1',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Right Sidebar
$settings = array(
	'id'			=> 'woo-right-sidebar',
	'type'			=> 'sidebars',
	'title'			=> esc_html__( 'Right Sidebar', 'tain' ),
	'description'	=> esc_html__( 'Select widget area for showing on right side.', 'tain' ),
	'default'		=> 'sidebar-1',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Shop Columns
$settings = array(
	'id'			=> 'woo-shop-columns',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Shop Columns', 'tain' ),
	'description'	=> esc_html__( 'This is column settings woocommerce shop page products.', 'tain' ),
	'choices'		=> array(
		'2'		=> esc_html__( '2 Columns', 'tain' ),
		'3'		=> esc_html__( '3 Columns', 'tain' ),
		'4'		=> esc_html__( '4 Columns', 'tain' ),
		'5'		=> esc_html__( '5 Columns', 'tain' ),
		'6'		=> esc_html__( '6 Columns', 'tain' ),
	),
	'default'		=> '3',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Shop Product Per Page
$settings = array(
	'id'			=> 'woo-shop-ppp',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Shop Product Per Page', 'tain' ),
	'description'	=> esc_html__( 'This is column settings woocommerce related products per page', 'tain' ),
	'default'		=> '12',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Related Product Per Page
$settings = array(
	'id'			=> 'woo-related-ppp',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Related Product Per Page', 'tain' ),
	'description'	=> esc_html__( 'This is column settings woocommerce related products per page', 'tain' ),
	'default'		=> '3',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );