<?php

//Single Post Slide Items to Display
$settings = array(
	'id'			=> 'single-slide-items',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Items to Display', 'tain' ),
	'description'	=> esc_html__( 'Enter number of slider items to display', 'tain' ),
	'default'		=> '3',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Slider Items to Display Tab
$settings = array(
	'id'			=> 'single-slide-tab',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Items to Display Tab', 'tain' ),
	'description'	=> esc_html__( 'Enter number of slider items to display on tab', 'tain' ),
	'default'		=> '1',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Slider Items to Display on Mobile
$settings = array(
	'id'			=> 'single-slide-mobile',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Items to Display on Mobile', 'tain' ),
	'description'	=> esc_html__( 'Enter items to display on mobile view. Example 1', 'tain' ),
	'default'		=> '1',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Slider Items Scrollby
$settings = array(
	'id'			=> 'single-slide-scrollby',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Items Scrollby', 'tain' ),
	'description'	=> esc_html__( 'Enter slider items scrollby. Example 1', 'tain' ),
	'default'		=> '1',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Slider Slide Autoplay
$settings = array(
	'id'			=> 'single-slide-autoplay',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Slide Autoplay', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable slide autoplay.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Slider Slide Center
$settings = array(
	'id'			=> 'single-slide-center',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Slide Center', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable slide center.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Slider Slide Duration
$settings = array(
	'id'			=> 'single-slide-duration',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Slide Duration', 'tain' ),
	'description'	=> esc_html__( 'Enter slide duration for each (in Milli Seconds). Example 5000', 'tain' ),
	'default'		=> '5000',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Slider Slide Smart Speed
$settings = array(
	'id'			=> 'single-slide-smartspeed',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Slide Smart Speed', 'tain' ),
	'description'	=> esc_html__( 'Enter slide smart speed for each (in Milli Seconds). Example 250', 'tain' ),
	'default'		=> '250',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Slider Infinite Loop
$settings = array(
	'id'			=> 'single-slide-center',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Infinite Loop', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable infinite loop.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Slider Slide Items Margin
$settings = array(
	'id'			=> 'single-slide-margin',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Slide Items Margin', 'tain' ),
	'description'	=> esc_html__( 'Enter slide item margin( item spacing ). Example 10', 'tain' ),
	'default'		=> '10',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Slider Slide Pagination
$settings = array(
	'id'			=> 'single-slide-pagination',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Slide Pagination', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable slide pagination.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Slider Slide Navigation
$settings = array(
	'id'			=> 'single-slide-navigation',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Slide Navigation', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable slide navigation.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Single Post Slider Slide Auto Height
$settings = array(
	'id'			=> 'single-slide-autoheight',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Slide Auto Height', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable slide item auto height.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );