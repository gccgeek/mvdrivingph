<?php

//Body Background Settings
$settings = array(
	'id'			=> 'body-background',
	'type'			=> 'background',
	'title'			=> esc_html__( 'Body Background Settings', 'tain' ),
	'description'	=> esc_html__( 'This is settings for body background with image, color, etc.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );