<?php

//Blog Slide Items to Display
$settings = array(
	'id'			=> 'blog-slide-items',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Items to Display', 'tain' ),
	'description'	=> esc_html__( 'Enter number of slider items to display', 'tain' ),
	'default'		=> '3',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Slider Items to Display Tab
$settings = array(
	'id'			=> 'blog-slide-tab',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Items to Display Tab', 'tain' ),
	'description'	=> esc_html__( 'Enter number of slider items to display on tab', 'tain' ),
	'default'		=> '1',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Slider Items to Display on Mobile
$settings = array(
	'id'			=> 'blog-slide-mobile',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Items to Display on Mobile', 'tain' ),
	'description'	=> esc_html__( 'Enter items to display on mobile view. Example 1', 'tain' ),
	'default'		=> '1',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Slider Items Scrollby
$settings = array(
	'id'			=> 'blog-slide-scrollby',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Items Scrollby', 'tain' ),
	'description'	=> esc_html__( 'Enter slider items scrollby. Example 1', 'tain' ),
	'default'		=> '1',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Slider Slide Autoplay
$settings = array(
	'id'			=> 'blog-slide-autoplay',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Slide Autoplay', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable slide autoplay.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Slider Slide Center
$settings = array(
	'id'			=> 'blog-slide-center',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Slide Center', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable slide center.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Slider Slide Duration
$settings = array(
	'id'			=> 'blog-slide-duration',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Slide Duration', 'tain' ),
	'description'	=> esc_html__( 'Enter slide duration for each (in Milli Seconds). Example 5000', 'tain' ),
	'default'		=> '5000',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Slider Slide Smart Speed
$settings = array(
	'id'			=> 'blog-slide-smartspeed',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Slide Smart Speed', 'tain' ),
	'description'	=> esc_html__( 'Enter slide smart speed for each (in Milli Seconds). Example 250', 'tain' ),
	'default'		=> '250',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Slider Infinite Loop
$settings = array(
	'id'			=> 'blog-slide-center',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Infinite Loop', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable infinite loop.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Slider Slide Items Margin
$settings = array(
	'id'			=> 'blog-slide-margin',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Slide Items Margin', 'tain' ),
	'description'	=> esc_html__( 'Enter slide item margin( item spacing ). Example 10', 'tain' ),
	'default'		=> '10',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Slider Slide Pagination
$settings = array(
	'id'			=> 'blog-slide-pagination',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Slide Pagination', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable slide pagination.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Slider Slide Navigation
$settings = array(
	'id'			=> 'blog-slide-navigation',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Slide Navigation', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable slide navigation.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Blog Slider Slide Auto Height
$settings = array(
	'id'			=> 'blog-slide-autoheight',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Slide Auto Height', 'tain' ),
	'description'	=> esc_html__( 'Enable/Disable slide item auto height.', 'tain' ),
	'default'		=> 0,
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );