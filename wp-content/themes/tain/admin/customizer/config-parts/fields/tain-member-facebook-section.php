<?php

//Facebook App ID
$settings = array(
	'id'			=> 'social-facebook-app-id',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Facebook App ID', 'tain' ),
	'description'	=> esc_html__( 'Set facebook login app id for login via facebook. You can refer here: ', 'tain' ) . 'https://developers.facebook.com/apps/',
	'default'		=> ''
);
TainCustomizerConfig::buildFields( $settings );

//Facebook Client Secret
$settings = array(
	'id'			=> 'social-facebook-app-secret',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Facebook Client Secret', 'tain' ),
	'description'	=> esc_html__( 'Set facebook login client secret for login via facebook.', 'tain' ),
	'default'		=> ''
);
TainCustomizerConfig::buildFields( $settings );

//Facebook Redirect
$settings = array(
	'id'			=> 'social-facebook-redirect',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Facebook Login Redirect URL', 'tain' ),
	'description'	=> esc_html__( 'Set facebook login redirect url.', 'tain' ),
	'default'		=> ''
);
TainCustomizerConfig::buildFields( $settings );