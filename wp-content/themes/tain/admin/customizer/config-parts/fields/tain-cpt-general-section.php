<?php

//Custom Post Types
$settings = array(
	'id'			=> 'cpt-opts',
	'type'			=> 'multicheck',
	'title'			=> esc_html__( 'Custom Post Types', 'tain' ),
	'description'	=> esc_html__( 'Enable the custom post types which are need, once done save theme options button. Then refresh page, you can see the enabled CPT options are showing sub level.', 'tain' ),
	'default'		=> '',
	'items' 		=> array(
		'portfolio'	    => esc_html__( 'Portfolio', 'tain' ),
		'team'	        => esc_html__( 'Team', 'tain' ),
		'testimonial'	=> esc_html__( 'Testimonial', 'tain' ),
		'events'	    => esc_html__( 'Events', 'tain' ),
		'courses'	    => esc_html__( 'Courses', 'tain' ),
	),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );