<?php

//Header Top Sliding Bar Typography
$settings = array(
	'id'			=> 'top-sliding-typography',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'Header Top Sliding Bar Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for header top sliding bar typography', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Topbar Typography
$settings = array(
	'id'			=> 'header-topbar-typography',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'Header Topbar Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for header top bar typography', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Logo Section Typography
$settings = array(
	'id'			=> 'header-logobar-typography',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'Header Logo Section Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for header logo section typography', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Header Navbar Typography
$settings = array(
	'id'			=> 'header-navbar-typography',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'Header Navbar Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for header navbar typography', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Sticky/Fixed Header Typography
$settings = array(
	'id'			=> 'header-fixed-typography',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'Sticky/Fixed Header Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for sticky or fixed header typography', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Mobile Menu Typography
$settings = array(
	'id'			=> 'mobile-menu-typography',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'Mobile Menu Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for mobile menu typography', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Typography
$settings = array(
	'id'			=> 'footer-typography',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'Footer Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for footer typography', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Top Typography
$settings = array(
	'id'			=> 'footer-top-typography',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'Footer Top Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for footer top typography', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Middle Typography
$settings = array(
	'id'			=> 'footer-middle-typography',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'Footer Middle Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for footer middle typography', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Footer Bottom Typography
$settings = array(
	'id'			=> 'footer-bottom-typography',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'Footer Bottom Typography', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for footer bottom typography', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );