<?php

//Import Theme Option
$settings = array(
	'id'			=> 'import-theme-option',
	'type'			=> 'import',
	'title'			=> esc_html__( 'Import Theme Option', 'tain' ),
	'description'	=> esc_html__( 'This is the option to import theme option value which given from theme authors/documentation/demo import.', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );