<?php

//Google Client ID
$settings = array(
	'id'			=> 'social-google-client-id',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Google Client ID', 'tain' ),
	'description'	=> esc_html__( 'Set google login client id for login via google. You can refer here: ', 'tain' ) . 'https://console.developers.google.com/',
	'default'		=> ''
);
TainCustomizerConfig::buildFields( $settings );

//Google Client Secret
$settings = array(
	'id'			=> 'social-google-client-secret',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Google Client Secret', 'tain' ),
	'description'	=> esc_html__( 'Set google login client secret for login via google.', 'tain' ),
	'default'		=> ''
);
TainCustomizerConfig::buildFields( $settings );

//Google Redirect
$settings = array(
	'id'			=> 'social-google-redirect',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Google Login Redirect URL', 'tain' ),
	'description'	=> esc_html__( 'Set google login redirect url.', 'tain' ),
	'default'		=> ''
);
TainCustomizerConfig::buildFields( $settings );