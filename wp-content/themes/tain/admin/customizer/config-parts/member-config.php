<?php

//Theme Option -> Member
$theme_member_panel = new Tain_WP_Customize_Panel( $wp_customize, 'theme_member_panel', array(
	'title'			=> esc_html__( 'Tain Member Settings', 'tain' ),
	'description'	=> esc_html__( 'These are the tain member addon settings of tain Theme.', 'tain' ),
	'priority'		=> 12,
	'panel'			=> 'tain_theme_panel'
));
$wp_customize->add_panel( $theme_member_panel );

//Member -> Member General
$tain_member_general_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_member_general_section', array(
	'title'			=> esc_html__( 'Member General', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for member general details.', 'tain' ),
	'priority'		=> 1,
	'panel'			=> 'theme_member_panel'
));
$wp_customize->add_section( $tain_member_general_section );

//General
$wp_customize->add_setting('ajax_trigger_tain_member_general_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_member_general_section', array(
	'section'		=> 'tain_member_general_section'
)));


//Member -> Member Google
$tain_member_google_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_member_google_section', array(
	'title'			=> esc_html__( 'Login via Google', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for google member.', 'tain' ),
	'priority'		=> 1,
	'panel'			=> 'theme_member_panel'
));
$wp_customize->add_section( $tain_member_google_section );

//Google
$wp_customize->add_setting('ajax_trigger_tain_member_google_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_member_google_section', array(
	'section'		=> 'tain_member_google_section'
)));

//Member -> Member Facebook
$tain_member_facebook_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_member_facebook_section', array(
	'title'			=> esc_html__( 'Login via Facebook', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for facebook member.', 'tain' ),
	'priority'		=> 1,
	'panel'			=> 'theme_member_panel'
));
$wp_customize->add_section( $tain_member_facebook_section );

//Facebook
$wp_customize->add_setting('ajax_trigger_tain_member_facebook_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_member_facebook_section', array(
	'section'		=> 'tain_member_facebook_section'
)));

