<?php

//Theme Option -> Sliders
$theme_slide_panel = new Tain_WP_Customize_Panel( $wp_customize, 'theme_slide_panel', array(
	'title'			=> esc_html__( 'Sliders', 'tain' ),
	'description'	=> esc_html__( 'These are the theme sliders settings', 'tain' ),
	'priority'		=> 8,
	'panel'			=> 'tain_theme_panel'
));
$wp_customize->add_panel( $theme_slide_panel );

//Sliders -> Featured Slider
$tain_general_featured_slide_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_general_featured_slide_section', array(
	'title'			=> esc_html__( 'Featured Slider', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for featured slider', 'tain' ),
	'priority'		=> 1,
	'panel'			=> 'theme_slide_panel'
));
$wp_customize->add_section( $tain_general_featured_slide_section );

//Featured Slider
$wp_customize->add_setting('ajax_trigger_tain_general_featured_slide_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_general_featured_slide_section', array(
	'section'		=> 'tain_general_featured_slide_section'
)));

//Sliders -> Related Slider
$tain_general_related_slide_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_general_related_slide_section', array(
	'title'			=> esc_html__( 'Related Slider', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for related slider', 'tain' ),
	'priority'		=> 2,
	'panel'			=> 'theme_slide_panel'
));
$wp_customize->add_section( $tain_general_related_slide_section );

//Related Slider
$wp_customize->add_setting('ajax_trigger_tain_general_related_slide_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_general_related_slide_section', array(
	'section'		=> 'tain_general_related_slide_section'
)));

//Sliders -> Blog Post Slider
$tain_general_blog_slide_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_general_blog_slide_section', array(
	'title'			=> esc_html__( 'Blog Post Slider', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for blog post slider', 'tain' ),
	'priority'		=> 3,
	'panel'			=> 'theme_slide_panel'
));
$wp_customize->add_section( $tain_general_blog_slide_section );

//Blog Post Slider
$wp_customize->add_setting('ajax_trigger_tain_general_blog_slide_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_general_blog_slide_section', array(
	'section'		=> 'tain_general_blog_slide_section'
)));

//Sliders -> Single Post Slider
$tain_general_single_slide_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_general_single_slide_section', array(
	'title'			=> esc_html__( 'Single Post Slider', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for single post slider', 'tain' ),
	'priority'		=> 4,
	'panel'			=> 'theme_slide_panel'
));
$wp_customize->add_section( $tain_general_single_slide_section );

//Single Post Slider
$wp_customize->add_setting('ajax_trigger_tain_general_single_slide_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_general_single_slide_section', array(
	'section'		=> 'tain_general_single_slide_section'
)));