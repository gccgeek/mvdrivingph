<?php

//Theme Option -> Header
$theme_header_panel = new Tain_WP_Customize_Panel( $wp_customize, 'theme_header_panel', array(
	'title'			=> esc_html__( 'Header', 'tain' ),
	'description'	=> esc_html__( 'These are header general settings of tain theme', 'tain' ),
	'priority'		=> 5,
	'panel'			=> 'tain_theme_panel'
));
$wp_customize->add_panel( $theme_header_panel );

//Header -> Header General
$tain_header_general_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_header_general_section', array(
	'title'			=> esc_html__( 'Header General', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for general header.', 'tain' ),
	'priority'		=> 1,
	'panel'			=> 'theme_header_panel'
));
$wp_customize->add_section( $tain_header_general_section );

//Header General
$wp_customize->add_setting('ajax_trigger_tain_header_general_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_header_general_section', array(
	'section'		=> 'tain_header_general_section'
)));

//Header -> Header Top Section
$tain_header_topbar_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_header_topbar_section', array(
	'title'			=> esc_html__( 'Header Top', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for header top.', 'tain' ),
	'priority'		=> 2,
	'panel'			=> 'theme_header_panel'
));
$wp_customize->add_section( $tain_header_topbar_section );

//Header Top
$wp_customize->add_setting('ajax_trigger_tain_header_topbar_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_header_topbar_section', array(
	'section'		=> 'tain_header_topbar_section'
)));

//Header -> Header Middle Section
$tain_header_logobar_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_header_logobar_section', array(
	'title'			=> esc_html__( 'Header Middle', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for header middle(logo section).', 'tain' ),
	'priority'		=> 3,
	'panel'			=> 'theme_header_panel'
));
$wp_customize->add_section( $tain_header_logobar_section );

//Header Middle
$wp_customize->add_setting('ajax_trigger_tain_header_logobar_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_header_logobar_section', array(
	'section'		=> 'tain_header_logobar_section'
)));

//Header -> Header Navbar Section
$tain_header_navbar_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_header_navbar_section', array(
	'title'			=> esc_html__( 'Header Bottom', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for header bottom(navbar).', 'tain' ),
	'priority'		=> 4,
	'panel'			=> 'theme_header_panel'
));
$wp_customize->add_section( $tain_header_navbar_section );

//Header Navbar
$wp_customize->add_setting('ajax_trigger_tain_header_navbar_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_header_navbar_section', array(
	'section'		=> 'tain_header_navbar_section'
)));

//Header -> Header Left/Right Navbar
$tain_header_fixed_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_header_fixed_section', array(
	'title'			=> esc_html__( 'Left/Right Navbar', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for header left/right navbar.', 'tain' ),
	'priority'		=> 5,
	'panel'			=> 'theme_header_panel'
));
$wp_customize->add_section( $tain_header_fixed_section );

//Header Left/Right
$wp_customize->add_setting('ajax_trigger_tain_header_fixed_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_header_fixed_section', array(
	'section'		=> 'tain_header_fixed_section'
)));

//Header -> Mobile Menu
$tain_mobile_menu_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_mobile_menu_section', array(
	'title'			=> esc_html__( 'Mobile Menu', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for mobile header and mobile menu.', 'tain' ),
	'priority'		=> 6,
	'panel'			=> 'theme_header_panel'
));
$wp_customize->add_section( $tain_mobile_menu_section );

//Mobile Menu
$wp_customize->add_setting('ajax_trigger_tain_mobile_menu_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_mobile_menu_section', array(
	'section'		=> 'tain_mobile_menu_section'
)));

//Header -> Top Sliding Bar
$tain_header_topslidingbar_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_header_topslidingbar_section', array(
	'title'			=> esc_html__( 'Top Sliding Bar', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for top sliding bar.', 'tain' ),
	'priority'		=> 7,
	'panel'			=> 'theme_header_panel'
));
$wp_customize->add_section( $tain_header_topslidingbar_section );

//Top Sliding Bar
$wp_customize->add_setting('ajax_trigger_tain_header_topslidingbar_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_header_topslidingbar_section', array(
	'section'		=> 'tain_header_topslidingbar_section'
)));