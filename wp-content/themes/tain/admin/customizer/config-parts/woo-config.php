<?php

//Theme Option -> WooCommerce
$theme_woo_panel = new Tain_WP_Customize_Panel( $wp_customize, 'theme_woo_panel', array(
	'title'			=> esc_html__( 'WooCommerce', 'tain' ),
	'description'	=> esc_html__( 'These are the WooCommerce settings of tain Theme.', 'tain' ),
	'priority'		=> 12,
	'panel'			=> 'tain_theme_panel'
));
$wp_customize->add_panel( $theme_woo_panel );

//WooCommerce -> Woo General
$tain_woo_general_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_woo_general_section', array(
	'title'			=> esc_html__( 'Woo General', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for woocommerce general options.', 'tain' ),
	'priority'		=> 1,
	'panel'			=> 'theme_woo_panel'
));
$wp_customize->add_section( $tain_woo_general_section );

//Woo General
$wp_customize->add_setting('ajax_trigger_tain_woo_general_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_woo_general_section', array(
	'section'		=> 'tain_woo_general_section'
)));

//WooCommerce -> Archive Template
$tain_woo_archive_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_woo_archive_section', array(
	'title'			=> esc_html__( 'Archive Template', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for woocommerce archive page template.', 'tain' ),
	'priority'		=> 2,
	'panel'			=> 'theme_woo_panel'
));
$wp_customize->add_section( $tain_woo_archive_section );

//Archive Template
$wp_customize->add_setting('ajax_trigger_tain_woo_archive_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_woo_archive_section', array(
	'section'		=> 'tain_woo_archive_section'
)));

//WooCommerce -> Single Product Template
$tain_woo_single_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_woo_single_section', array(
	'title'			=> esc_html__( 'Single Product Template', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for woocommerce single product template.', 'tain' ),
	'priority'		=> 3,
	'panel'			=> 'theme_woo_panel'
));
$wp_customize->add_section( $tain_woo_single_section );

//Single Product Template
$wp_customize->add_setting('ajax_trigger_tain_woo_single_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_woo_single_section', array(
	'section'		=> 'tain_woo_single_section'
)));

//WooCommerce -> Woo Related Slider
$tain_general_woo_slide_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_general_woo_slide_section', array(
	'title'			=> esc_html__( 'Woo Related Slider', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for woo related products slider', 'tain' ),
	'priority'		=> 4,
	'panel'			=> 'theme_woo_panel'
));
$wp_customize->add_section( $tain_general_woo_slide_section );

//Woo Related Slider
$wp_customize->add_setting('ajax_trigger_tain_general_woo_slide_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_general_woo_slide_section', array(
	'section'		=> 'tain_general_woo_slide_section'
)));