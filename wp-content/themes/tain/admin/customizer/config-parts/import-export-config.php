<?php

//Theme Option -> Import and Export
$theme_import_export_panel = new Tain_WP_Customize_Panel( $wp_customize, 'theme_import_export_panel', array(
	'title'			=> esc_html__( 'Import and Export', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for import or export of current site theme options.', 'tain' ),
	'priority'		=> 14,
	'panel'			=> 'tain_theme_panel'
));
$wp_customize->add_panel( $theme_import_export_panel );

//Import and Export -> Import
$tain_import_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_import_section', array(
	'title'			=> esc_html__( 'Import', 'tain' ),
	'description'	=> esc_html__( 'This is the import setting for current theme option values.', 'tain' ),
	'priority'		=> 1,
	'panel'			=> 'theme_import_export_panel'
));
$wp_customize->add_section( $tain_import_section );

//Import
$wp_customize->add_setting('ajax_trigger_tain_import_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_import_section', array(
	'section'		=> 'tain_import_section'
)));

//Import and Export -> Export
$tain_export_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_export_section', array(
	'title'			=> esc_html__( 'Export', 'tain' ),
	'description'	=> esc_html__( 'This is the export setting for current theme option values.', 'tain' ),
	'priority'		=> 2,
	'panel'			=> 'theme_import_export_panel'
));
$wp_customize->add_section( $tain_export_section );

//Export
$wp_customize->add_setting('ajax_trigger_tain_export_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_export_section', array(
	'section'		=> 'tain_export_section'
)));