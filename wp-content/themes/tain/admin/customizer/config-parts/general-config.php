<?php

//Theme Option -> General
$theme_general_panel = new Tain_WP_Customize_Panel( $wp_customize, 'theme_general_panel', array(
	'title'			=> esc_html__( 'General', 'tain' ),
	'description'	=> esc_html__( 'These are the general settings of Tain theme', 'tain' ),
	'priority'		=> 1,
	'panel'			=> 'tain_theme_panel'
));
$wp_customize->add_panel( $theme_general_panel );

//General -> Layout
$tain_layout_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_layout_section', array(
	'title'			=> esc_html__( 'Layout', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for theme layouts', 'tain' ),
	'priority'		=> 1,
	'panel'			=> 'theme_general_panel'
));
$wp_customize->add_section( $tain_layout_section );

//Layout
$wp_customize->add_setting('ajax_trigger_tain_layout_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_layout_section', array(
	'section'		=> 'tain_layout_section'
)));

//General -> Loaders
$tain_loader_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_loader_section', array(
	'title'			=> esc_html__('Loaders', 'tain'),
	'description'	=> esc_html__( 'This is the setting for theme loader images.', 'tain' ),
	'priority'		=> 2,
	'panel'			=> 'theme_general_panel'
));
$wp_customize->add_section( $tain_loader_section );

//Loaders
$wp_customize->add_setting('ajax_trigger_tain_loader_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_loader_section', array(
	'section'		=> 'tain_loader_section'
)));

//General -> Theme Logo
$tain_logo_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_logo_section', array(
	'title'			=> esc_html__('Site Logo\'s', 'tain'),
	'description'	=> esc_html__( 'This is the setting for all the site logo\'s.', 'tain' ),
	'priority'		=> 3,
	'panel'			=> 'theme_general_panel'
));
$wp_customize->add_section( $tain_logo_section );

//Theme Logo
$wp_customize->add_setting('ajax_trigger_tain_logo_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_logo_section', array(
	'section'		=> 'tain_logo_section'
)));

//General -> API's
$tain_api_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_api_section', array(
	'title'			=> esc_html__('API', 'tain'),
	'description'	=> esc_html__( 'This is the setting for all the api\'s where used in this site.', 'tain' ),
	'priority'		=> 4,
	'panel'			=> 'theme_general_panel'
));
$wp_customize->add_section( $tain_api_section );

//API's
$wp_customize->add_setting('ajax_trigger_tain_api_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_api_section', array(
	'section'		=> 'tain_api_section'
)));

//General -> Comments
$tain_comments_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_comments_section', array(
	'title'			=> esc_html__('Comments', 'tain'),
	'description'	=> esc_html__( 'This is the setting for comments.', 'tain' ),
	'priority'		=> 5,
	'panel'			=> 'theme_general_panel'
));
$wp_customize->add_section( $tain_comments_section );

//Comments
$wp_customize->add_setting('ajax_trigger_tain_comments_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_comments_section', array(
	'section'		=> 'tain_comments_section'
)));

//General -> Smooth Scroll
$tain_smooth_scroll_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_smooth_scroll_section', array(
	'title'			=> esc_html__('Smooth Scroll', 'tain'),
	'description'	=> esc_html__( 'This is the setting for page smooth scroll.', 'tain' ),
	'priority'		=> 6,
	'panel'			=> 'theme_general_panel'
));
$wp_customize->add_section( $tain_smooth_scroll_section );

//Smooth Scroll
$wp_customize->add_setting('ajax_trigger_tain_smooth_scroll_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_smooth_scroll_section', array(
	'section'		=> 'tain_smooth_scroll_section'
)));

//General -> Media Settings
$tain_media_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_media_section', array(
	'title'			=> esc_html__('Media Settings', 'tain'),
	'description'	=> esc_html__( 'This is the setting for media sizes', 'tain' ),
	'priority'		=> 7,
	'panel'			=> 'theme_general_panel'
));
$wp_customize->add_section( $tain_media_section );

//Media Settings
$wp_customize->add_setting('ajax_trigger_tain_media_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_media_section', array(
	'section'		=> 'tain_media_section'
)));

//General -> RTL
$tain_rtl_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_rtl_section', array(
	'title'			=> esc_html__('RTL', 'tain'),
	'description'	=> esc_html__( 'This is the setting for theme view RTL', 'tain' ),
	'priority'		=> 8,
	'panel'			=> 'theme_general_panel'
));
$wp_customize->add_section( $tain_rtl_section );

//RTL
$wp_customize->add_setting('ajax_trigger_tain_rtl_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_rtl_section', array(
	'section'		=> 'tain_rtl_section'
)));