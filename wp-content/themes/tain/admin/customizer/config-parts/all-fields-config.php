<?php

//Text
$settings = array(
	'id'			=> 'ajax-trigger-text-test',
	'type'			=> 'text',
	'title'			=> esc_html__( 'Text Field', 'tain' ),
	'description'	=> esc_html__( 'This is text field', 'tain' ),
	'default'		=> '',
	'refresh'		=> 0,
	'instant'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Textarea
$settings = array(
	'id'			=> 'ajax-trigger-textarea-test',
	'type'			=> 'textarea',
	'title'			=> esc_html__( 'Textarea Field', 'tain' ),
	'description'	=> esc_html__( 'This is textarea field', 'tain' ),
	'default'		=> '',
	'refresh'		=> 0,
	'instant'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Select
$settings = array(
	'id'			=> 'ajax-trigger-select-test',
	'type'			=> 'select',
	'title'			=> esc_html__( 'Select Field', 'tain' ),
	'description'	=> esc_html__( 'This is select field', 'tain' ),
	'choices'		=> array(
		'boxed'		=> esc_html__( 'Boxed', 'tain' ),
		'wide'		=> esc_html__( 'Wide', 'tain' )
	),
	'default'		=> 'wide',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Color
$settings = array(
	'id'			=> 'ajax-trigger-color-test',
	'type'			=> 'color',
	'title'			=> esc_html__( 'Color Field', 'tain' ),
	'description'	=> esc_html__( 'This is color field', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Image
$settings = array(
	'id'			=> 'ajax-trigger-image-test',
	'type'			=> 'image',
	'title'			=> esc_html__( 'Image Field', 'tain' ),
	'description'	=> esc_html__( 'This is image field', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Alpha Color
$settings = array(
	'id'			=> 'ajax-trigger-alpha-test',
	'type'			=> 'alpha',
	'title'			=> esc_html__( 'Alpha Field', 'tain' ),
	'description'	=> esc_html__( 'This is alpha field', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Background
$settings = array(
	'id'			=> 'ajax-trigger-bg-test',
	'type'			=> 'background',
	'title'			=> esc_html__( 'Background Field', 'tain' ),
	'description'	=> esc_html__( 'This is background field', 'tain' ),
	'default'		=> '',
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Border
$settings = array(
	'id'			=> 'ajax-trigger-border-test',
	'type'			=> 'border',
	'title'			=> esc_html__( 'Border Field', 'tain' ),
	'description'	=> esc_html__( 'This is border field', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Dimension
$settings = array(
	'id'			=> 'ajax-trigger-dimension-test',
	'type'			=> 'dimension',
	'title'			=> esc_html__( 'Dimension Field', 'tain' ),
	'description'	=> esc_html__( 'This is dimension field', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Links
$settings = array(
	'id'			=> 'ajax-trigger-link-color-test',
	'type'			=> 'link',
	'title'			=> esc_html__( 'Link Color Field', 'tain' ),
	'description'	=> esc_html__( 'This is link color field', 'tain' ),
	'default'		=> '',
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Multi Check
$settings = array(
	'id'			=> 'ajax-trigger-multi-check-test',
	'type'			=> 'multicheck',
	'title'			=> esc_html__( 'Multi Check Field', 'tain' ),
	'description'	=> esc_html__( 'This is multi check field', 'tain' ),
	'default'		=> '',
	'items' 		=> array(
		'portfolio'	    => esc_html__( 'Portfolio', 'tain' ),
		'team'	        => esc_html__( 'Team', 'tain' ),
		'testimonial'	=> esc_html__( 'Testimonial', 'tain' ),
		'events'	    => esc_html__( 'Events', 'tain' ),
		'courses'	    => esc_html__( 'Courses', 'tain' ),
	),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Radio Image
$settings = array(
	'id'			=> 'ajax-trigger-radio-image-test',
	'type'			=> 'radioimage',
	'title'			=> esc_html__( 'Radio Image Field', 'tain' ),
	'description'	=> esc_html__( 'This is radio image field', 'tain' ),
	'default'		=> 'no-sidebar',
	'items' 		=> array(
		'no-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/1.png',
		'right-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/2.png',
		'left-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/3.png',
		'both-sidebar'	=> TAIN_ADMIN_URL . '/customizer/assets/images/page-layouts/4.png'		
	),
	'cols'			=> '4',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Sidebars
$settings = array(
	'id'			=> 'ajax-trigger-sidebars-test',
	'type'			=> 'sidebars',
	'title'			=> esc_html__( 'Sidebar Field', 'tain' ),
	'description'	=> esc_html__( 'This is sidebar field', 'tain' ),
	'default'		=> '',
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Toggle Switch
$settings = array(
	'id'			=> 'ajax-trigger-toggle-test',
	'type'			=> 'toggle',
	'title'			=> esc_html__( 'Toggle Switch Field', 'tain' ),
	'description'	=> esc_html__( 'This is toggle switch field', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Pages
$settings = array(
	'id'			=> 'ajax-trigger-pages-test',
	'type'			=> 'pages',
	'title'			=> esc_html__( 'Pages Field', 'tain' ),
	'description'	=> esc_html__( 'This is pages field', 'tain' ),
	'default'		=> '',
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Height Width
$settings = array(
	'id'			=> 'ajax-trigger-hw-test',
	'type'			=> 'hw',
	'title'			=> esc_html__( 'Height Width Field', 'tain' ),
	'description'	=> esc_html__( 'This is height width field', 'tain' ),
	'default'		=> '',
	'refresh'		=> 0
);
TainCustomizerConfig::buildFields( $settings );

//Fonts
$settings = array(
	'id'			=> 'ajax-trigger-fonts-test',
	'type'			=> 'fonts',
	'title'			=> esc_html__( 'Google Fonts Field', 'tain' ),
	'description'	=> esc_html__( 'This is fonts field', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Drag Drop
$settings = array(
	'id'			=> 'ajax-trigger-dd-test',
	'type'			=> 'dragdrop',
	'title'			=> esc_html__( 'Drag Drop Field', 'tain' ),
	'description'	=> esc_html__( 'This is drag and drop field.', 'tain' ),
	'default' 		=> array(
		'Left' 	=> array(
			'date'		=> esc_html__( 'Date', 'tain' ),
			'client'	=> esc_html__( 'Client', 'tain' ),
			'duration'	=> esc_html__( 'Duration', 'tain' )		
		),
		'Right' 	=> array(
			'category'	=> esc_html__( 'Category', 'tain' ),
			'tag'		=> esc_html__( 'Tags', 'tain' ),
			'share'		=> esc_html__( 'Share', 'tain' )			
		),
		'Disabled' 	=> array(
			'estimation'=> esc_html__( 'Estimation', 'tain' ),
			'url'		=> esc_html__( 'Url', 'tain' ),
			'place'		=> esc_html__( 'Place', 'tain' )
		)
	),
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Export
$settings = array(
	'id'			=> 'ajax-trigger-export-test',
	'type'			=> 'export',
	'title'			=> esc_html__( 'Export Field', 'tain' ),
	'description'	=> esc_html__( 'This is export field', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Import
$settings = array(
	'id'			=> 'ajax-trigger-import-test',
	'type'			=> 'import',
	'title'			=> esc_html__( 'Import Field', 'tain' ),
	'description'	=> esc_html__( 'This is import field', 'tain' ),
	'default'		=> '',
	'refresh'		=> 1
);
TainCustomizerConfig::buildFields( $settings );

//Section Start
$settings = array(
	'type'			=> 'section',
	'label'			=> esc_html__( 'Options Section Title', 'tain' ),
	'description'	=> esc_html__( 'This is inside option section description.', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Section End
$settings = array(
	'type'			=> 'section',
	'section_stat'	=> false
);
TainCustomizerConfig::buildFields( $settings );

//Header Label Start
$settings = array(
	'type'			=> 'toggle_section',
	'label'			=> esc_html__( 'Header Label Settings', 'tain' ),
	'description'	=> esc_html__( 'Click to edit address label settings.', 'tain' ),
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );

//Header Label End
$settings = array(
	'type'			=> 'toggle_section',
	'section_stat'	=> true
);
TainCustomizerConfig::buildFields( $settings );