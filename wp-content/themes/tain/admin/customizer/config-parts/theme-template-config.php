<?php

//Theme Option -> Theme Template
$theme_template_panel = new Tain_WP_Customize_Panel( $wp_customize, 'theme_template_panel', array(
	'title'			=> esc_html__( 'Theme Template', 'tain' ),
	'description'	=> esc_html__( 'These is the template settings for page.', 'tain' ),
	'priority'		=> 7,
	'panel'			=> 'tain_theme_panel'
));
$wp_customize->add_panel( $theme_template_panel );

//Theme Template -> Template General
$tain_general_template_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_general_template_section', array(
	'title'			=> esc_html__( 'Template General', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for template general.', 'tain' ),
	'priority'		=> 1,
	'panel'			=> 'theme_template_panel'
));
$wp_customize->add_section( $tain_general_template_section );

//Template General
$wp_customize->add_setting('ajax_trigger_tain_general_template_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_general_template_section', array(
	'section'		=> 'tain_general_template_section'
)));

//Theme Template -> Page Template
$tain_page_template_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_page_template_section', array(
	'title'			=> esc_html__( 'Page Template', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for page template.', 'tain' ),
	'priority'		=> 2,
	'panel'			=> 'theme_template_panel'
));
$wp_customize->add_section( $tain_page_template_section );

//Page Template
$wp_customize->add_setting('ajax_trigger_tain_page_template_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_page_template_section', array(
	'section'		=> 'tain_page_template_section'
)));

//Theme Template -> Blog Template
$tain_blog_template_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_blog_template_section', array(
	'title'			=> esc_html__( 'Blog Template', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for blog template.', 'tain' ),
	'priority'		=> 3,
	'panel'			=> 'theme_template_panel'
));
$wp_customize->add_section( $tain_blog_template_section );

//Blog Template
$wp_customize->add_setting('ajax_trigger_tain_blog_template_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_blog_template_section', array(
	'section'		=> 'tain_blog_template_section'
)));

//Theme Template -> Archive Template
$tain_archive_template_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_archive_template_section', array(
	'title'			=> esc_html__( 'Archive Template', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for archive template.', 'tain' ),
	'priority'		=> 4,
	'panel'			=> 'theme_template_panel'
));
$wp_customize->add_section( $tain_archive_template_section );

//Archive Template
$wp_customize->add_setting('ajax_trigger_tain_archive_template_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_archive_template_section', array(
	'section'		=> 'tain_archive_template_section'
)));

//Theme Template -> Single Post Template
$tain_single_post_template_section = new Tain_WP_Customize_Section( $wp_customize, 'tain_single_post_template_section', array(
	'title'			=> esc_html__( 'Single Post Template', 'tain' ),
	'description'	=> esc_html__( 'This is the setting for single post template.', 'tain' ),
	'priority'		=> 9,
	'panel'			=> 'theme_template_panel'
));
$wp_customize->add_section( $tain_single_post_template_section );

//Single Post Template
$wp_customize->add_setting('ajax_trigger_tain_single_post_template_section', array(
	'default'           => '',
	'sanitize_callback' 	=> 'esc_attr'
));
$wp_customize->add_control( new Trigger_Custom_control( $wp_customize, 'ajax_trigger_tain_single_post_template_section', array(
	'section'		=> 'tain_single_post_template_section'
)));