<?php
/**
 * The template for displaying all custom post types
 */
 
get_header(); 
$ahe = new TainHeaderElements;
$aps = new TainPostSettings;
$post_type = get_post_type();
$template = str_replace( "tain-", "", $post_type ); // template id
$aps->tain_set_post_template( $template );
$template_class = $aps->tain_template_content_class();
$full_width_class = '';
$acpt = new TainCPT;
?>
<div class="tain-content <?php echo esc_attr( 'tain-' . $template ); ?> tain-page">
		
		<?php $ahe->tain_header_slider('bottom'); ?>
		
		<?php $ahe->tain_page_title( 'page' ); ?>
		<div class="tain-content-inner">
			<div class="container">
	
				<div class="row">
					
					<div class="<?php echo esc_attr( $template_class['content_class'] ); ?>">
						<div id="primary" class="content-area">
							<?php
								$acpt->tain_cpt_call_template( $post_type );
							?>				
						</div><!-- #primary -->
					</div><!-- main col -->
					
					<?php if( $template_class['lsidebar_class'] != '' ) : ?>
					<div class="<?php echo esc_attr( $template_class['lsidebar_class'] ); ?>">
						<aside class="widget-area left-widget-area<?php echo esc_attr( $template_class['sticky_class'] ); ?>">
							<?php dynamic_sidebar( $template_class['left_sidebar'] ); ?>
						</aside>
					</div><!-- sidebar col -->
					<?php endif; ?>
					
					<?php if( $template_class['rsidebar_class'] != '' ) : ?>
					<div class="<?php echo esc_attr( $template_class['rsidebar_class'] ); ?>">
						<aside class="widget-area right-widget-area<?php echo esc_attr( $template_class['sticky_class'] ); ?>">
							<?php dynamic_sidebar( $template_class['right_sidebar'] ); ?>
						</aside>
					</div><!-- sidebar col -->
					<?php endif; ?>
					
				</div><!-- row -->
			
		</div><!-- .container -->
	</div><!-- .tain-content-inner -->
</div><!-- .tain-content -->
<?php get_footer();